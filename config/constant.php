<?php
return [
    'job_industry' => [
        0 => 'Account Management',
        1 => 'Accounting & Finance',
        2 => 'Administrative',
        3 => 'Advertising & PR',
        4 => 'Animals & Wildlife',
        5 => 'Art & Creative',
        6 => 'Bilingual',
        7 => 'Business Development',
        8 => 'Call Center',
        9 => 'Communications',
        10 => 'Computer & IT',
        11 => 'Consulting',
        12 => 'Customer Service',
        13 => 'Data Entry',
        14 => 'Editing',
        15 => 'Education & Training',
        16 => 'Engineering',
        17 => 'Entertainment & Media',
        18 => 'Environmental & Green',
        19 => 'Event Planning',
        20 => 'Fashion & Beauty',
        21 => 'Food & Beverage',
        22 => 'Government & Politics',
        23 => 'Graphic Design',
        24 => 'HR & Recruiting',
        25 => 'Human Services',
        26 => 'Insurance',
        27 => 'International',
        28 => 'Internet & Ecommerce',
        29 => 'Legal',
        30 => 'Manufacturing',
        31 => 'Marketing',
        32 => 'Math & Economics',
        33 => 'Medical & Health',
        34 => 'Mortgage & Real Estate',
        35 => 'News & Journalism',
        36 => 'Nonprofit & Philanthropy',
        37 => 'Operations',
        38 => 'Project Management',
        39 => 'Research',
        40 => 'Retail',
        41 => 'Sales',
        42 => 'Science',
        43 => 'Software Development',
        44 => 'Sports & Fitness',
        45 => 'Telemarketing',
        46 => 'Transcription',
        47 => 'Translation',
        48 => 'Travel & Hospitality',
        49 => 'Web Design',
        50 => 'Writing',
        51 => 'Youth & Children',
    ],
    'employment_type' => [
        0 => 'Full Time',
        1 => 'Part Time',
        2 => 'Casual',
        3 => 'Contracted',
        4 => 'Commission Only',
        5 => 'Volunteer',
        6 => 'Apprentice'
    ],
    'benefits_offered' => [
        'false' => 0,
        'true' => 1,
    ],
    'reverse_benefits_offered' => [
        0 => 'false',
        1 => 'true',
    ],
    'media_type' => [
        'image' => 1,
        'video_link' => 2,
    ],
    'voc_image_path' => '/uploads/',
    'roles' => [
        'administrator' => 'Administrator',
        'voc' => 'Voc',
        'voc_dex' => 'Vocdex'
    ],
    'roles_inverse' => [
        'administrator' => '1',
        'voc' => '2',
        'voc_dex' => '3'
    ],
    'vocs' => [
        0 => "All",
        1 => 'Regional',
        2 => 'Nearby',
        3 => 'Trending'
    ],
    'inverse_vocs' => [
        'All' => 0,
        'regional' => 1,
        'nearby' => 2,
        'Trending' => 3
    ],
    //Voc search home page
    'search' => [
        'pagination' => 5
    ],
    'range' => [
        'all' => "All Voc's",
        'regional' => 'Regional',
        'nearby' => 'Nearby'
    ],
    'posts' => [
        'job' => "App\Models\VocDashboard\Job",
        'event' => 'App\Models\Voc\VocEvent',
        'promotion' => 'App\Models\Voc\VocPromotion',
        'voice' => 'App\Models\VocDashboard\Voice',
        'update' => 'App\Models\VocDashboard\Update'
    ],
    'sharing' => [
        "Send E-mail" => "<i class='fa fa-envelope-o' aria-hidden='true'></i>",
        "Add to Collection" => "<i class='fa fa-briefcase' aria-hidden='true'></i>",
        'Facebook' => "<span class='facebook-share'></span>",
        'Twitter' => "<span class='twitter-share'></span>",
        'Linkedin' => "<span class='linkedin-share'></span>",
        'Pinterest' => "<span class='pinterest-share'></span>",
        'Google+' => "<span class='google-share'></span>"
    ],
    'sharing_type' => [
        'share_one' => '1',
        'share_two' => '2'
    ],
    'inverse_sharing' => [
        0 => "Send E-mail",
        1 => 'Add to Collection',
        2 => 'Facebook',
        3 => 'Twitter',
        4 => 'Linkedin',
        5 => 'Pinterest',
        6 => 'Google+'
    ], 
    'services' => [
        'basic' => '0',
        'plus' => '1',
        'create_your_own' => '2',
        'hire_us' => '3',
    ],
    'inverse_service' => [
        0 => 'basic',
        1 => 'plus',
        2 => 'create your own',
        3 => 'hire_us',
    ],
    'hire_us_status' => [
        'requested' => '1',
        'contacted' => '2',
    ],
    'inverse_hire_us_status' => [
        1 => 'Requested',
        2 => 'Contacted',
    ],
    
];
