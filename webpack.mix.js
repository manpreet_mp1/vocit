let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
        .sass('resources/assets/sass/frontend/app.scss', 'public/css/frontend.css')
        .sass('resources/assets/sass/frontend/home/home.scss', 'public/css/home.css')
        .sass('resources/assets/sass/frontend/about.scss', 'public/css/about.css')
        .sass('resources/assets/sass/frontend/signup.scss', 'public/css/signup.css')
        .sass('resources/assets/sass/frontend/pages/voc_dashboard.scss', 'public/css/voc_dashboard.css')
        .sass('resources/assets/sass/frontend/vocDex/home.scss', 'public/css/vocdex-home.css')
        .sass('resources/assets/sass/frontend/custom_template/voc_one_template.scss', 'public/css/voc_one_template.css')
        .sass('resources/assets/sass/frontend/custom_template/voc_two_template.scss', 'public/css/voc_two_template.css')
        .sass('resources/assets/sass/frontend/custom_template/lifeid.scss', 'public/css/lifeid.css')
        .sass('resources/assets/sass/frontend/custom_template/training.scss', 'public/css/training.css')
        .sass('resources/assets/sass/frontend/simple-content-pages.scss', 'public/css/simple-content-pages.css')

        .scripts([
            'resources/assets/js/plugins.js',
            'resources/assets/js/frontend/app.js',
            'resources/assets/js/frontend/user/login.js',
            'resources/assets/js/frontend/user/forget_password.js',
            'resources/assets/js/plugin/modernizr.custom.js',
            'resources/assets/js/plugin/scale-overlay.js',
        ], 'public/js/frontend.js')
        .scripts([
            'resources/assets/js/plugins.js',
            'resources/assets/js/frontend/app.js',
            'resources/assets/js/plugin/jquery.mousewheel.min.js',
            'resources/assets/js/plugin/jquery.zaccordion.min.js',
            'resources/assets/js/plugin/owl.carousel.min.js',
            'resources/assets/js/plugin/modernizr.custom.js',
            'resources/assets/js/plugin/scale-overlay.js',
            'resources/assets/js/frontend/user/forget_password.js',
            'resources/assets/js/plugin/packery.pkgd.min.js',
            'resources/assets/js/frontend/home.js',
            'resources/assets/js/frontend/user/login.js',
        ], 'public/js/home.js')
        .scripts([
            'resources/assets/js/plugins.js',
            'resources/assets/js/frontend/app.js',
            'resources/assets/js/plugin/jquery.maskedinput.min.js',
            'resources/assets/js/frontend/user/voc_create.js',
            'resources/assets/js/frontend/user/vocdex_create.js',
            'resources/assets/js/plugin/modernizr.custom.js',
            'resources/assets/js/plugin/scale-overlay.js',
            'resources/assets/js/frontend/user/login.js',
            'resources/assets/js/frontend/user/forget_password.js',
        ], 'public/js/signup.js')
        .scripts([
            'resources/assets/js/plugins.js',
            'resources/assets/js/frontend/app.js',
            'resources/assets/js/plugin/jquery.mousewheel.min.js',
            'resources/assets/js/plugin/jquery.zaccordion.min.js',
            'resources/assets/js/plugin/owl.carousel.min.js',
            'resources/assets/js/plugin/packery.pkgd.min.js',
            'resources/assets/js/frontend/vocdex/home.js',
        ], 'public/js/vocdex-home.js')
        .scripts([
            'resources/assets/js/plugin/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
            'resources/assets/js/frontend/voc/common2.js',
        ], 'public/js/voc_dashboard.js')

        .scripts([
            'resources/assets/js/plugins.js',
            'resources/assets/js/frontend/app.js',
            'resources/assets/js/plugin/modernizr.custom.js',
            'resources/assets/js/plugin/scale-overlay.js',
            'resources/assets/js/frontend/user/forget_password.js',
            'resources/assets/js/frontend/user/login.js',
            'resources/assets/js/frontend/custom_template/template.js',
        ], 'public/js/voc_template.js')
        //Backend
        .sass('resources/assets/sass/backend/app.scss', 'public/css/backend.css')
        .scripts([
            'resources/assets/js/plugins.js',
            'resources/assets/js/backend/app.js',
            'resources/assets/js/backend/custom.js',
            'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
            'resources/assets/js/plugin/jquery.dataTables.min.js',
            'resources/assets/js/plugin/dataTables.bootstrap.min.js',
            'resources/assets/js/plugin/bootstrap-datetimepicker/js/moment.js',
            'resources/assets/js/plugin/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js',
            'resources/assets/js/plugin/toastr.js',
        ], 'public/js/backend.js')

        .options({processCssUrls: false});

if (mix.inProduction) {
    mix.version();
}

if (mix.inDevelopment) {
    mix.sourceMaps();
}