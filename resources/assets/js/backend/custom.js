(function ($) {
    $("#voc-dashboard-header .user-menu .dropdown-toggle").on('click', function (e) {
        var $this = $(this),
                sel = $this.find('.angle-toggle');
        if (sel.hasClass('fa-angle-down')) {
            sel.addClass('fa-angle-up').removeClass('fa-angle-down');
        } else {
            sel.addClass('fa-angle-down').removeClass('fa-angle-up');
        }
        e.preventDefault();
    });

    $('#hireUs').on('click', function (e) {
        var $this = $(this);
        if (!$this.parent().hasClass('active')) {
            $.ajax({
                url: frontendHireUs,
                success: function (resp) {
                    if (resp.status == true) {
                        toastr.success(resp.message);
                        $this.parent().remove();
                    }
                },
                error: function (error) {
                    var responseText = JSON.parse(error.responseText);
                    toastr.info(responseText.message);
                }
            });
        }
        e.preventDefault();
    });
    
    $('.customization_option').on('click', function (e) {
        var $this = $(this);
        if (!$this.parent().hasClass('active')) {
            $.ajax({
                url: templateCustomizationAddOwn,
                type:"POST",
                data: {level:$this.attr('data-custom')},
                success: function (resp) {
                   location.href = $this.attr('href');
                },
                error: function (error) {
                    var responseText = JSON.parse(error.responseText);
                    toastr.info(responseText.message);
                }
            });
        }else{
           location.href = $this.attr('href'); 
        }
        e.preventDefault();
    });
    
    $('#custom_plus').on('click', function (e) {
        e.preventDefault();
    });


})(jQuery);