$(document).ready(function () {
    $('#voc-topic-choose .btn-red').on('click', function () {
        $(this).toggleClass('success');
    });
    $('#voc-follow-form').on('submit', function (e) {
        $('#voc-follow-submit-btn').attr("disabled", true);
        e.preventDefault();
        var $this = $(this),
                checkedVals = [];
        $('#voc-topic-choose .success').each(function (i, elem) {
            checkedVals.push($(elem).attr('data-model'));
        });
        $.ajax({
            type: "post",
            url: $this.attr('action'),
            data: {model: checkedVals, voc_id: $this.find('.voc_id').val()},
            dataType: 'json',
            success: function (response) {
                if (response.success == true) {
                    $('#voc-template').modal('hide');
//                        $('.continue-button button').removeClass('success');
                    $('#voc-follow-submit-btn').attr("disabled", false);
                } else if (response.status == false) {
                    $('#voc-follow-submit-btn').attr("disabled", false);
                    $('#voc-topic-choose .btn-red').removeClass('success');
                }
            },
            error: function (error) {
                $('#voc-follow-submit-btn').attr("disabled", false);
            }
        });
    });
    $('.follow-voc-link').on('click', function () {
        var $this = $(this);
        if ($this.attr('data-target') == 'login-modal') {
            vocApp.toggleOverlay(document.querySelector('#login-outer-popup .overlay'));
        } else {
            vocApp.toggleOverlay(document.querySelector('#follow-outer-popup .overlay'));
        }
    });
    $('.follow-selection').on('click', function (e) {
        var $this = $(this),
                model = $this.attr('data-model');
        $this.addClass('orange-color');
        $this.parent().find('.template-modals').val(model);
         e.preventDefault();
    });
    $('#all-sel-btn').on('click', function (e) {
        if ($('#voc-follow-form .follow-selection').hasClass('orange-color')) {
            $('#voc-follow-form .follow-selection').removeClass('orange-color');
        } else {
            $('#voc-follow-form .follow-selection').addClass('orange-color');
        }
         e.preventDefault();
    });

});