//MP JS File
(function ($) {
    vocApp.resizeIframe = function (iframeID, iframeContent) {
        var iframe = window.parent.document.getElementById(iframeID),
                container = document.getElementById(iframeContent);
        iframe.style.height = container.offsetHeight + 'px';
    };
    //right side page settings and your code box show/hide
    $(".page-block-title").on('click', function () {
        var $this = $(this),
                content = $this.attr('data-type');

        $this.siblings('.page-block-title').removeClass('active');
        $this.addClass('active');

        $('.page-block-content').hide();
        $('.page-block-content.' + content).show();
    });
    //END
    //page settings form submit
    $('.page-settings-form').on('submit', function (e) {
        var $this = $(this);
        $this.find('.form-submitter .btn-success').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: new FormData($(this)[0]),
            processData: false,
            contentType: false,
            success: function (response) {
                $this.find('.form-submitter .btn-success').attr("disabled", false);
                $this.siblings('.form-response').addClass('success').text(response.message);
                if (typeof response.logo != 'undefined') {
                    $this.find('.voc img').attr('src', response.logo);
                }
                setTimeout(function () {
                    $this.siblings('.form-response').removeClass('success').text('');
                }, 6000);
            },
            error: function (error) {
                vocApp.ajaxInputError(error, $('.page-settings-form'));
                $('.page-settings-form .btn-success').attr("disabled", false);
            }
        });
        e.preventDefault();
    });
    //END
    //code your own form submit
    $('#codeYourOwnForm').on('submit', function (e) {
        var $this = $(this);
        $this.find('.form-submitter .btn-success').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            success: function (response) {
                $this.find('.form-submitter .btn-success').attr("disabled", false);
                $this.siblings('.form-response').addClass('success').text(response.message);

                //reload
                var iframe = document.getElementById('codeYourOwnIframe');
                iframe.contentWindow.location.reload();
                //give height
                vocApp.resizeIframe('codeYourOwnIframe', 'codeYourOwnDiv');

                setTimeout(function () {
                    $this.siblings('.form-response').removeClass('success').text('');
                }, 6000);
            },
            error: function (error) {
                vocApp.ajaxInputError(error, $('#codeYourOwnForm'));
                $('#codeYourOwnForm .btn-success').attr("disabled", false);
            }
        });
        e.preventDefault();
    });
    //END
    $('#preview-upper-save').on('click', function () {
        if ($('.content-block').is(':visible')) {
            $('.content-block form').submit();
        } else {
            $('.page-settings form').submit();
        }
    });

})(jQuery);