var footerPostGrid;
$(document).ready(function () {
    //packery initialize
    vocApp.reloadPackery();
    //END

    var $allvocslide = $('#all-vocs-slide'),
            $allvocs = $('.vocit-posts');

    function dataAppend(resp) {
        if (!$.isEmptyObject(resp.vocs)) {
            $allvocslide.empty()
                    .html(resp.vocs);
            vocApp.zAccordionSlider();
            vocApp.owlSlider();
        }
        if (!$.isEmptyObject(resp.vocPosts)) {
            $allvocs.empty();
            footerPostGrid.packery('destroy');
            $allvocs.html(resp.vocPosts);
            vocApp.reloadPackery();
        }
        if (resp.empty === '') {
            $('#search-no-data').show();
        }
    }

    //search button  ajax
    $(".search-btn").on('click', function (e) {
        var string = $('.voc_search_input').val(),
                range = $('.selected-option').attr('data-range'),
                url_search = $('.search_form').attr('action');
        $('#search-no-data').hide();

        //next
        var next_page = 1;
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: url_search,
            data: {
                string: string,
                range: range,
                next_page: next_page
            },
            success: function (resp) {
                dataAppend(resp);
            },
            error: function (err) {
            }
        });
    });

    //Next Vocs
    $(document).on('click', '#next_btn', function (e) {
        $('.select2-selection__clear').empty();
        url_search_next = $('.search_form').attr('action');
        var next_page = vocApp.nextpageinc();
        var range = $('.selected-option').attr('data-range');
        e.preventDefault();
        $.ajax({
            url: url_search_next,
            type: "get",
            data: {
                next_page: next_page,
                range: range
            },
            success: function (resp)
            {
                if (resp.vocs) {
                    $allvocslide.empty()
                            .html(resp.vocs)
                            .removeClass('no-data');
                    vocApp.carouselInit();
                    if (resp.allVocsCount === resp.page) {
                        $('#next_btn').addClass('disable_btn');
                    }
                }
                if (resp.vocPosts) {
                    $allvocs.empty();
                    footerPostGrid.packery('destroy');
                    $allvocs.html(resp.vocPosts);
                    vocApp.reloadPackery();
                }
                if (resp.empty === '') {
                    $allvocslide.empty()
                            .addClass('no-data')
                            .html('<p> Oops! No result found. Try a new search. </p>')
                            .css('padding-top', '20px');
                }
//                $('.popular-searches.selected-option').find('a').addClass('orange-color');
//                if (resp.all) {
//                    $('#voc_range').empty();
//                    $('#voc_range').html("All Voc's");
//                }
//                if (resp.regional) {
//                    $('#voc_range').empty();
//                    $('#voc_range').html('Regional');
//                }
//                if (resp.nearby) {
//                    $('#voc_range').empty();
//                    $('#voc_range').html('NearBy');
//                }
            },
            error: function () {
//                console.log('fail');
            }
        });

    });

//Previous Button 
    $(document).on('click', '#prev_btn', function (e) {
        url_search_prev = $('.search_form').attr('action');
        var next_page = vocApp.prevpagedecr();
        var range = $('.selected-option').attr('data-range');
        e.preventDefault();
        $.ajax({
            url: url_search_prev,
            type: "get",
            data: {
                next_page: next_page,
                range: range
            },
            success: function (resp)
            {
                if (resp.vocs) {
                    $allvocslide.empty()
                            .html(resp.vocs)
                            .removeClass('no-data');
                    vocApp.carouselInit();
                    $('#bg-shd').attr('data-page', resp.page);
                    var page = $('#bg-shd').attr('data-page');
                    if (page <= 1) {
                        $('#prev_btn').addClass('disable_btn');
                    }
                }
                if (resp.vocPosts) {
                    $allvocs.empty();
                    footerPostGrid.packery('destroy');
                    $allvocs.html(resp.vocPosts);
                    vocApp.reloadPackery();
                    $('#bg-shd').attr('data-page', resp.page);
                }
                if (resp.empty === '') {
                    $allvocslide.empty().empty()
                            .addClass('no-data')
                            .html('<p> Oops! No result found. Try a new search. </p>')
                            .css('padding-top', '20px');
                }

            },
            error: function () {
//                console.log('fail');
            }
        });
    });

    $(document).on('click', '.popular-searches', function (e) {
        var $this = $(this);
        $this.addClass('selected-option').siblings().removeClass('selected-option');
        var rangeSelected = $('.selected-option').attr('data-range'),
                next_page = 1,
                url = $this.children('a').attr('href');
        $('#search-no-data').hide();

        e.preventDefault();
        $.ajax({
            type: "get",
            url: url,
            data: {
                next_page: next_page,
                range: rangeSelected
            },
            success: function (resp) {
                dataAppend(resp);
            },
            error: function () {
            }
        });
    });


    $(document).on('click', '.search_category_form li a', function (e) {
        var $this = $(this),
                cat_id = $this.attr('data-cat_id');
        $this.addClass('orange-color');
        $this.find('input').val(cat_id);
        e.preventDefault();
    });

    $(document).on('submit', '.search_category_form', function (e) {
        e.preventDefault();
//        $(this).toggleClass('selected-category');
        var $this = $(this);
//        var url = $(this).children('a').attr('href');
//        var categories_ids = [];
        var string = $('.voc_search_input').val();

//        $('.voc-categories.selected-category').each(function () {
//            categories_ids.push($(this).data('categories_id'));
//        });
        console.log($this.serialize());
        $.ajax({
            type: "get",
            url: $this.attr('action'),
            data: {
                categories: $this.serialize(),
                string: string
            },
            success: function (resp) {
                dataAppend(resp);
            },
            error: function () {
//                console.log('fail');
            }
        });
    });
    //category show/hide
    $('#category-popup').on('click', function () {
        vocApp.toggleOverlay(document.querySelector('#category-outer-popup .overlay'));
    });
    $('#category-outer-popup .overlay-close').on('click', function () {
        vocApp.toggleOverlay(document.querySelector('#category-outer-popup .overlay'));
    });
    //END
});

$(window).load(function () {
    vocApp.zAccordionSlider();
    vocApp.owlSlider();
});

$(window).scroll(function () {
    vocApp.scrollAppendData();
});
$(window).resize(function () {
    vocApp.zAccordionSlider();
    vocApp.owlSlider();
});