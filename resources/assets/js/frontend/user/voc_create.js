$(document).ready(function () {
    //Join now event handler
    if (location.hash == '#Voc') {
        $(".signup_page .join_company").css("opacity", "0");
        $(".signup_user .innerdata .inner_text").fadeOut("fast", "swing", function () {
            $(".vocformdiv").fadeIn(2000);
        });
        $(".signupcompany .inner_text").show();
        $(".userform").hide();
    }
    if (location.hash == '#VocDex') {
        $(".signup_page .join_user").css("opacity", "0");
        $(".signupcompany .inner_text").fadeOut("fast", "swing", function () {
            $(".userform").fadeIn(2000);
        });
        $(".signup_user .innerdata .inner_text").show();
        $(".vocformdiv").hide();
    }
    $('.join_company').click(function (e) {
        $(".signup_page .join_company").css("opacity", "0");
        $(".signup_user .innerdata .inner_text").fadeOut("fast", "swing", function () {
            $(".vocformdiv").fadeIn(2000);
        });
        $(".signupcompany .inner_text").show();
        $(".userform").hide();
        e.preventDefault();
    });
    $('.join_user').click(function (e) {
        $(".signup_page .join_user").css("opacity", "0");
        $(".signupcompany .inner_text").fadeOut("fast", "swing", function () {
            $(".userform").fadeIn(2000);
        });
        $(".signup_user .innerdata .inner_text").show();
        $(".vocformdiv").hide();
        e.preventDefault();
    });
    //END
    //Voc create first from on submit event 
    $('#createVoc').on('submit', function (e) {
        var $this = $(this);
        $this.find('.btn-create').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        var zipCode = $this.find("#zip_code");
        if (!vocApp.isValidUSZip(zipCode.val())) {
            zipCode.addClass('has-error').after('<div class="alert-error">Enter a valid Zip Code or you can add it later.</div>');
            $this.find('.create-btn').attr("disabled", false);
        } else {
            $.ajax({
                type: "POST",
                url: $this.attr('action'),
                data: $this.serialize(),
                success: function (response) {
                    if (response.success == true) {
                        $('#voc-create').hide();
                        $('#basic-info').show();
                        $this.find('input[type="text"]').val('');
                    } else if (response.status == 0) {
                        $('#basic-info').hide();
                        $('#voc-create').show();
                        $('#createVoc .btn-create').attr("disabled", false);
                    }
                },
                error: function (error) {
                    vocApp.ajaxInputError(error, $('#createVoc'));
                    $('#createVoc .btn-create').attr("disabled", false);
                }
            });
        }
        e.preventDefault();
    });
    //END
//    $("#phone_number").mask("(999) 999-9999");
    $('#vocProfile').on('submit', function (e) {
        var $this = $(this);
        $this.find('.btn-create').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        if (!vocApp.isValidPhoneNumber($this.find("#phone_number").val())) {
            $this.find("#phone_number").addClass('has-error').after('<div class="alert-error">Add a valid phone number.</div>');
            $this.find('.create-btn').attr("disabled", false);
        } else if ($this.find("#password").val() != $this.find("#password_confirmation").val()) {
            $this.find("#password").addClass('has-error').after('<div class="alert-error">Passwords do not matched.</div>');
            $this.find('.create-btn').attr("disabled", false);
        } else {
            $.ajax({
                type: "POST",
                url: $this.attr('action'),
                data: $this.serialize(),
                success: function (response) {
                    if (response.success == true) {
                        $('#topic').show();
                        $('#basic-info').hide();
                        $this.find('input[type="text"],input[type="password"],input[type="email"]').val('');
                    } else if (response.status == 0) {
                        $('#basic-info').hide();
                        $('#voc-create').show();
                        $('#vocProfile .btn-create').attr("disabled", false);
                    }
                },
                error: function (error) {
                    vocApp.ajaxInputError(error, $('#vocProfile'));
                    $('#vocProfile .btn-create').attr("disabled", false);
                }
            });
        }
        e.preventDefault();
    });

    $("#templateModal .voc_template").on('click', function (e) {
        var $this = $(this);
        $.ajax({
            type: "POST",
            url: storeTemplateIdUrl,
            data: {
                template_id: $this.attr('data-template_id'),
            },
            success: function (response) {
                $this.parent().siblings('.each-template').removeClass('success');
                $this.parent().addClass('success');
            },
            error: function (error) {
            }
        });
        e.preventDefault();
    });
    $("#templateModal .next-btn").on('click', function (e) {
        $(this).find('.btn-create').attr("disabled", true);
        $('#template').hide();
        $('#site-builder').show();
        setTimeout(function () {
            CKEDITOR.instances['rich-editor'].resize('100%', 300);
        }, 1000);
    });

    CKEDITOR.replace('rich-editor');
    $('#siteBuilerForm').on('submit', function (e) {
        var $this = $(this);
        $this.find('.btn-create').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        CKEDITOR.instances['rich-editor'].updateElement();
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: new FormData($(this)[0]),
            processData: false,
            contentType: false,
            success: function (response) {
                if (response.success == true) {
                    $this.siblings('.form-response').addClass('success').text(response.message);
                    setTimeout(function () {
                        location.href = response.route;
                    }, 2000);
                } else if (response.status == 0) {
                    $('#site-builder').hide();
                    $('#voc-create').show();
                    $('#siteBuilerForm .btn-create').attr("disabled", false);
                }
            },
            error: function (error) {
                vocApp.ajaxInputError(error, $('#siteBuilerForm'));
                $('#siteBuilerForm .btn-create').attr("disabled", false);
            }
        });
        e.preventDefault();
    });
    //category button toggle with subcategory
//    $(".cat-subcat-selection").click(function () {
//        var $this = $(this),
//                catData = $this.attr('data-cat'),
//                subCatHtml = $('.sub-categories-list[data-subcat="' + catData + '"]').children().clone(),
//                modalSel = $('#voc-category-topic-modal');
//        modalSel.find('.create-body').empty().html(subCatHtml);
//        modalSel.find('.create-body').attr('data-cat', catData);
//        
//    });
//    $(document).on('click', '#voc-category-topic-modal .btn-red', function () {
//        var modalSel = $('#voc-category-topic-modal'),
//                catData = modalSel.find('.create-body').attr('data-cat');
//
//        var checked = false;
//        modalSel.find('.create-body').find('input[type="checkbox"]').each(function (i, v) {
//            if ($(this).is(':checked')) {
//                checked = true;
//            }
//        });
//        if (checked == true) {
//            $('.cat-subcat-selection[data-cat="' + catData + '"]').addClass('success');
//        } else {
//            $('.cat-subcat-selection[data-cat="' + catData + '"]').removeClass('success');
//        }
//
//        var modalHtml = modalSel.find('.create-body').children().clone();
//        $('.sub-categories-list[data-subcat="' + catData + '"]').empty().html(modalHtml);
//        modalSel.hide();
//    });

    $(".cat-subcat-selection").click(function () {
        var $this = $(this),
                checkboxSel = $this.find('.category-check');
        if (checkboxSel.prop('checked')) {
            checkboxSel.closest('a').removeClass('category-visited');
            checkboxSel.prop('checked', false);
        } else {
            checkboxSel.closest('a').addClass('category-visited');
            checkboxSel.prop('checked', true);
        }
    });
    //END
    $('#voc-category-form').on('submit', function (e) {
        var $this = $(this);
        $this.find('.btn-voc').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            success: function (response) {
                if (response.success == true) {
                    $('#templateModal .new_created_voc_id').val(response.voc_id);
                    $('#topic').hide();
                    $('#template').show();
                } else if (response.status == 0) {
                    $('#topic').hide();
                    $('#voc-create').show();
                    $('#voc-category-form .btn-voc').attr("disabled", false);
                }
            },
            error: function (error) {
                $('#voc-category-form').siblings('.form-response').addClass('error').text('Something went wrong. Please try again.');
                $('#voc-category-form .btn-voc').attr("disabled", false);
            }
        });
        e.preventDefault();
    });

});
