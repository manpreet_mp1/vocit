$(document).ready(function () {
    //vocdex create first modal event
    $('#createVocDex').on('submit', function (e) {
        var $this = $(this);
        $this.find('.btn-vocdex').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
       var zipCode = $this.find("#zip_code");
        if (!vocApp.isValidUSZip(zipCode.val())) {
            zipCode.addClass('has-error').after('<div class="alert-error">Enter a valid Zip Code or you can add it later.</div>');
            $this.find('.btn-vocdex').attr("disabled", false);
        } else {
            $.ajax({
                type: "POST",
                url: $this.attr('action'),
                data: $this.serialize(),
                success: function (response) {
                    $('#preferences .new_created_user_id').val(response);
                    $('#homepage-create').hide();
                    $('#preferences').show();
                    $this.find('input[type="text"],input[type="password"]').val('');
                    $('#createVocDex .btn-vocdex').attr("disabled", false);
                },
                error: function (error) {
                    vocApp.ajaxInputError(error, $('#createVocDex'));
                    $('#createVocDex .btn-vocdex').attr("disabled", false);
                }
            });
        }
        e.preventDefault();
    });

    $('#storeVocDexSubCategory').on('submit', function (e) {
        var $this = $(this),
                $allvocslide = $('#voc-list-slider .slides');
        $this.find('.btn-vocdex').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            success: function (response) {
                if (response.success == true) {

                    $allvocslide.empty().html(response.recommended_vocs);
//                    vocApp.carouselInit();

                    $('#preferences').hide();
                    $('#carousel').show();
                    $this.find('input[type="text"]').val('');
                } else if (response.status == 0) {
                    $('#preferences').hide();
                    $this.find('input[type="text"]').val('');
                    $('#homepage-create').show();
                    $('#storeVocDexSubCategory .btn-vocdex').attr("disabled", false);
                }
            },
            error: function (error) {
                $('#storeVocDexSubCategory').siblings('.form-response').addClass('error').text('Something went wrong. Please try again.');
                $('#storeVocDexSubCategory .btn-vocdex').attr("disabled", false);
            }
        });
        e.preventDefault();
    });

    $('#preferences .btn-skip').on('click', function () {
        $('#preferences').hide();
        $('#carousel').show();
    });

//    $('#voc-list-slider').carousel({
//        carouselWidth: 550,
//        carouselHeight: 300,
//        frontWidth: 250,
//        frontHeight: 270,
//        directionNav: true,
//        shadow: false,
//        buttonNav: 'none',
//        backOpacity: 1,
//        reflection: false,
//        autoplay: false
//    });
//    $('#voc-list-slider .prev i').addClass('rec fa fa-angle-left');
//    $('#voc-list-slider .next i').addClass('rec fa fa-angle-right');

    $('#carousel .btn-skip').on('click', function () {
        var $sel = $('#carousel');
        $sel.find('.form-response').addClass('success').text('Yayy!! Your Vocdex profile has been created successfully.');
        setTimeout(function () {
            $sel.find('.form-response').removeClass('success').text('');
        }, 2000);
    });
    $('#storeRecommendedVocs-form').on('submit', function (e) {
        var $this = $(this);
        $this.find('.btn-vocdex').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            success: function (response) {
                if (response.success == true) {
                    $this.find('.form-response').addClass('success').text(response.message);
                    setTimeout(function () {
                        location.href = response.route;
                    }, 2000);
                } else if (response.status == 0) {
                    $('#carousel').hide();
                    $('#homepage-create').show();
                    $this.find('.btn-vocdex').attr("disabled", false);
                }
            },
            error: function (error) {
                $('#storeRecommendedVocs-form').siblings('.form-response').addClass('error').text('Something went wrong. Please try again.');
                $('#storeRecommendedVocs-form .btn-vocdex').attr("disabled", false);
            }
        });
        e.preventDefault();
    });
});