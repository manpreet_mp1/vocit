$(document).ready(function () {
    $('#forget-password-link').on('click', function (e) {
        $('#login-modal').modal('hide');
        $('#forget-password').modal('show');
        e.preventDefault();
    });
    $("#forget-password-form").on('submit', function (e) {
        var $this = $(this);
        $this.find('.btn-login').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        $.ajax({
            type: 'POST',
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (resp) {
                $this.siblings('.form-response').addClass('success').text(resp.success);
                setTimeout(function () {
                    $this.find('input[type="email"]').val('');
                    $this.siblings('.form-response').removeClass('success').text('');
                    $('#forget-password').modal('hide');
                }, 20000);
            },
            error: function (err) {
                vocApp.ajaxInputError(err, $('#forget-password-form'));
                $("#forget-password-form .btn-login").attr("disabled", false);
            }
        });
        e.preventDefault();
    });
});