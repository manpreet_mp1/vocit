$(document).ready(function () {
    $("#custom-login").on('submit', function (e) {
        var $this = $(this);
        $this.find('.btn-login').attr("disabled", true);
        vocApp.ajaxInputErrorRemove($this);
        $.ajax({
            type: 'POST',
            url: $this.attr('action'),
            data: $this.serialize(),
            async: false,
            success: function (resp) {
                location.href = resp.route;
            },
            error: function (err) {
                vocApp.ajaxInputError(err, $("#custom-login"));
                $('#custom-login .btn-login').attr("disabled", false);
            }
        });
        e.preventDefault();
    });
    $("#signup-voc-show1").on('click', function (e) {
        $('#login-modal').modal('hide');
        $('#voc-create').modal('show');
    });
    $("#signup-vocdex-show1").on('click', function (e) {
        $('#login-modal').modal('hide');
        $('#homepage-create').modal('show');
    });
});