vocApp.ajaxInputErrorRemove = function (formSel) {
    formSel.find('.has-error').removeClass('has-error');
    formSel.find('.alert-error').remove();
    formSel.siblings('.form-response').removeClass('error').text('');
},
        vocApp.ajaxInputError = function (error, formSel) {
            //removing all error classes
            formSel.find('.has-error').removeClass('has-error');
            formSel.find('.alert-error').remove();
            formSel.siblings('.form-response').removeClass('error').text('');
            if ($.isEmptyObject(error)) {
                formSel.siblings('.form-response').addClass('error').text('Something went wrong. Please try again.');
            } else {
                if (!$.isEmptyObject(error.responseText)) {
                    var responseText = JSON.parse(error.responseText);
                    if (typeof responseText.error == 'undefined') {
                        $.each(responseText, function (i, v) {
                            if ($.isArray(v)) {
                                var textValue = '';
                                $.each(v, function (i, value) {
                                    textValue = textValue + ' ' + value;
                                });
                            } else {
                                var textValue = v;
                            }
                            formSel.find('[name="' + i + '"]').addClass('has-error').after('<div class="alert-error">' + textValue + '</div>');
                        });
                    } else {
                        formSel.siblings('.form-response').addClass('error').text(responseText.error);
                    }
                }
            }
        },
        vocApp.isValidUSZip = function (sZip) {
            if (sZip.length != 0) {
                return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(sZip);
            } else {
                return true;
            }
        },
        vocApp.isValidPhoneNumber = function (number) {
            if (typeof number != 'undefined' && number.length != 0) {
                return /(?:^|\D)\(([2-9])(?:\d(?!\1)\d|(?!\1)\d\d)\)\s*[2-9]\d{2}-\d{4}/.test(number);
            } else {
                return true;
            }
        },
        vocApp.customerPopup = function (e, sharedLink, intWidth, intHeight, blnResize) {
            // Social Share Function
            e.preventDefault();
            // Set values for window
            intWidth = intWidth || '500';
            intHeight = intHeight || '400';
            strResize = (blnResize ? 'yes' : 'no');
            // Set title and open popup with focus on it
            //var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
            strParam = 'width=' + intWidth + ',height=' + intHeight + ', top=' + ($(window).height() / 2 - (intHeight / 2)) + ',left=' + ($(window).width() / 2 - (intWidth / 2)) + ',resizable=' + strResize,
                    objWindow = window.open(sharedLink, 'Social Share', strParam).focus();
        },
//        vocApp.carouselInit = function () {
//            $('#voc-list-slider').carousel({
//                carouselWidth: 550,
//                carouselHeight: 300,
//                frontWidth: 250,
//                frontHeight: 270,
//                directionNav: true,
//                shadow: false,
//                buttonNav: 'none',
//                backOpacity: 1,
//                reflection: false,
//                autoplay: false,
//                responsive: true
//            });
//            $('#voc-list-slider .prev i').addClass('fa fa-angle-left');
//            $('#voc-list-slider .next i').addClass('fa fa-angle-right');
//        },
//        vocApp.disablePrevBtn = function () {
//            var page = $('#bg-shd').attr('data-page');
//            if (page <= 1) {
//                $('#prev_btn').addClass('disable_btn');
//            }
//        },
        vocApp.reloadPackery = function () {
            footerPostGrid = $('.vocit-posts').packery({
                itemSelector: '.voc-post',
                gutter: 0,
                percentPosition: true
            }).imagesLoaded().progress(function () {
                footerPostGrid.packery('layout');
            });
        },
        vocApp.scrollAppendData = function () {
            if ($(document).height() <= $(window).scrollTop() + $(window).height()) {
//        get voc ids
                var arr = [],
                        inputs = $('input[type="hidden"][class="scroll_voc_id"]');
                for (var i = 0, len = inputs.length; i < len; i++) {
                    if (inputs[i].type === "hidden") {
                        arr.push(inputs[i].value);
                    }
                }
                var scroll_page = $('input[type="hidden"][class="scroll_voc_id"]').attr('data-scroll-page'),
                        next_page = eval(scroll_page) + 1;
                $('input[type="hidden"][class="scroll_voc_id"]').attr('data-scroll-page', next_page);
                $.ajax({
                    url: scroll_form_route,
                    type: "GET",
                    data: {
                        arr: arr,
                        next_page: next_page
                    },
                    success: function (resp) {
                        var $items = $(resp.vocPosts);
                        footerPostGrid.append($items).packery('appended', $items).imagesLoaded().progress(function () {
                            footerPostGrid.packery('layout');
                        });
                    },
                    error: function () {
                    }
                });
            }
        },
        vocApp.nextpageinc = function () {
            var page = $('#bg-shd').attr('data-page'),
                    next_page = parseInt(page) + 1;
            $('#bg-shd').attr('data-page', next_page);
            return next_page;
        },
        vocApp.vocdexprevpagedecr = function () {
            var page = $('#bg-shd').attr('data-page');
            next_page = parseInt(page) - 1;
            next_page = 1;
            $('#bg-shd').attr('data-page', next_page);
            return next_page;
        },
        vocApp.prevpagedecr = function () {
            var page = $('#bg-shd').attr('data-page');
            next_page = eval(page) - 1;
            $('#bg-shd').attr('data-page', next_page);
            return next_page;
        },
        vocApp.zAccordionBuild = function (x) {
            var opts, current;
            var media = $(document).find("#slider_div");
            var defaults = {
                slideClass: 'slide',
                buildComplete: function () {
                    console.log('build complete');
                    media.css('visibility', 'visible');
                },
                timeout: 5500

            };
            if (!$.isEmptyObject(media.data())) { /* If an zAccordion is found, rebuild it with new settings. */
                media.css('visibility', 'hidden');
                current = media.data('current');
                opts = $.extend({
                    startingSlide: current
                }, defaults, x);
                media.zAccordion('destroy', {
                    removeStyleAttr: true,
                    removeClasses: true,
                    destroyComplete: {
                        afterDestroy: function () {
                            try {
                                zAccordionSlider();
                                console.log('zAccordion destroyed! Attempting to rebuild...');
                            } catch (e) {
                            }
                        },
                        rebuild: opts
                    }
                });
            } else { /* If no zAccordion is found, build one from scratch. */
                media.css('visibility', 'hidden');
                opts = $.extend(defaults, x);
                media.zAccordion(opts);
            }
        },
        vocApp.zAccordionSlider = function (pass) {
            var sliderContentHeightConstant = 132;
            var tabWidthPercentage = 0.1;
            var all_slider_div = $('.slider_outer_div'),
                    tabWidth = parseInt(all_slider_div.width()) * tabWidthPercentage,
                    imageWidth = parseInt(all_slider_div.width()) - parseInt(tabWidth) * 4,
                    imageHeight = parseInt(imageWidth) / 1.8,
                    sliderHeight = imageHeight + sliderContentHeightConstant,
                    slider_div = $("#slider_div");
            if (typeof pass != 'undefined' && pass.resize == true) {
                vocApp.zAccordionBuild({
                    slideWidth: imageWidth,
                    slideHeight: imageHeight,
                    width: all_slider_div.width(),
                    height: sliderHeight
                });
            } else {
                slider_div.zAccordion({
                    auto: false,
                    timeout: 5500,
                    tabWidth: tabWidth,
                    width: all_slider_div.width(),
                    height: sliderHeight,
                    startingSlide: 2,
                    slideClass: 'slide',
                    animationStart: function () {
                        slider_div.find('li.slide-previous .content .left_div').removeClass('animate');
                    },
                    animationComplete: function () {
                        slider_div.find('li.slide-open .content .left_div').addClass('animate');
                    }
                });
                all_slider_div.find('.image_div').css('width', imageWidth);
                all_slider_div.find('.image_div').css('height', imageHeight);
                all_slider_div.css({'opacity': 1});
                $('#slider_div > li .content').css('top', imageHeight);
                $('#slider_div').css('visibility', 'visible');
            }
        },
        vocApp.owlSlider = function () {
            var all_slider_div = $('.all_slider_div');
            if ($(window).width() < 768 || all_slider_div.hasClass('lesser-vocs')) {
                all_slider_div.owlCarousel({
                    animateOut: 'fadeOut',
                    animateIn: 'fadeIn',
                    autoplay: false,
                    items: 1,
                    dotsEach: false,
                    navText: ['', '']
                });
                all_slider_div.css({'opacity': 1});
                $('.owl-nav .owl-next').click(function () {
                    all_slider_div.trigger('next.owl.carousel');
                });
                $('.owl-nav .owl-prev').click(function () {
                    all_slider_div.trigger('prev.owl.carousel');
                });
            }
        };
// END
$(document).ready(function () {
    //Voc Posts Mini
//    $(document).on('click', '.footer-posts', function () {
//        var $this = $(this),
//                //mail-share
//                footerSel = $this.closest('.footer-posts'),
//                title = footerSel.find('.share-title').text(),
//                desp = footerSel.find('.share-desc p').text(),
//                link = 'mailto:someone@example.com&subject=' + title + '&body=' + desp;
//        $('#mailLink').attr("href", link);
//        //clone
//        var clonedHtml = $this.clone();
//        clonedHtml = clonedHtml.removeClass('col-sm-3').addClass('col-sm-12').removeAttr("style");
//        clonedHtml.find('.sharing-icons').removeAttr("style");
//        var vocPostMiniSel = $('#vocPostMini');
//        vocPostMiniSel.modal('show');
//        vocPostMiniSel.find('.modal-body > .row').html(clonedHtml);
//    });
//Show share buttons
    $('.share-all').hover(function () {
        $(this).find('.share-buttons').stop().fadeIn();
    }, function () {
        $(this).find('.share-buttons').stop().fadeOut();
    });
    //END
    $('.social-share').on("click", function (e) {
        var $this = $(this),
                href = $this.attr('href');
        if (href.length > 0) {
            vocApp.customerPopup(e, href, 600, 600, true);
        } else {
            vocApp.toggleOverlay(document.querySelector('#login-outer-popup .overlay'));
        }
        e.preventDefault();
    });

    $(document).on('click', '.loginModalOpen', function (e) {
        e.preventDefault();
        setTimeout(function () {
            $('#vocPostMini').modal('hide');
            setTimeout(function () {
                $('#login-modal').modal('show');
            }, 400);
        }, 300);

    });

    //END

//    $(document).on('click', '.voc-Mini', function (e) {
//        var $this = $(this);
//        var voc_mini_id = $this.find(".scroll_voc_id").val();
//        e.preventDefault();
//        $.ajax({
//            url: voc_mini,
//            type: "get",
//            data: {
//                voc_mini_id: voc_mini_id
//            },
//            success: function (resp)
//            {
//                if (resp.vocMini) {
//                    $('#vocMini').empty();
//                    $('#vocMini').html(resp.vocMini);
//                    $('#vocMini').modal('show');
//                }
//            },
//            error: function () {
//                console.log('fail');
//            }
//        });
//    });
//    // follow 
//    $(document).on('click', '.follow-voc', function (e) {
//        $('.follow-voc').attr("disabled", true);
//        e.preventDefault();
//        console.log('hi');
//        var follow_id = $('.follow-voc').next('.voc_id').val();
//        $.ajax({
//            url: miniVoc,
//            type: "get",
//            data: {
//                voc_id: follow_id
//            },
//            dataType: 'json',
//            success: function (response) {
//                if (response.success == true) {
//                    $('#vocMini').modal('hide');
//                    $('.follow-voc').attr("disabled", false);
//                }
//            },
//            error: function (error) {
//                $('.follow-voc').attr("disabled", false);
//            }
//        });
//    });

//coming soon tooltip
    $('.basic-tooltip').tooltip();
//END
    //login show/hide
    $('.login_btn').on('click', function (e) {
        vocApp.toggleOverlay(document.querySelector('#login-outer-popup .overlay'));
        e.preventDefault();
    });
    $('#login-outer-popup .overlay-close').on('click', function () {
        vocApp.toggleOverlay(document.querySelector('#login-outer-popup .overlay'));
    });
    //END
    //forgot show/hide
    $('#forget-password-link').on('click', function (e) {
        vocApp.toggleOverlay(document.querySelector('#forgot-outer-popup .overlay'));
        vocApp.toggleOverlay(document.querySelector('#login-outer-popup .overlay'));
        e.preventDefault();
    });
    $('#forgot-outer-popup .overlay-close').on('click', function () {
        vocApp.toggleOverlay(document.querySelector('#forgot-outer-popup .overlay'));
    });
    //END
});
