var footerPostGrid;
$(document).ready(function () {
  //packery initialize
    vocApp.reloadPackery();
    //END

    var $allvocslide = $('.all-vocs-slide'),
            $allvocs = $('.vocit-posts');

    //next btn
    $(document).on('click', '#next_btn', function (e) {
        var next_page = vocApp.nextpageinc();
        e.preventDefault();
        $.ajax({
            url: vocdex_form_route,
            type: "get",
            data: {
                next_page: next_page
            },
            success: function (resp)
            {
                if (!$.isEmptyObject(resp.vocs)) {
                    $allvocslide.empty()
                            .html(resp.vocs)
                            .removeClass('no-data');
                    vocApp.carouselInit();
                    if (resp.allVocsCount === resp.page) {
                        $('#next_btn').addClass('disable_btn');
                    }
                }
                if (!$.isEmptyObject(resp.vocPosts)) {
                    $allvocs.empty();
                    footerPostGrid.packery('destroy');
                    $allvocs.html(resp.vocPosts);
                    vocApp.reloadPackery();
                }
                if ((resp.empty === '')) {
                    $allvocslide.empty()
                            .addClass('no-data')
                            .html('<p> Oops! No result found. Try a new search. </p>')
                            .css('padding-top', '20px');
                }
            },
            error: function () {
//                console.log('fail');
            }
        });

    });
//Previous Button 
    $(document).on('click', '#prev_btn', function (e) {
        var next_page = vocApp.vocdexprevpagedecr();
        e.preventDefault();
        $.ajax({
            url: vocdex_form_route,
            type: "get",
            data: {
                next_page: next_page
            },
            success: function (resp)
            {
                if (resp.vocs) {
                    $allvocslide.empty()
                            .html(resp.vocs)
                            .removeClass('no-data');
                    vocApp.carouselInit();
                    $('#bg-shd').attr('data-page', resp.page);
                    var page = $('#bg-shd').attr('data-page');
                    if (page <= 1) {
                        $('#prev_btn').addClass('disable_btn');
                    } else {
                        $('#prev_btn').removeClass('disable_btn');
                    }
                }
                if (resp.vocPosts) {
                    $allvocs.empty();
                    footerPostGrid.packery('destroy');
                    $allvocs.html(resp.vocPosts);
                    vocApp.reloadPackery();
                    $('#bg-shd').attr('data-page', resp.page);
                }
                if (resp.empty === '') {
                    $allvocslide.empty().empty()
                            .addClass('no-data')
                            .html('<p> Oops! No result found. Try a new search. </p>')
                            .css('padding-top', '20px');
                }
            },
            error: function () {
//                console.log('fail');
            }
        });
    });
});
$(window).load(function () {
    vocApp.zAccordionSlider();
    vocApp.owlSlider();
});

$(window).scroll(function () {
    vocApp.scrollAppendData();
});
$(window).resize(function () {
    vocApp.zAccordionSlider();
    vocApp.owlSlider();
});