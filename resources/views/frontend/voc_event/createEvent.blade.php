@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Create Event')
@section('page-header')
<h1>
    Create Event
</h1>
@stop

@section('after-styles')
{{ Html::style(mix('css/voc_dashboard.css')) }}
@endsection

@section('content')

{{ Form::open(['route'=>'frontend.event.store','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Create Event</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('name')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('name', 'Name',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('name', null, array('class' => 'form-control','maxlength' => '255', 'required')) }}
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('body', 'Body',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::textarea('body', null, array('class' => 'form-control ckeditor', 'required')) }}
                <span class="help-block">{{ $errors->first('body') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('start')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('start', 'Start',array('class'=>'control-label')) }}
            </div>
            <div class='col-sm-10 align-datetimepicker'>
                <div class="input-group date" id="start-time">
                    <input type='text' name="start" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <span class="help-block">{{ $errors->first('start') }}</span>
                </div>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('end')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('end', 'End',array('class'=>'control-label')) }}
            </div>
            <div class='col-sm-10 align-datetimepicker'>
                <div class="input-group date" id="end-time">
                    <input type='text' name="end" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <span class="help-block">{{ $errors->first('end') }}</span>
                </div>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('logo')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('logo', 'Logo',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10 input-group" style="padding: 0 15px;">
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                    </a>
                </span>
                <input id="thumbnail" class="form-control" type="text" name="logo">
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <img id="holder" style="margin-top:15px;max-height:100px;">
            </div>
            <span class="help-block">{{ $errors->first('logo') }}</span>
        </div>

        <div class="form-group <?php if ($errors->first('location')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('location', 'Location',array('class'=>'control-label')) }}
            </div>
            <input type="hidden" name="street_number" class="field" id="street_number" disabled="true"></input>
            <input type="hidden" name="route" class="field" id="route" disabled="true"></input>
            <input type="hidden" name="locality" class="field" id="locality" disabled="true"></input>
            <input type="hidden" name="neighborhood" class="field" id="neighborhood" disabled="true"></input>
            <input type="hidden" name="district" class="field" id="administrative_area_level_2" disabled="true"></input>
            <input type="hidden" name="state" class="field" id="administrative_area_level_1" disabled="true"></input>
            <input type="hidden" name="country" class="field" id="country" disabled="true"></input>
            <input type="hidden" name="postal_code" class="field" id="postal_code" disabled="true"></input>

            <div class="col-sm-10">
                {{ Form::text('location', null, array('class' => 'form-control', 'required', 'id' => 'txtPlaces','placeholder'=>"Enter a location")) }}
                {{ Form::hidden('latitude', 'latitude', array('id' => 'latitude')) }}
                {{ Form::hidden('longitude', 'longitude', array('id' => 'longitude')) }}
                <span class="help-block">{{ $errors->first('location') }}</span>
            </div>
        </div>

    </div>

    <?php
    if (!empty($sharing)) {
        ?>

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Post Options</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'General',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==1)
                        {{ Form::checkbox('sharingOptions[]', $share->id, null, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'Share',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==2)
                        {{ Form::checkbox('sharingOptions[]', $share->id, null, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="box-body">
        <div class="pull-right">
            {{ Form::submit(trans('Post'), ['class' => 'btn btn-success']) }}
        </div>
        {{ Form::close() }}
        <div class="clearfix"></div>
    </div>
</div>

@endsection
@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGJ5EazH2m_do6_Svm7ZWn2DGB3g-qLOg&sensor=false&libraries=places" type="text/javascript"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkUOdZ5y7hMm0yrcCQoCvLwzdM6M8s5qk&libraries=places&callback=initAutocomplete" async defer></script>-->
<script src="{{url('/')}}/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
$(document).ready(function () {
    $('#lfm').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});

    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        neighborhood: 'long_name',
        administrative_area_level_2: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            console.log(place);
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
            $('#latitude').val(latitude);
            $('#longitude').val(longitude);

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        });
    });

//    var placeSearch, autocomplete;
//    var componentForm = {
//      street_number: 'short_name',
//      route: 'long_name',
//      locality: 'long_name',
//      administrative_area_level_1: 'short_name',
//      country: 'long_name',
//      postal_code: 'short_name'
//    };
//
//    function initAutocomplete() {
//      autocomplete = new google.maps.places.Autocomplete(
//          (document.getElementById('autocomplete')),
//          {types: ['geocode']});
//      autocomplete.addListener('place_changed', fillInAddress);
//    }
//
//    function fillInAddress() {
//      // Get the place details from the autocomplete object.
//      var place = autocomplete.getPlace();
//
//      for (var component in componentForm) {
//        document.getElementById(component).value = '';
//        document.getElementById(component).disabled = false;
//      }
//
//      for (var i = 0; i < place.address_components.length; i++) {
//        var addressType = place.address_components[i].types[0];
//        if (componentForm[addressType]) {
//          var val = place.address_components[i][componentForm[addressType]];
//          document.getElementById(addressType).value = val;
//        }
//      }
//    }
//
//    function geolocate() {
//      if (navigator.geolocation) {
//        navigator.geolocation.getCurrentPosition(function(position) {
//          var geolocation = {
//            lat: position.coords.latitude,
//            lng: position.coords.longitude
//          };
//          var circle = new google.maps.Circle({
//            center: geolocation,
//            radius: position.coords.accuracy
//          });
//          autocomplete.setBounds(circle.getBounds());
//        });
//      }
//    }






    $("#start-time,#end-time").datetimepicker({
        minDate: moment(),
        useCurrent: false
    });
    $("#start-time").on("dp.change", function (e) {
        $('#end-time').data("DateTimePicker").minDate(e.date);
    });

    $("#end-time").on("dp.change", function (e) {
        $('#start-time').data("DateTimePicker").maxDate(e.date);
    });
});
</script>
@endsection 