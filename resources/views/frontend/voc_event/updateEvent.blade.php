@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Update Event')
@section('page-header')
<h1>
    Update Event
</h1>
@stop
@section('content')

{{ Form::open(['route'=>'frontend.event.update','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) }}
{{ Form::hidden ('id', $event->id) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Update Event</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('name')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('name', 'name',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('name', $event->name, array('class' => 'form-control','maxlength' => '255', 'required')) }}
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('body', 'Body',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::textarea('body', $event->body, array('class' => 'form-control ckeditor', 'required')) }}
                <span class="help-block">{{ $errors->first('body') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('start')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('start', 'Start',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('start', \Carbon\Carbon::parse($event->start)->format('Y-m-d'), array('class' => 'form-control datepicker','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('start') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('end')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('end', 'End',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('end', \Carbon\Carbon::parse($event->end)->format('Y-m-d'), array('class' => 'form-control datepicker','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('end') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('logo')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('logo', 'Logo',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10 input-group" style="padding: 0 15px;">
                @if(isset($event->logo))
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                    </a>
                </span>
                {{ Form::text('logo', $event->logo, array('class' => 'form-control', 'id' => "thumbnail"))}}
                @else
                {{ Form::text('logo', $event->logo, array('class' => 'form-control', 'id' => "thumbnail", 'required'))}}
                @endif
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <img id="holder" style="margin-top:15px;max-height:100px;">
            </div>
            <span class="help-block">{{ $errors->first('logo') }}</span>
            @if(isset($event->logo))
            <div class="col-sm-2"></div>
            <div class="col-sm-10 voc">
                <img src="{{ URL::to($event->logo) }}" class="team-image"/>
            </div>
            @endif
        </div>

        <div class="form-group <?php if ($errors->first('location')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('location', 'Location',array('class'=>'control-label')) }}
            </div>
            
            <input type="hidden" value="{{ (!empty($event->street_number)) ? $event->street_number : '' }}" name="street_number" class="field" id="street_number"></input>
            <input type="hidden" value="{{ (!empty($event->route)) ? $event->route : '' }}" name="route" class="field" id="route"></input>
            <input type="hidden" value="{{ (!empty($event->locality)) ? $event->locality : '' }}" name="locality" class="field" id="locality"></input>
            <input type="hidden" value="{{ (!empty($event->neighborhood)) ? $event->neighborhood : '' }}" name="neighborhood" class="field" id="neighborhood"></input>
            <input type="hidden" value="{{ (!empty($event->district)) ? $event->district : '' }}" name="district" class="field" id="administrative_area_level_2"></input>
            <input type="hidden" value="{{ (!empty($event->state)) ? $event->state : '' }}" name="state" class="field" id="administrative_area_level_1"></input>
            <input type="hidden" value="{{ (!empty($event->country)) ? $event->country : '' }}" name="country" class="field" id="country"></input>
            <input type="hidden" value="{{ (!empty($event->postal_code)) ? $event->postal_code : '' }}" name="postal_code" class="field" id="postal_code"></input>

            <div class="col-sm-10">
                {{ Form::text('location', $event->location, array('class' => 'form-control', 'required', 'id' => 'txtPlaces','placeholder'=>"Enter a location")) }}
                {{ Form::hidden('latitude', $event->latitude, array('id' => 'latitude')) }}
                {{ Form::hidden('longitude', $event->longitude, array('id' => 'longitude')) }}
                <span class="help-block">{{ $errors->first('location') }}</span>
            </div>
        </div>

    </div>
    <!-- /.box-body -->


    <?php
    if (!empty($sharing)) {
        ?>

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Post Options</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'General',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==1)
                        {{ Form::checkbox('sharingOptions[]', $share->id, $event->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'Share',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==2)
                        {{ Form::checkbox('sharingOptions[]', $share->id, $event->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="box-body">
        <div class="pull-right">
            @if(isset($event->cover_image))
            @else
            @endif
            {{ $errors->first('cover_image') }}
            @if(isset($event->cover_image))
            @endif
            {{ Form::submit(trans('Save'), ['class' => 'btn btn-success']) }}
        </div>
        {{ Form::close() }}
        <div class="clearfix"></div>
    </div>
</div>

@endsection
@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGJ5EazH2m_do6_Svm7ZWn2DGB3g-qLOg&sensor=false&libraries=places" type="text/javascript"></script>
<script src="{{url('/')}}/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
$(document).ready(function () {
    $('#lfm').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});

    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      neighborhood: 'long_name',
      administrative_area_level_2: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    var lat = $('#latitude').val();
    var long = $('#longitude').val();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
            $('#latitude').val(latitude);
            $('#longitude').val(longitude);

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }

        });
    });
    $(function () {
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd'
        });
    });
});
</script>
@endsection 