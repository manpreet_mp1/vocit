@extends('frontend.layouts.voc_dashboard')
@section ('title', 'Event')
@section('page-header')
<h1>
    Event
</h1>
@endsection
@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">All Event</h3>
        <div class="box-tools pull-right">
            <a href="{{ route('frontend.event.create') }}" class="btn btn-primary">Create Event</a>
        </div>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="marketing-table" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th class='title-th'>Name</th>
                        <th>Body</th>
                        <th>Start</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($event->all() as $index => $field)
                    @php
                    $commingStartDate = $field->start;
                    $formatedStartDate = date("F j, Y",strtotime(str_replace('-','/', $commingStartDate)));
                    @endphp
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td class="event-name">{{ $field->name }}</td>
                        <td class="event-body">{!! $field->body !!}</td>
                        <td class="">{{ $formatedStartDate }}</td>
                        <td>
                            <a href="{{ route('frontend.event.edit',$field->id) }}" class="label label-success"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i></a>
                            <a href="{{ route('frontend.event.delete',$field->id) }}" class="label label-danger delete"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i></a>
                        </td>
                    </tr>
                    @empty
                    <tr><td colspan="5">No events added.</td></tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection 

@section('after-scripts')
@endsection