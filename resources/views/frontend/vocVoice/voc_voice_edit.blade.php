@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Update Voice')
@section('page-header')
<h1>
    Update Voice
</h1>
@stop
@section('content')
{{ Form::open(['route'=>'frontend.voice.update','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','enctype' => "multipart/form-data"]) }}
{{ Form::hidden('id', $voices->id) }}


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Update Voice</h3>
    </div><!-- /.box-header -->


    <div class="box-body">
        <div class="form-group <?php if ($errors->first('name')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('name', 'Name',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('name', $voices->name, array('class' => 'form-control','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('profile_image')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('profile_image', 'Profile Image',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10 input-group" style="padding: 0 15px;">
                @if(isset($voices->profile_image))
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                    </a>
                </span>
                {{ Form::text('profile_image', $voices->profile_image, array('class' => 'form-control', 'id' => "thumbnail"))}}
                @else
                {{ Form::text('profile_image', $voices->profile_image, array('class' => 'form-control', 'id' => "thumbnail", 'required'))}}
                @endif
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <img id="holder" style="margin-top:15px;max-height:100px;">
            </div>
            <span class="help-block">{{ $errors->first('profile_image') }}</span>
            @if(isset($voices->profile_image))
            <div class="col-sm-2"></div>
            <div class="col-sm-10 voc">
                <img src="{{ URL::to($voices->profile_image) }}" class="team-image"/>
            </div>
            @endif
        </div>

        <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('body', 'Body',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::textarea('body', $voices->body, array('class' => 'form-control ckeditor','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('body') }}</span>
            </div>
        </div>
    </div>


    <?php
    if (!empty($sharing)) {
        ?>

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Post Options</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'General',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                            @if($share->type==1)
                            {{ Form::checkbox('sharingOptions[]', $share->id, $voices->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'Share',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                            @if($share->type==2)
                                {{ Form::checkbox('sharingOptions[]', $share->id,$voices->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="box-body">
        <div class="pull-right">
            @if(isset( $voices->id))
            {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
            @else
            {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
            @endif
        </div>
        {{ Form::close() }}
        <div class="clearfix"></div>
    </div>
</div>

@if(count($errors) > 0)
@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{$error}}
</div>
@endforeach
@endif

@endsection

@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script src="{{url('/')}}/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
$(document).ready(function () {
    $('#lfm').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});
});
</script>
@endsection