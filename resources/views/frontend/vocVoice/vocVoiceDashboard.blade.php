@extends('frontend.layouts.voc_dashboard')
@section ('title', 'Voice')
@section('page-header')
<h1>
    Voice
</h1>
@endsection
@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">All Voice</h3>
        <div class="box-tools pull-right">
            <a href="{{ route('frontend.voice.create') }}" class="btn btn-primary">Create Voice</a>
        </div>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="marketing-table" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th class='title-th'>Name</th>
                        <th>Body</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($voices as $index=>$voice)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td class="event-name">{{ $voice->name }}</td>
                        <td class="event-body">{!! $voice->body !!}</td>
                        <td>
                            <a href="{{ route('frontend.voice.edit',$voice->id) }}" class="label label-success"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i></a>
                            <a href="{{ route('frontend.voice.delete',$voice->id) }}" class="label label-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i></a>
                        </td>
                    </tr>
                     @empty
                    <tr><td colspan="4">No voice added.</td></tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteMarketing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Voc delete</h4>
            </div>
            @if(!empty($voice))
            {!! Form::open(['id' => 'marketing_delete', 'method' => 'post', 'route'('frontend.voice.delete',$voice->id)]) !!}
            <div class="modal-body">
                <p>Do you want to delete "<span id='market-name'></span>" market?</p>
                {!! Form::hidden('id','',['id'=>'hidden_marketing_id']) !!}
            </div>
            <div class="modal-footer">
                {!! Form::reset('Cancel',['class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                {!! Form::submit(trans('labels.general.yes'),['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
            @endif
        </div>
    </div>
</div>
@endsection 
@section('after-scripts-end')
<script>
    $(function () {
        $('.delete_marketing').on('click', function (e) {
            var $this = $(this);
            $('#hidden_marketing_id').val($this.attr('data-marketing_id'));
            $('#market-name').text($this.attr('data-marketing_title'));
            $('#deleteMarketing').modal(show);
            e.preventDefault();
        });
    });
</script>
@stop
