@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Site Builder')
@section('page-header')
<h1>
    Site Builder
</h1>
@stop
@section('content')
<div class="box wide-box">

    <div class="box-body col-xs-8 col-sm-8">
        Preview page here
    </div>

    <div class="box-body col-xs-4 col-sm-4 box-body-right">
        <div data-type="content-block" class="page-block-title col-xs-6 col-sm-6">
            Content Blocks
            <span>Click to add to template</span>
        </div>
        <div data-type="page-settings" class="page-block-title col-xs-6 col-sm-6 active">
            Page Settings
            <span>Title, Logo, Description...</span>
        </div>
        <div class="clearfix"></div>

        <div class="page-block-content content-block" style="display:none;">
            Content Blocks contents
        </div>
        <div class="page-block-content page-settings">
            {{ Form::open([ 'route'=>'frontend.voice.vocUpdate','class' => '', 'role' => 'form', 'method' => 'post','enctype' => "multipart/form-data"]) }}
            {{ Form::hidden('id', $voc->id) }}
            <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
                {{ Form::label('title', 'Title',array('class'=>'control-label')) }}
                {{ Form::text('title', $voc->title , array('class' => 'form-control','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('title') }}</span>
            </div>

            <div class="form-group <?php if ($errors->first('logo')) echo ' has-error'; ?>">
                <div class="row">
                    {{ Form::label('logo','Logo', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-sm-10">
                        @if(isset($voc->logo))
                        <input type="file" name="logo" class="control-label-input"/>
                        @else
                        <input type="file" name="logo" class="control-label-input" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('image') }}</span>
                    </div>
                    @if(isset($voc->logo))
                    <div class="col-sm-10 voc">
                        <img src="{{ URL::to('/uploads').'/'.$voc->logo }}" class="voc-image"/>
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group <?php if ($errors->first('image')) echo ' has-error'; ?>">
                <div class="row">
                    {{ Form::label('image','Cover Image', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-sm-10">
                        @if(isset($voc->image))
                        <input type="file" name="image" class="control-label-input"/>
                        @else
                        <input type="file" name="image" class="control-label-input" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('image') }}</span>
                    </div>

                    @if(isset($voc->image))
                    <div class="col-sm-10 voc">
                        <img src="{{ URL::to('/uploads').'/'.$voc->image }}" class="voc-image"/>
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group <?php if ($errors->first('information')) echo ' has-error'; ?>">
                {{ Form::label('information', 'Brief Description',array('class'=>'control-label')) }}
                {{ Form::textarea('information', $voc->information , array('class' => 'form-control ckeditor', 'id'=>'ckeditor','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('information') }}</span>
            </div>

            <div class="pull-right">
                @if(isset( $voc->id))
                {{ Form::submit('Update', ['class' => 'btn btn-success']) }}
                @else
                {{ Form::submit(trans('buttons.general.save'), ['class' => 'btn btn-success']) }}
                @endif
            </div>
            {{ Form::close() }}
            <div class="clearfix"></div>
        </div>
    </div>

</div>

@endsection

@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace('ckeditor').config.allowedContent = true;
//Hide sidebar when this page opens
$(window).load(function () {
    if ($(window).width() > 768) {
        $('.sidebar-toggle').click();
    }
});
//END
</script>
{{ Html::script(mix('js/voc_dashboard.js')) }}
@endsection