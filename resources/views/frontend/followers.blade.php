@extends('frontend.layouts.voc_dashboard')

@section('page-header')
<h1>
    Voc Followers
</h1>
@endsection
@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Followers</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="marketing-table" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Since when</th>
                        <th>Select to Contact</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($vocFollowers as $vocFollower)
                    <tr>
                         <td>@if (($vocFollower['user']['first_name']) && ($vocFollower['user']['last_name']))
                             {{ $vocFollower['user']['first_name'] . ' ' . $vocFollower['user']['last_name'] }}
                            @else
                             {{ $vocFollower['user']['email'] }}
                            @endif</td>
                        <td>@if(($vocFollower['user']['profile']['city']) && ($vocFollower['user']['profile']['state']))
                            {{ $vocFollower['user']['profile']['city'] .', '. $vocFollower['user']['profile']['state'] }}
                            @elseif($vocFollower['user']['profile']['city'])
                            {{ $vocFollower['user']['profile']['city'] }}
                            @elseif($vocFollower['user']['profile']['state'])
                            {{ $vocFollower['user']['profile']['state'] }}
                            @endif</td>
                        <td>{{ $vocFollower['created_at'] ? $vocFollower->created_at->format('jS F, Y g:i a') : '' }}</td>
                        <td>
                            <a href="#" class="label label-success">Contact</a>
                        </td>
                    </tr>
                    @empty
                    <tr><td colspan="4">No followers till now.</td></tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteMarketing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Voc delete</h4>
            </div>
            @if(!empty($voice))
            {!! Form::open(['id' => 'marketing_delete', 'method' => 'post', 'route'('frontend.voice.delete',$voice->id)]) !!}
            <div class="modal-body">
                <p>Do you want to delete "<span id='market-name'></span>" market?</p>
                {!! Form::hidden('id','',['id'=>'hidden_marketing_id']) !!}
            </div>
            <div class="modal-footer">
                {!! Form::reset('Cancel',['class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                {!! Form::submit(trans('labels.general.yes'),['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
            @endif
        </div>
    </div>
</div>
@endsection
@section('after-scripts-end')
<script>
    $(function () {
        $('.delete_marketing').on('click', function (e) {
            var $this = $(this);
            $('#hidden_marketing_id').val($this.attr('data-marketing_id'));
            $('#market-name').text($this.attr('data-marketing_title'));
            $('#deleteMarketing').modal(show);
            e.preventDefault();
        });
    });
</script>
@stop
