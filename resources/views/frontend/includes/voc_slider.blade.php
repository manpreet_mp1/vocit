<div class="container-fluid" id="all-vocs-slide">
    <?php
    if (isset($vocs) && !empty($vocs)) {
        $voc_show = 'lesser-vocs';
        if (count($vocs) >= 5) {
            $voc_show = 'visible-xs-block';
        }

        if (count($vocs) >= 5) {
            ?> 
            <div class="slider_outer_div hidden-xs">
                <ul id="slider_div">
                    @foreach($vocs as $voc)
                    <li>
                        <?php
                        if ($voc->id == 1) {
                            $link = url('') . '/about';
                        } else {
                            $link = route('frontend.getSingleVoc', $voc->id);
                        }
                        ?>
                        <a href="{{ $link }}">
                            <img src="{{ config('constant.voc_image_path').$voc->logo }}" alt="{{ $voc->title }}" class="voc-logo" />
                            <div class="image_div" style="background-image:url({{ config('constant.voc_image_path').$voc->image }})"></div>
                        </a>
                        <div class="content">
                            <a href="{{ $link }}"><h2>{{ $voc->title }}</h2></a>
                            <p class="right_div">{{ substr(strip_tags($voc->information),0,110) . "..." }}</p>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        <?php } ?>

        <div class="container">
            <div class="owl-carousel all_slider_div {{$voc_show}}">
                @foreach($vocs as $voc)
                <div class="owl-item">
                    <?php
                    if ($voc->id == 1) {
                        $link = url('') . '/about';
                    } else {
                        $link = route('frontend.getSingleVoc', $voc->id);
                    }
                    ?>
                    <a href="{{ $link }}">
                        <img src="{{ config('constant.voc_image_path').$voc->logo }}" alt="{{ $voc->title }}" class="voc-logo" />
                        <div class="image_div" style="background-image:url({{ config('constant.voc_image_path').$voc->image }})"></div>
                    </a>
                    <div class="content">
                        <a href="{{ $link }}"><h2>{{ $voc->title }}</h2></a>
                        <p class="right_div">{{ substr(strip_tags($voc->information),0,110) . "..." }}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    <?php } ?>
</div>