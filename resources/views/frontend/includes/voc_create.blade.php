@extends('frontend.layouts.frontend')

@section('title')
Create Voc
@stop

@section('after-styles')
{{ Html::style(elixir('css/home.css')) }}
@stop

@section('content')
<div class="container sign-up">
    <div class="row">
        <div class="col-sm-12 all-divs-row">
            <div class="col-sm-7 vocs text-top">
                <div class="" id="voc-create">
                    <div class="create-voc">
                        <!-- Create Voc content-->
                        <div class="create-content"> 
                            <div class="create-header">
                                <h4 class="create-title">Create a <span>VOC</span></h4>
                            </div>
                            <div class="create-body" id="voc-create">
                                <div class="form-response"></div>
                                {{ Form::open(['route'=>'frontend.storeInfo', 'id'=>'createVoc']) }}
                                <div class="input-outer">
                                    <label> Name your VOC </label>
                                    {{ Form::text('name', null,['placeholder' => 'Name @Voc', 'class'=>'form-input','required' => 'required','id'=>'name']) }}
                                </div>
                                <div class="input-outer">
                                    <label>Primary region of focus</label>
                                    {{ Form::text('zip_code', null, ['placeholder' => 'Zip Code', 'class'=>'form-input', 'required' => 'required','id'=>'zip_code']) }}
                                </div>
                                <div class='continue-button'>
                                    <button type="submit" class="btn btn-default top-margin-set create-btn btn-red btn-create">Continue</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="" id="basic-info">
                    <div class="create-voc">
                        <!--Create content-->
                        <div class="create-content">
                            <div class="create-header">
                                <h4 class="create-title">Basic Information</h4>
                            </div>
                            <div class="create-body" id="storeProfile">
                                <div class="form-response"></div>
                                {{ Form::open(['route'=>'frontend.storeUser', 'id'=>'vocProfile']) }}
                                {{ Form::hidden('user_role', config('constant.roles_inverse.voc')) }}
                                <div class="input-outer">
                                    {{ Form::email('email', null, ['placeholder' => 'Professional Email', 'class'=>'form-input','required' => 'required','id'=>'email']) }}
                                </div>
                                <div class="input-outer">
                                    {{ Form::text('phone_number', null, ['id'=>'phone_number', 'class'=>'form-input','placeholder' => 'Phone Number','required' => 'required']) }}
                                </div>
                                <div class="input-outer">
                                    {{ Form::password('password',['id'=>'password', 'class'=>'form-input','placeholder' => 'Create Password','required' => 'required']) }}
                                </div>
                                <div class="input-outer">
                                    {{ Form::password('password_confirmation', ['id'=>'password_confirmation', 'class'=>'form-input','placeholder' => 'Confirm Password','required' => 'required']) }}
                                </div>
                                <div class='continue-button'>
                                    <button type="submit" class="btn btn-default top-margin-set create-btn btn-red btn-create">Create</button>
                                </div>
                                {{ Form::close() }}

                                <p class='tag-line'>Let your voice be heard.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="topic">
                    <?php
                    if (isset($vocCategoriesSignUpList) && !empty($vocCategoriesSignUpList)) {
                        $topicCount = count($vocCategoriesSignUpList);
                        if ($topicCount <= 10) {
                            $width = "width-min";
                        } elseif ($topicCount >= 11 && $topicCount <= 15) {
                            $width = "width-medium";
                        } elseif ($topicCount >= 15 && $topicCount <= 20) {
                            $width = "width-max";
                        }
                    }
                    ?>
                    <div class="create-voc create-width <?php
                    if (isset($vocCategoriesSignUpList) && !empty($vocCategoriesSignUpList)) {
                        echo "$width";
                    }
                    ?>">
                        <!--Create content-->
                        <div class="create-content">
                            <div class="create-header">
                                <h4 class="create-title">Choose your VOC topic </h4>
                            </div>
                            <div class="create-body" id="topicModal">
                                <div class="form-response"></div>
                                {{ Form::open(['route'=>'frontend.storeVocCategory', 'id'=>'voc-category-form']) }}
                                <!------------------------------------------------------------------------>
                                    <div class="card collapse-icon accordion-icon-rotate">
                                        <?php
                                        if (isset($vocCategoriesSignUpList) && !empty($vocCategoriesSignUpList)) {
                                            $count = 0;
                                            foreach ($vocCategoriesSignUpList as $category) {
                                                ?>
                                                <div class="card-header">
                                                    <a role="button" class="card-title lead btn btn-red btn-width cat-subcat-selection">
                                                    <input type="checkbox" name="voc_categories[]" value="{{$category->id}}" class="category-check">
                                                        {{ $category->name }}
                                                    </a>
                                                </div>
                                            <?php }
                                        } else {
                                            ?>
                                            <span>No Categories found.</span>
                                        <?php } ?>
                                    </div>
                                <!-------------------------------------------------->
                                <div class='continue-button'>
                                    <button type="submit" class="btn btn-default top-margin-set next-btn btn-red btn-create">Next</button>
                                </div>
                                {{ Form::close() }}
                                <p class='tag-line'> Let your Voice be heard. </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="" id="template">
                    <div class="create-voc  create-width">
                        <!--Create content-->
                        <div class="create-content">
                            <div class="create-header">
                                <h4 class="create-title">Choose your VOC Template </h4>
                            </div>
                            <div class="create-body" id="templateModal">
                                <div class="templates">
                                    <?php
                                    if (isset($template) && !empty($template)) {
                                        foreach ($template as $data) {
                                            ?>
                                            <div class="each-template">
                                                <img src="{{ asset('/img/frontend/template/'.$data['cover_image'])}}" class="voc_template" data-template_id="{{$data['id']}}"/>
                                                <span class="success-tick"><i class="fa fa-check"></i></span>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class='continue-button'>
                                    <button type="submit" class="btn btn-default top-margin-set next-btn btn-red btn-create">Next</button>
                                </div>
                                <p class='tag-line'> Take control of your presence. </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="" id="site-builder">
                    <div class="create-voc create-width  site-builder-format">
                        <!--Create content-->
                        <div class="create-content site-builder-body">
                            <div class="create-header">
                            </div>
                            <div class="create-body">
                                <div class="form-response"></div>
                                {{ Form::open(['route' =>'frontend.storeTemplate','id'=>'siteBuilerForm', 'enctype' => "multipart/form-data"]) }}
                                <div class="input-outer">
                                    {{ Form::text('title', null, ['placeholder' => "VOC's title", 'class'=>'form-input','required'=>'required']) }}
                                </div>
                                <div class="input-outer">
                                    <label for="add_logo">Add your logo</label>
                                    {{ Form::file('logo', null, ['class' => 'logo', 'id'=>'add_logo', 'required'=>'required']) }}
                                </div>
                                <div class="input-outer">
                                    <label for="add_cover_image">Add your cover image</label>
                                    {{ Form::file('cover_image', null, ['class' => 'file','required'=>'required', 'id'=>'add_cover_image']) }}
                                </div>
                                <div class="input-outer">
                                    {{ Form::textarea('information', null, ['id' => 'rich-editor', 'placeholder'=>'Brief description about your Voc']) }}
                                </div>
                                <div class='continue-button'>
                                    <button type="submit" class="btn btn-default top-margin-set btn-red btn-create">Done</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 vocs outer-voc-content">
                <div class="inner-voc-content"></div>
                <div>
                    <h4>Company or Campaigner</h4>
                    <p>Do you have a Voice that people want to follow? Create your own VOC and let the world hear you through you're
                        own marketing and campaigning website.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script>
var storeTemplateIdUrl = "{{ route('frontend.storeVocProfileTemplateId') }}";
</script>
{!! Html::script(mix('js/voc_create.js')) !!}
@endsection