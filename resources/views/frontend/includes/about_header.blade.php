<header class="white-bg">
    <div class="container-fluid p-0 header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12 padding0m">
                    <div class="float-left logo">
                        <a href="{{ route('frontend.home') }}">{{ Html::image("img/frontend/vocit_logo.png" ,'vocit_logo', ["class" => "logo"]) }}</a>
                    </div>
                    <div class="float-left">
                        <p>Rock it with <span>Social Websites</span></p>
                    </div>
                    <?php if (!(Auth::user())) { ?>
                        <div class="dropdown pull-right header-home-button">  
                            <ul class="top_btns">
                                <li><a href="#" class="btn btn-orange login_btn text-uppercase"  data-toggle="modal" data-target="#login-modal">Follow</a></li>
                            </ul> 
                        </div>
                    <?php } ?> 
                </div>
            </div>
        </div>
    </div>
</header>