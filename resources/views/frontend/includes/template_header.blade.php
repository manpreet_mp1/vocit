<section class="white-bg">
    <div class="container-fluid p-0 header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <img src="{{ url('/').config('constant.voc_image_path').$voc->logo }}" class="voc-logo">

                    @if (access()->hasRole(config('constant.roles.voc_dex')) || Auth::user() == null)
                    <button type="button" class="btn btn-default-black pull-right follow-voc-link" data-toggle="modal" <?php if (Auth::user() == null) { ?>
                            data-target="login-modal" <?php } if (access()->hasRole(config('constant.roles.voc_dex'))) { ?> data-target="voc-template" <?php } ?>>Follow</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!--<div class="modal fade" id="voc-template" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
         Modal content
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">What do you want from {{ $voc->title }}?</h3>
            </div>

            <div class="modal-body" id="topicModal">
                <div class="form-response"></div>
                {{ Form::open(['route'=>'frontend.vocFollow', 'id'=>'voc-follow-form','method' => 'post']) }}
                <div class='continue-button' id="voc-btn-wraper">
                    <div class='options' id="voc-topic-choose">
                        {{ Form::hidden('voc_id', $voc->id, array('class' => 'voc_id')) }}
                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}
                        <button type="button" class="btn btn-default btn-red btn-width cat-subcat-selection seclected {{ $followersData->search("App\Models\VocDashboard\Job") !== false? 'success':'' }}" data-model="App\Models\VocDashboard\Job">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Job Postings
                        </button>
                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}
                        <button type="button" class="btn btn-default btn-red btn-width cat-subcat-selection seclected {{ $followersData->search("App\Models\Voc\VocEvent") !== false? 'success':'' }}" data-model="App\Models\Voc\VocEvent">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Events
                        </button>
                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}

                        <button type="button" class="btn btn-default btn-red btn-width cat-subcat-selection seclected {{ $followersData->search("App\Models\VocDashboard\Update") !== false? 'success':'' }}" data-model="App\Models\VocDashboard\Update">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Updates
                        </button>

                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}
                        <button type="button" class="btn btn-default btn-red btn-width cat-subcat-selection seclected {{ $followersData->search("App\Models\Voc\VocPromotion") !== false? 'success':'' }}" data-model="App\Models\Voc\VocPromotion">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Promotions
                        </button>

                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}
                        <button type="button" class="btn btn-default btn-red btn-width cat-subcat-selection seclected {{ $followersData->search("App\Models\VocDashboard\Voice") !== false? 'success':'' }}" data-model="App\Models\VocDashboard\Voice">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Voice
                        </button>

                        <button type="button" class="btn btn-default btn-red btn-width" id="all-sel-btn">
                            All
                        </button>

                    </div>
                </div>
                <div class='continue-button'>
                    <button type="submit" id="voc-follow-submit-btn" class="btn btn-default top-margin-set next-btn btn-red">Follow</button>
                </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>-->


<div id="follow-outer-popup">
    <div class="overlay overlay-scale">
        <button type="button" class="overlay-close">Close</button>
        <nav>
            <div class="container">
                <h2>What do you want from {{ $voc->title }}?</h2>
                {{ Form::open(['route'=>'frontend.vocFollow', 'id'=>'voc-follow-form','method' => 'post']) }}
                {{ Form::hidden('voc_id', $voc->id, array('class' => 'voc_id')) }}
                <ul>
                    <li>
                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}
                        <a href="#" class="follow-selection {{ $followersData->search("App\Models\VocDashboard\Job") !== false? 'success':'' }}" data-model="App\Models\VocDashboard\Job" >
                            Job Postings
                        </a>
                    </li>
                    <li>
                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}
                        <a href="#" class="follow-selection {{ $followersData->search("App\Models\Voc\VocEvent") !== false? 'success':'' }}" data-model="App\Models\Voc\VocEvent" >
                            Events
                        </a>
                    </li>
                    <li>
                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}
                        <a href="#" class="follow-selection {{ $followersData->search("App\Models\VocDashboard\Update") !== false? 'success':'' }}" data-model="App\Models\VocDashboard\Update" >
                            Updates
                        </a>
                    </li>
                    <li>
                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}
                        <a href="#" class="follow-selection {{ $followersData->search("App\Models\Voc\VocPromotion") !== false? 'success':'' }}" data-model="App\Models\Voc\VocPromotion" >
                            Promotions
                        </a>
                    </li>
                    <li>
                        {{ Form::hidden('modals', '', array('class' => 'template-modals')) }}
                        <a href="#" class="follow-selection {{ $followersData->search("App\Models\VocDashboard\Voice") !== false? 'success':'' }}" data-model="App\Models\VocDashboard\Voice" >
                            Voice
                        </a>
                    </li>
                    <li>
                        <a href="#" id="all-sel-btn">
                            All
                        </a>
                    </li>
                </ul>
                <button type="submit" class="btn btn-default-black">Follow</button>
                {{ Form::close() }}
            </div>
        </nav>
    </div>
</div>