<div class="container-fluid footer-blue-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-6">
                <ul class="list-inline">
                    <li><a href="{{ route('frontend.termsOfUse') }}">Terms</a></li>
                    <li><a href="{{ route('frontend.privacy') }}">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-xs-6">
                <p>&copy; 2018 VOC IT. All rights reserved.</p>
            </div>
        </div>
    </div>
</div>
@include('frontend.includes.login')  
@include('frontend.includes.forget_password')