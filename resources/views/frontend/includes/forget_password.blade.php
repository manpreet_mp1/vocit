<div id="forgot-outer-popup">
    <div class="overlay overlay-scale">
        <button type="button" class="overlay-close">Close</button>
        <nav>
            <div class="container">
                <div class="col-sm-9 col-md-7 col-lg-6 login-content">
                    <div class="head">
                        <h3>Forgot Password</h3>
                    </div>
                    <div class="login-form">
                        <div class="form-response"></div>
                        {{ Form::open(['route' => 'frontend.auth.password.email.post', 'class' => '', 'id'=>'forget-password-form']) }}

                        <div class="form-group">
                            <label>Email</label>
                            {{ Form::email('email', null, ['placeholder' => 'Enter Your Email', 'class'=>'form-input form-control','required'=>'required']) }}
                        </div>
                        <div class="clearfix"></div>
                        <div class='continue-button'>
                            <button type="submit" class="btn btn-default-black">Submit</button>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>