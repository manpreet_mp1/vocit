<!--  Essential META Tags -->
<meta property="og:title" content="{{$title}}">
<meta property="og:description" content="{{$description}}">
<meta property="og:image" content="{{$image}}">
<meta property="og:url" content="{{$url}}">
<meta name="twitter:card" content="{{$image}}">
<meta property="og:type" content="website" />

<!--  Non-Essential, But Recommended -->

<meta property="og:site_name" content="{{$title}}">
<meta name="twitter:image:alt" content="{{$tagline}}">


<!--  Non-Essential, But Required for Analytics -->

<meta property="fb:app_id" content="179129739593908" />
<meta name="twitter:site" content="@VocitOfficial">