<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
<!--        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{- access()->user()->picture --}}" class="img-circle" alt="User Image" />
            </div>pull-left
            <div class="pull-left info">
                <p>{{-- access()->user()->full_name }--}}</p>
                 Status 
                <a href="#"><i class="fa fa-circle text-success"></i> {{-- trans('strings.backend.general.status.online') --}}</a>
            </div>pull-left
        </div>-->
        <!--user-panel-->

        <!-- Sidebar Menu -->
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!--<li class="header">{{-- trans('menus.backend.sidebar.general') --}}</li>-->
            <li class="{{ active_class(Active::checkUriPattern('vocDashboard/dashboard')) }} treeview">
                <a href="{{ route('frontend.vocDashboard.vocDashboard') }}">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('voice/vocEdit')) }} treeview">
                <a href="{{ route('frontend.voice.vocEdit') }}">
                    <i class="fa fa-building-o" aria-hidden="true"></i>
                    <span>Site Builder</span>
                </a>

            </li>
            <li class="{{ active_class(Active::checkUriPattern(['vocDashboard','jobPosting/dashboard','event/show','promotion/show','update/dashboard','voice/dashboard','jobPosting/create','jobPosting/edit/*','event/create','event/edit/*','update/create','update/edit/*','promotion/create','promotion/edit/*','voice/edit/*','voice/create'])) }} treeview">
                <a href="#">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span>Post Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern(['vocDashboard','jobPosting/dashboard','event/show','promotion/show','update/dashboard','voice/dashboard','jobPosting/create','jobPosting/edit/*','event/create','event/edit/*','update/create','update/edit/*','promotion/create','promotion/edit/*','voice/edit/*','voice/create']), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern(['vocDashboard','jobPosting/dashboard','event/show','promotion/show','update/dashboard','voice/dashboard','jobPosting/create','jobPosting/edit/*','event/create','event/edit/*','update/create','update/edit/*','promotion/create','promotion/edit/*','voice/edit/*','voice/create']), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern(['jobPosting/dashboard','jobPosting/create','jobPosting/edit/*'])) }}">
                        <a href="{{ route('frontend.jobPosting.vocDashboard') }}">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            <span>Job Posting</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern(['event/show','event/create','event/edit/*'])) }}">
                        <a href="{{ route('frontend.event.show') }}">
                            <i class="fa fa-tasks" aria-hidden="true"></i>
                            <span>Event</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern(['update/dashboard','update/create','update/edit/*'])) }}">
                        <a href="{{ route('frontend.update.vocUpdateDashboard') }}">
                            <i class="fa fa-upload" aria-hidden="true"></i>
                            <span>Update</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern(['promotion/show','promotion/create','promotion/edit/*'])) }}">
                        <a href="{{ route('frontend.promotion.show') }}">
                            <i class="fa fa-suitcase"></i>
                            <span>Promotion</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern(['voice/dashboard','voice/edit/*','voice/create'])) }}">
                        <a href="{{ route('frontend.voice.dashboard') }}">
                            <i class="fa fa-th-large" aria-hidden="true"></i>
                            <span>Voice</span>
                        </a>
                    </li>
                </ul>
            </li>
<!--            <li class="treeview">
                <a href="#">
                    <i class="fa fa-wrench" aria-hidden="true"></i>
                    <span>Tools & Widgets</span>
                </a>
            </li>-->
            <li class="{{ active_class(Active::checkUriPattern('voice/template')) }} treeview">
                <a href="{{ route('frontend.voice.template') }}">
                    <i class="fa fa-file-text" aria-hidden="true"></i>
                    <span>Templates</span>
                </a>
            </li>
<!--            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o" aria-hidden="true"></i>
                    <span>Files</span>
                </a>
            </li>-->
            <li class="{{ active_class(Active::checkUriPattern('voice/followers')) }} treeview">
                <a href="{{ route('frontend.voice.followers') }}">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span>Followers</span>
                </a>
            </li>
        </ul>
    </section><!-- /.sidebar -->
</aside>