@extends('frontend.layouts.frontend')

@section('title')
Vocdex Create
@stop

@section('after-styles')
{{ Html::style(elixir('css/home.css')) }}
@stop

@section('content')
<style>
    .create-body .follow {
        width: 100%;
        display: inline-flex;
    }
</style>
<div class="container sign-up">
    <div class="row">
        <div class="col-sm-12 all-divs-row">
            <div class="col-sm-7 vocs text-top">
                <div class="" id="homepage-create">
                    <div class="create-voc">
                        <!-- Create Vocdex content-->
                        <div class="create-content">
                            <div class="create-header">
                                <h4 class="create-title">Create your VOCDEX Homepage</h4>
                            </div>
                            <div class="create-body">
                                <div class="form-response"></div>
                                {{ Form::open(['id'=>'createVocDex', 'route'=>'frontend.storeUser']) }}
                                {{ Form::hidden('user_role', config('constant.roles_inverse.voc_dex')) }}
                                <div class="form-group">
                                    <label> Email</label>
                                    {{ Form::email('email', null, ['placeholder' => 'Email', 'class'=>'form-input','required' => 'required','id'=>'email']) }}
                                </div>
                                <div class="form-group">
                                       <label> Email</label>
                                    {{ Form::text('zip_code', null, ['placeholder' => 'Zipcode', 'class'=>'form-input','required' => 'required','id'=>'zip_code']) }}
                                </div>
                                <div class="form-group">
                                       <label> Email</label>
                                    {{ Form::password('password', ['placeholder' => 'Create Password', 'class'=>'form-input','required' => 'required','id'=>'password']) }}
                                </div>
                                <div class="form-group">
                                       <label> Email</label>
                                    {{ Form::password('password_confirmation',['placeholder' => 'Confirm Password', 'class'=>'form-input','required' => 'required','id'=>'confirm_password']) }}
                                </div>
                                <div class='continue-button'>
                                    <button type="submit" class="btn btn-default btn-red btn-vocdex btn-create">Create</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                

                <div class="" id="preferences">
                    <div class="create-voc create-width">
                        <!--Create Vocdex content-->
                        <div class="create-content">
                            <div class="create-header">
                                <h4 class="create-title">Welcome<br> Tell us a little more</h4>
                            </div>
                            <div class="create-body follow">
                                <div class="form-response"></div>
                                {{ Form::open(['id'=>'storeVocDexSubCategory', 'route'=>'frontend.storeVocDexSubCategory']) }}
                                <div class="input-outer">
                                    <input name="vocdex_category[0]" class="form-input" placeholder="Favorite Food" required>
                                </div>
                                <div class="input-outer">
                                    <input name="vocdex_category[1]" class="form-input" placeholder="Favorite genre of music" required>
                                </div>
                                <div class="input-outer">
                                    <input name="vocdex_category[2]" class="form-input" placeholder="Favorite sport" required>
                                </div>
                                <div class="input-outer">
                                    <input name="vocdex_category[3]" class="form-input" placeholder="Hobbies" required>
                                </div>
                                <div class="input-outer">
                                    <input type="text" name="vocdex_category[4]" class="form-input" placeholder="Occupation" required>
                                </div>
                                <div class='continue-button'>
                                    <button type="submit" class="btn btn-default margin-set btn-vocdex btn-red btn-create">Done</button>
                                    <button type="button" class="btn btn-default margin-set btn-red btn-skip btn-create">Skip</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="" id="carousel">
                    <div class="create-voc create-width">
                        <!--Create Vocdex content-->
                        <div class="create-content">
                            <div class="create-header">
                                <h4 class="create-title">Recommended VOCS</h4>
                            </div>
                            <div class="create-body">
                                <div class="form-response"></div>
                                {{ Form::open(['id'=>'storeRecommendedVocs-form', 'route'=>'frontend.storeRecommendedVocs']) }}

                                <div class="carousel" id="voc-list-slider">
                                    <div class="slides">
                                    </div>
                                </div>

                                <div class='continue-button'>
                                    <button type="submit" class="btn btn-default margin-set btn-vocdex btn-red btn-create">Done</button>
                                    <button type="submit" class="btn btn-default margin-set btn-red btn-skip btn-create">Skip</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 vocs outer-voc-content">
                <div class="inner-voc-content"></div>
                <div>
                    <h4>User</h4>
                    <p>Do you want to be inspired by your favorite business, people and ideas? Follow your favorite VOC'S and received
                        instant posts to your home page.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('after-scripts')
<script>
    var searchSubCatUrl = "{{ route('frontend.searchCategory') }}";
</script>
{!! Html::script(mix('js/vocdex_create.js')) !!}
@endsection