<?php
if (count($vocPosts) > 0) {
    ?>
    @include('frontend.custom_template.posts.promotion')
    @include('frontend.custom_template.posts.voice')
    @include('frontend.custom_template.posts.jobs')
    @include('frontend.custom_template.posts.update')
    @include('frontend.custom_template.posts.event')
<?php } else { ?>
    <div class="voc-empty-message">
        <p>Discover vocs and follow them 
            <a href="{{route('frontend.home')}}">now</a>.</p>
    </div>
<?php } ?>
