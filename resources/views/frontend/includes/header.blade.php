<header>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12 header-box">
                    <div class="logo-box">
                        <a href="{{ route('frontend.home') }}">{{ Html::image("img/frontend/vocit_logo.png" ,'vocit_logo', ["class" => "logo"]) }}</a>
                    </div>
                    <div class="dropdown pull-right">  
                        <?php if (Auth::user()) { ?>
                            <ul class="nav_bar list-inline">
                                <li><a href="#" data-toggle="tooltip" title="Coming Soon" data-placement="bottom" class="basic-tooltip">Email</a></li>
                                <li><a href="#" data-toggle="tooltip" title="Coming Soon" data-placement="bottom" class="basic-tooltip">Blogs</a></li>
                                <li><a href="#" data-toggle="tooltip" title="Coming Soon" data-placement="bottom" class="basic-tooltip">Collections</a></li>
                                <li><a href="#" data-toggle="tooltip" title="Coming Soon" data-placement="bottom" class="basic-tooltip">Calendar</a></li>
                                <li class="hidden-md"><a href="#" data-toggle="tooltip" title="Coming Soon" data-placement="bottom" class="basic-tooltip">Wallet</a></li>
                                <li class="hidden-md"><a href="#" data-toggle="tooltip" title="Coming Soon" data-placement="bottom" class="basic-tooltip">Chat</a></li>
                            </ul> 
                        <?php } ?>

                        <ul class="top_btns list-inline">
                            <li class="dropdown">
                                <?php if (Auth::user()) { ?>
                                    <a href="#" class="dropdown-toggle signup_btn" data-toggle="dropdown">Hi {{ Auth::user()->first_name }} <span></span></a>
                                <?php } else { ?>
                                    <a href="{{ route('frontend.signup') }}" class="">Sign Up</a>
                                <?php } ?>
                                @if(Auth::check())
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @role(config('constant.roles.voc_dex'))
                                    <li>
                                        <a class="" href="{{ route('frontend.vocdex-home') }}">Vocdex Dashboard</a>
                                    </li>
                                    @endauth
                                    @role(config('constant.roles.voc'))
                                    <li>
                                        <a class="" href="{{ route('frontend.voice.dashboard') }}">Voc Dashboard</a>
                                    </li>
                                    @endauth
                                    <li><a class="" href="{{ route('frontend.auth.logout') }}">Logout</a></li>
                                </ul>
                                @endif
                            </li>
                            <?php if (!(Auth::user())) { ?>
                                <li>
                                    <a href="#" class="login_btn">Login</a>
                                </li>
                            <?php } ?>  
                            <li class="line-seprater"> <a href="{{ route('frontend.about') }}" class="learnmore"> Learn More</a></li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>