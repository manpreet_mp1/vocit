<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#frontend-navbar-collapse">
                <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            {{ link_to_route('frontend.home', app_name(), [], ['class' => 'navbar-brand']) }}
        </div><!--navbar-header-->

        <div class="collapse navbar-collapse" id="frontend-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if (! $logged_in_user)

                @if (config('access.users.registration'))
                @endif
                @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ $logged_in_user->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        @permission('view-backend')
                        <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                        @endauth
                        
                        <?php
                        $role_user = $logged_in_user->roles()->get();
                        if ($role_user[0]->id == 3) {
                            ?>
                            <li>{{ link_to_route('frontend.user.createVoc', 'Create Voc') }}</li>
                            <li>{{ link_to_route('frontend.user.createBlog', 'Blog') }}</li>
                        <?php } ?>

                        <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => active_class(Active::checkRoute('frontend.user.account')) ]) }}</li>
                        <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                    </ul>
                </li>
                @endif
            </ul>
        </div><!--navbar-collapse-->
    </div><!--container-->
</nav>