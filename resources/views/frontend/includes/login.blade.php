<div id="login-outer-popup">
    <div class="overlay overlay-scale">
        <button type="button" class="overlay-close">Close</button>
        <nav>
            <div class="container">
                <div class="col-sm-9 col-md-7 col-lg-6 login-content">
                    <div class="head">
                        <h3>Login</h3>
                    </div>
                    <div class="login-form">
                        <div class="form-response"></div>
                        {{ Form::open(['route'=>'frontend.auth.customlogin','id'=>'custom-login']) }}
                        <div class="form-group">
                            <label>Email</label>
                            {{ Form::email('email', null, ['placeholder' => 'Enter Your Email', 'class'=>'form-input form-control','required'=>'required']) }}
                        </div>
                        <div class="form-group"> 
                            <label>Password</label>
                            {{ Form::password('password', ['placeholder' => 'Enter Your Password', 'class'=>'form-input form-control','required'=>'required']) }}
                        </div>

                        <div class="clearfix"></div> 
                        <div class='continue-button'>
                            <button type="submit" class="btn btn-default-black">Login</button> 
                            
                            <div class="links">
                                <a href="#" id="forget-password-link">Forgot Your password?</a>
                                <p>Don't have an account?
                                    <a href="{{ route('frontend.signup') }}?#Voc"><span>Sign up as Voc</span></a> <i>or</i>
                                    <a href="{{ route('frontend.signup') }}?#VocDex"><span>VocDex</span></a> 
                                </p>
                            </div>
                        </div>
                        {{ Form::close() }} 
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>