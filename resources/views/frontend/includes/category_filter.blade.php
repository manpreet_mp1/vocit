<!--filter tab start-->
@if(isset($vocCategories) && !empty($vocCategories))
<div class="row">  
    <div class="all-vocs col-xs-12 col-sm-12 col-md-12 col-lg-12">    
        <div class="filter">
            <h3>categories</h3>
            <ul class="all_tab">
                @foreach($vocCategories as $category)
                <li class="voc-categories" data-categories_id="{{$category['id']}}">
                    <a href="{{ route('frontend.vocSearch') }}" class="btn-white">
                        <span>{{ $category['name'] }}</span>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif
<!--filter tab end-->