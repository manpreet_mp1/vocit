<?php
foreach ($recommended_vocs as $vocs) {
    if (!empty($vocs->image)) {
        ?>
        <div class="slideItem" data-href="{{$vocs->id}}">
            <div>
                <span>{{$vocs->title}}</span>
                <img src="{{ Url('/').config('constant.voc_image_path').$vocs->logo }}">
                <input type="hidden" data-scroll-page="1" class="scroll_voc_id" value="{{$vocs->id}}">
                <div class="actions">
                    <a href="#" class="btn btn-white">Follow</a>
                    <a href="{{ route('frontend.getSingleVoc',$vocs->id) }}" class="out-link" target="_blank"><i class="fa fa-briefcase" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="shadow">
                <div class="shadowLeft"></div>
                <div class="shadowMiddle"></div>
                <div class="shadowRight"></div>
            </div>
        </div>
        <?php
    }
}
?>