@extends('frontend.layouts.voc_dashboard')

@section('page-header')
@endsection

@section('content')
<div class="custom-level">
    <h1>Level of Customization</h1>
    <div class="col-sm-10 col-md-11 col-lg-10 level-no-outer">
        <?php $active = ''; ?>
        <div>
            <?php
            $ba_active = '';
            if ($voc->level_id == config('constant.services.basic')) {
                $ba_active = 'active';
            }
            ?>
            <div class="col-sm-6 col-md-3 level-no {{$ba_active}}">
                <a href="{{route("frontend.voice.vocEdit")}}" class="customization_option" data-custom="{{config('constant.services.basic')}}">
                    <h3>Basic</h3>
                </a>
            </div>
            <?php
            $pl_active = '';
            if ($voc->level_id == config('constant.services.plus')) {
                $pl_active = 'active';
            }
            ?>
            <div class="col-sm-6 col-md-3 level-no {{$pl_active}}">
                <a href="" class="basic-tooltip customization_option" data-toggle="tooltip" title="Coming Soon" data-placement="bottom">
                    <h3>Plus</h3>
                </a>
            </div>
            <?php
            $co_active = '';
            if ($voc->level_id == config('constant.services.create_your_own')) {
                $co_active = 'active';
            }
            ?>
            <div class="col-sm-6 col-md-3 level-no {{$co_active}}">
                <a href="{{route("frontend.voice.vocEdit")}}" class="customization_option" data-custom="{{config('constant.services.create_your_own')}}">
                    <h3>Code Your Own</h3>
                </a>
            </div>
            <?php
            $hi_active = '';
            if (isset($hireus_value) && $hireus_value == true) {
                $hi_active = 'active';
            }
            ?>
            <div class="col-sm-6 col-md-3 level-no {{$hi_active}}">
                <a href="#" id="hireUs">
                    <h3>Hire Us</h3>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Template</h3>
    </div>
    <div class="box-body">
        @if(isset($templates) && !empty ($templates))
        @foreach($templates as $template)
        <?php
        $class = '';
        if (isset($currentVocTemplate) && $currentVocTemplate == $template->id) {
            $class = 'active';
        }
        ?>
        <div class="col-sm-4 each-template">
            <div class="<?php echo $class; ?>">
                <img src="{{asset('img/frontend/template')}}/{{ $template->cover_image }}" class="img-responsive" />
                <h4>
                    <?php
                    if ($class == 'active') {
                        echo '<b>Active:</b>';
                    }
                    ?>
                    {{ $template->name }}</h4>
                <div class="box-tools pull-right activate">
                    <a href="{{ route('frontend.voice.templateStore',$template->id) }}" class="btn btn-primary">Activate</a>
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>
@endsection
@section('after-scripts')
<script>
    var frontendHireUs = '{{route("frontend.voice.hireUs")}}',
            templateCustomizationAddOwn = '{{route("frontend.voice.templateCustomizationAdd")}}';
</script>
@stop