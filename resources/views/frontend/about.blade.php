@extends('frontend.layouts.about') 

@section('title') 
About 
@stop 

@section('after-styles') 
{{ Html::style(elixir('css/about.css')) }} 
@stop 

@section('content')
<div class="hero-image">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="desc col-sm-6">
                    <p>
                        A <span>Social Platform</span> focused solely on the <span>Consumer</span> and <span>Business Relationship</span>.
                    </p>
                    <p><span>Sign up</span> below for updates.</p>
                </div>
                <div class="tagline">
                    <p>
                        Rock it with Vocit.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container campaign-box">
    <div class="row">
        <div class="col-sm-7 campaign-prizes">
            <div class="heading">
                <i class="fa fa-hand-o-right pull-left"></i>
                <h2 clas="pull-left">
                    Share with your friends<br>  
                    <span>Win Prizes</span><br>
                    <span>Win Beta Voci Tokens</span>
                </h2>
            </div>
            <ul class="list-unstyled prizes-points">
                <li><i class="fa fa-hand-o-right"></i><span>Enter your email to the right.</span></li>
                <li><i class="fa fa-hand-o-right"></i><span>Get your custom referral link.</span></li>
                <li><i class="fa fa-hand-o-right"></i><span>Share your links with friends.</span></li>
                <li><i class="fa fa-hand-o-right"></i><span>Earn prizes as your friends sign up.</span></li>
            </ul>
            <div class="prizes-list clearfix">
                <div class="prize">
                    {{ Html::image("img/frontend/harddisk.jpg" ,'Vocit Portable External Hard Drive prize', ["class" => "img-responsive"]) }}
                    <h5>WD 4TB Black My Passport Portable External Hard Drive - USB 3.0</h5>
                </div>
                <div class="prize">
                    {{ Html::image("img/frontend/Wristband.jpg" ,'Vocit prize', ["class" => "img-responsive"]) }}
                    <h5>Huawei Band 2 Pro Smart Fitness Wristband | GPS | Multi-Sport Mode| Heart Rate | 5ATM Waterproof</h5>
                </div>
                <div class="prize">
                    {{ Html::image("img/frontend/Headphones.jpg" ,'Vocit prize', ["class" => "img-responsive"]) }}
                    <h5>COWIN E7 Active Noise Cancelling Bluetooth Headphones with Microphone</h5>
                </div>
                <div class="prize">
                    {{ Html::image("img/frontend/Camping-Hammock.jpg" ,'Vocit prize', ["class" => "img-responsive"]) }}
                    <h5>Greenlight Outdoor Double Camping With Hammock Straps & Carabiners</h5>
                </div>
                <div class="prize">
                    {{ Html::image("img/frontend/Lantern-Emergency.jpg" ,'Vocit prize', ["class" => "img-responsive"]) }}
                    <h5>3Pack LED Tent Bulb, Portable Lantern Emergency Night light for Outdoor Lighting</h5>
                </div>
            </div>
        </div>
        <div class="col-sm-5 campaign-form">
            <div class="heading">
                <i class="fa fa-trophy"></i>
                <h3>Free prizes are a step away</h3>
                <small>Only valid emails will be counted.</small>
            </div>
            <div class="campaign-embed-form">
                <div id="kol_embed_container_216774"></div>
                <script>window.jQuery || document.write('<scr' + 'ipt src="https://code.jquery.com/jquery-1.11.0.min.js"><\/sc' + 'ript>')</script>
                <script>window.$kol_jquery = window.jQuery</script>
                <script src="https://kickoffpages-kickofflabs.netdna-ssl.com/widgets/1.9.6/kol_embed.js"></script>
                <script src='https://kickoffpages-kickofflabs.netdna-ssl.com/w/114975/216774.js'></script>
            </div>
            <div class="options">
                <h4>All valid emails will be entered into a random email
                    drawing for prizes and tokens. The top ten sharers will win prizes and tokens.
                </h4>
                <small>We respect your privacy and will never sell or spam your email. In fact, we will only send three emails unless you ask for more.</small>
            </div>
        </div>
    </div>
</div>

<div class="container other-features">
    <div class="row">
        <div class="col-sm-6 learn-more">
            <h2>Learn More</h2>
            <p class="signup">Join us by <a class="" href="{{ route('frontend.signup') }}">Signup</a> now.</p>
            <!--<p class="traning">Visit our <a class="" href="{{-- route('frontend.signup') --}}">Training</a> page.</p>-->
            <p class="home">Visit our <a class="" href="{{ route('frontend.home') }}">Vocit Beta Home</a> page.</p>
        </div>
        <div class="col-sm-6 social-sites">
            <h2>Visit our Social Sites</h2>
            <ul class="list-unstyled list-inline">
                <li><a class="" href="http://www.facebook.com/vocitofficial" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                <li><a class="" href="http://www.linkedin.com/company/vocitofficial/" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                <li><a class="" href="http://www.instagram.com/vocitofficial" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <li><a class="" href="http://www.twitter.com/vocitofficial" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                <li><a class="telegram-icon" href="https://t.me/officialVocit" target="_blank"><i class="fa fa-paper-plane"></i></a></li>
                <li><a class="" href="https://discord.gg/XQfs4hy" target="_blank">
                        {{ Html::image("img/frontend/discord.png" ,'Vocit Discord', ["class" => "discord-img"]) }}
                    </a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container platform">
    <div class="row">
        <div class="col-sm-6 image vocit-video">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/YGacMahVA_I?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
        <div class="col-sm-6 description">
            <h2>What is the Vocit Platform?</h2>
            <p>
                Vocit is a new social media platform that allows
                businesses and consumers to have a mutual online
                relationship on a level that no other online avenue
                can offer. Vocit’s social platform improves the online
                relationship of consumers and businesses by
                providing social websites, decentralized asset
                exchanges, ownership of consumer data, analytics,
                and personal identifiable information (DAPII), and
                other communication technologies.
            </p>
            <a class="btn-blue-border" href="{{ route('frontend.home') }}">Visit our Vocit Beta Home page</a>
        </div>
    </div>
</div>

<div class="container platform reward-tokens">
    <div class="row">
        <div class="col-sm-6 description">
            <h2>Beta Vocit Token</h2>
            <p>Beta Voci is the pre-launch reward token for the Vocit
                platform. Upon launching we will allow a 1:1 exchange for
                the official Vocit reward token called Voci. 
            </p>
            <p>
                Enter your email above to receive 25 Beta Voci and a chance
                to win more prizes. 
            </p>
            <a class="btn-blue-border" target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSe6xq0IG3m0WRYkHamJZVPWJjufYLWkD2ktdqfHTfNIOLc78w/viewform">Get 25 Free Beta Voci tokens</a>
        </div>
        <div class="col-sm-6 image">
            <ul class="list-unstyled list-inline">
                <li>{{ Html::image("img/frontend/vocit-token.png" ,'What is the Vocit Platform?', ["class" => "img-responsive"]) }}</li>
                <li>{{ Html::image("img/frontend/vocit-token-two.png" ,'What is the Vocit Platform?', ["class" => "img-responsive"]) }}</li>
            </ul>
        </div>
    </div>
</div>

<div class="container main_content">
    <div class="row">
        <div class="all-vocs">
            <div id="pinBoot">
                <div class="before-footer-divs">
                    @include('frontend.custom_template.posts.promotion')
                    @include('frontend.custom_template.posts.voice')
                    @include('frontend.custom_template.posts.jobs')
                    @include('frontend.custom_template.posts.update')
                    @include('frontend.custom_template.posts.event')
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main_content">
    <div class="all-vocs">
        <div id="pinBoot">
            @include('frontend.includes.voc_post_mini_modal')
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
{!! Html::script(mix('js/voc_template.js')) !!}
@endsection