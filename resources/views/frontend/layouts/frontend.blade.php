<!doctype html> 
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', app_name())</title>

        <link rel="shortcut icon" type="image/png" href="{{URL::to('/')}}/favicon.png"/>
        <link rel="shortcut icon" type="image/png" href="{{URL::to('/')}}/favicon.png"/>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Vocit is a social platform that focuses on the customer and business relationship with social websites, digital reward programs, customer ownership of data, and other communication technologies.')">
        <meta name="author" content="@yield('meta_author', 'Vocit')">
        @yield('meta')
        
        <?php
        $title = "Vocit";
        $description = "Vocit is a social platform that focuses on the customer and business relationship with social websites, digital reward programs, customer ownership of data, and other communication technologies.";
        $image = URL::to('/')."/img/frontend/Vocit-main.jpg";
        $url = URL::to('/');
        $tagline = "Rocit with Vocit";
        ?>
        @include('frontend.includes.share_meta')


        <!-- Styles -->
        @yield('before-styles') 
        @yield('after-styles')

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>
        </script>
    </head>
    <body>
        @include('frontend.includes.header')
        <div class="voc-main-content">
            @yield('content')
        </div>
        @include('frontend.includes.footer') 

        <!-- Scripts -->
        @yield('before-scripts')
        <script type="text/javascript">
            var vocApp = {},
                    category = <?php echo json_encode(isset($categories) ? $categories->toArray() : []); ?>;
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.1/underscore-min.js"></script>
        @yield('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>  