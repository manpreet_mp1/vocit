<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', app_name())</title>

        <link rel="shortcut icon" type="image/png" href="{{URL::to('/')}}/favicon.png"/>
        <link rel="shortcut icon" type="image/png" href="{{URL::to('/')}}/favicon.png"/>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Vocit is a social platform that focuses on the customer and business relationship with social websites, digital reward programs, customer ownership of data, and other communication technologies.')">
        <meta name="author" content="@yield('meta_author', 'Vocit')">
        @yield('meta')

        <?php
        $title = "Vocit";
        $description = "Vocit is a social platform that focuses on the customer and business relationship with social websites, digital reward programs, customer ownership of data, and other communication technologies.";
        $image = "http://vocit.io/img/frontend/Vocit-main.jpg";
        $url = "http://vocit.io/";
        $tagline = "Rocit with Vocit";
        ?>
        @include('frontend.includes.share_meta')

        <!-- Styles -->
        @yield('before-styles')
        {{ Html::style(mix('css/backend.css')) }}
        {{ Html::style(mix('css/voc_dashboard.css')) }}
        @yield('after-styles')

        <!-- Scripts -->
        <script>
            window.Laravel = <?php
        echo json_encode([
            'csrfToken' => csrf_token(),
        ]);
        ?>
        </script>
    </head>
    <body id="app-layout" class="skin-{{ config('backend.theme') }} {{ config('backend.layout') }} voc-dash">
        <div class="wrapper">
            @include('frontend.includes.voc_dashboard_header')
            @include('frontend.includes.voc_dashboard_sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    @yield('page-header')

                    {{-- Change to Breadcrumbs::render() if you want it to error to remind you to create the breadcrumbs for the given route --}}
                    {!! Breadcrumbs::renderIfExists() !!}
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="loader" style="display: none;">
                        <div class="ajax-spinner ajax-skeleton"></div>
                    </div><!--loader-->

                    @include('includes.partials.messages')
                    @yield('content')
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            @include('frontend.includes.voc_dashboard_footer')
        </div><!-- ./wrapper -->

        <!-- Scripts -->
        @yield('before-scripts')
         <script type="text/javascript">
            var vocApp = {};
        </script>
        {{ Html::script(mix('js/backend.js')) }}
        @yield('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>