<!doctype html> 
<html lang="{{ app()->getLocale() }}"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', app_name())</title>

        <link rel="shortcut icon" type="image/png" href="{{URL::to('/')}}/favicon.png"/>
        <link rel="shortcut icon" type="image/png" href="{{URL::to('/')}}/favicon.png"/>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Vocit is a social platform that focuses on the customer and business relationship with social websites, digital reward programs, customer ownership of data, and other communication technologies.')">
        <meta name="author" content="@yield('meta_author', 'Vocit')">
        @yield('meta')

        <?php
        $title = $voc->title;

        $start = strpos($voc->information, '<p>');
        $end = strpos($voc->information, '</p>', $start);
        $paragraph = substr($voc->information, $start, $end - $start + 4);
        $description = html_entity_decode(strip_tags($paragraph));
        
        $image = url('/').config('constant.voc_image_path').$voc->image;
        $url = url()->current();
        $tagline = "Rocit with Vocit";
        ?>
        @include('frontend.includes.share_meta')

        <!-- Styles -->
        @yield('before-styles') 
        @yield('after-styles')

        <!-- Scripts -->
        <script>
            window.Laravel = <?php
        echo json_encode([
            'csrfToken' => csrf_token(),
        ]);
        ?>
        </script>
    </head>
    <body id="app-layout">
        @include('frontend.includes.navigation')
        @include('frontend.includes.template_header')

        @yield('content')

        @include('frontend.includes.template_footer') 

        <!-- Scripts -->
        @yield('before-scripts')
        <script type="text/javascript">
            var vocApp = {};
        </script>
        {!! Html::script(mix('js/voc_template.js')) !!} 
        @yield('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>