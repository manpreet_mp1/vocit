<!doctype html> 
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', app_name())</title>

        <link rel="shortcut icon" type="image/png" href="http://vocit.io/favicon.png"/>
        <link rel="shortcut icon" type="image/png" href="http://vocit.io/favicon.png"/>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Vocit is a social platform that focuses on the customer and business relationship with social websites, digital reward programs, customer ownership of data, and other communication technologies.')">
        <meta name="author" content="@yield('meta_author', 'Vocit')">
        @yield('meta')
        
        <?php
        $title = "Vocit";
        $description = "Vocit is a social platform that focuses on the customer and business relationship with social websites, digital reward programs, customer ownership of data, and other communication technologies.";
        $image = "http://vocit.io/img/frontend/Vocit-main.jpg";
        $url = "http://vocit.io/";
        $tagline = "Rocit with Vocit";
        ?>
        @include('frontend.includes.share_meta')

        <!-- Styles -->
        @yield('before-styles') 
        @yield('after-styles')

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>
        </script>
    </head>
    <body>
        @include('frontend.includes.about_header')
        <div class="voc-main-content">
            @yield('content')
        </div>
        @include('frontend.includes.footer') 

        <!-- Scripts -->
        @yield('before-scripts')
        <script type="text/javascript">
            var vocApp = {};
        </script>
        @yield('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>  