@extends('frontend.layouts.frontend')

@section('title')
Home
@stop

@section('after-styles')
{{ Html::style(elixir('css/frontend.css')) }}
@stop

@section('content')
<div class="row" style="margin: 0;">
    @include('includes.partials.logged-in-as')
    @include('includes.partials.messages')
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="panel col-md-8 reset-password-panel">

        <div class="panel-heading reset-password-heading"> <h4> Reset Password <h4></div>

                    <div class="panel-body">

                        {{ Form::open(['route' => 'frontend.auth.password.reset', 'class' => 'form-horizontal']) }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group  reset-password-input">
                            {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
                            <p class="form-control-static">{{ $email }}</p>
                            {{ Form::hidden('email', $email, ['class' => 'form-input', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                        </div><!--form-group-->

                        <div class="form-group reset-password-input <?php if ($errors->first('password')) echo ' has-error'; ?>">
                            {{ Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::password('password', ['class' => 'form-input', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                                <?php if ($errors->first('password')) { ?>
                                    <div class="alert-error">{{ $errors->first('password') }}</div>
                                <?php } ?>
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group reset-password-input">
                            {{ Form::label('password_confirmation', trans('validation.attributes.frontend.password_confirmation'), ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::password('password_confirmation', ['class' => 'form-input', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group reset-password-input">
                            <div class="col-md-6 col-md-offset-4">
                                {{ Form::submit(trans('labels.frontend.passwords.reset_password_button'), ['class' => 'btn buttons reset-password-btn']) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        {{ Form::close() }}

                    </div><!-- panel body -->

                    </div><!-- panel -->

                    </div><!-- row -->
                    @endsection