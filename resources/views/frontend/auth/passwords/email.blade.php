@extends('frontend.layouts.frontend')

@section('title')
Home
@stop

@section('after-styles')
{{ Html::style(elixir('css/frontend.css')) }}
@stop

@section('content')
<div class="row" style="margin: 0;">
    @include('includes.partials.messages')
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="panel col-md-6 reset-password-panel">

        <div class="panel-heading reset-password-heading"> <h4> Reset Password <h4></div>

                    <div class="panel-body">

                        {{ Form::open(['route' => 'frontend.auth.password.email.post', 'class' => 'form-horizontal']) }}
                        <div class="form-group reset-password-input">
                            {{ Form::email('email', null, ['class' => 'form-input reset-password-input', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::submit(trans('labels.frontend.passwords.send_password_reset_link_button'), ['class' => 'btn buttons reset-password-btn']) }}
                        </div><!--form-group-->

                        {{ Form::close() }}

                    </div><!-- panel body -->

                    </div><!-- panel -->

                    </div><!-- row -->
                    @endsection