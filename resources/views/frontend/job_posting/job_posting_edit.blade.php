@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Update Job Posting')
@section('page-header')
<h1>
    Update Job posting
</h1>
@stop
@section('content')
{{ Form::open(['route'=>'frontend.jobPosting.update','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','enctype' => "multipart/form-data"]) }}
{{ Form::hidden('id', $jobs->id) }}

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Update Job posting</h3>
    </div><!-- /.box-header -->


    <div class="box-body">
        <div class="form-group <?php if ($errors->first('job_title')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('job_title', 'Job Title',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('job_title', $jobs->job_title , array('class' => 'form-control', 'required')) }}
                <span class="help-block">{{ $errors->first('job_title') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('job_industry')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('job_industry', 'Job Industry',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::select('job_industry', config('constant.job_industry'),$jobs->job_industry, array('class' => 'form-control', 'required')) }}
                <span class="help-block">{{ $errors->first('job_industry') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('location')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('location', 'Job Location',array('class'=>'control-label')) }}
            </div>
            <input type="hidden" value="{{ (!empty($jobs->street_number)) ? $jobs->street_number : '' }}" name="street_number" class="field" id="street_number"></input>
            <input type="hidden" value="{{ (!empty($jobs->route)) ? $jobs->route : '' }}" name="route" class="field" id="route"></input>
            <input type="hidden" value="{{ (!empty($jobs->locality)) ? $jobs->locality : '' }}" name="locality" class="field" id="locality"></input>
            <input type="hidden" value="{{ (!empty($jobs->neighborhood)) ? $jobs->neighborhood : '' }}" name="neighborhood" class="field" id="neighborhood"></input>
            <input type="hidden" value="{{ (!empty($jobs->district)) ? $jobs->district : '' }}" name="district" class="field" id="administrative_area_level_2"></input>
            <input type="hidden" value="{{ (!empty($jobs->state)) ? $jobs->state : '' }}" name="state" class="field" id="administrative_area_level_1"></input>
            <input type="hidden" value="{{ (!empty($jobs->country)) ? $jobs->country : '' }}" name="country" class="field" id="country"></input>
            <input type="hidden" value="{{ (!empty($jobs->postal_code)) ? $jobs->postal_code : '' }}" name="postal_code" class="field" id="postal_code"></input>


            <div class="col-sm-10">
                {{ Form::text('location', $jobs->location , array('class' => 'form-control', 'id'=>'txtPlaces', 'required')) }}
                {{ Form::hidden('latitude', $jobs->latitude, array('id' => 'latitude')) }}
                {{ Form::hidden('longitude', $jobs->longitude, array('id' => 'longitude')) }}
                <span class="help-block">{{ $errors->first('location') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('pay')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('pay', 'Pay',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::number('pay', $jobs->pay, array('class' => 'form-control abc', 'required')) }}
                <span class="help-block">{{ $errors->first('pay') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('employment_type')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('employment_type', 'Employment Type',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::select('employment_type', config('constant.employment_type'),$jobs->employment_type, array('class' => 'form-control', 'required')) }}
                <span class="help-block">{{ $errors->first('employment_type') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('benefits_offered')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('benefits_offered', 'Benefits Offered',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::radio('benefits_offered', config('constant.benefits_offered.true'),$jobs->benefits_offered==config('constant.benefits_offered.true')?true:false, array('required')) }}
                <span style="margin-right:10px;">Yes</span>
                {{ Form::radio('benefits_offered', config('constant.benefits_offered.false'),$jobs->benefits_offered==config('constant.benefits_offered.false')?true:false, array('required')) }}
                <span>No</span>
                <span class="help-block">{{ $errors->first('benefits_offered') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('logo')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('logo', 'Logo',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10 input-group" style="padding: 0 15px;">
                @if(isset($jobs->logo))
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                    </a>
                </span>
                {{ Form::text('logo', $jobs->logo, array('class' => 'form-control', 'id' => "thumbnail"))}}
                <!--<input id="thumbnail" class="form-control" type="text" name="logo">-->
                @else
                {{ Form::text('logo', $jobs->logo, array('class' => 'form-control', 'id' => "thumbnail", 'required'))}}
                <!--<input id="thumbnail" class="form-control" type="text" name="logo" required>-->
                @endif
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <img id="holder" style="margin-top:15px;max-height:100px;">
            </div>
            <span class="help-block">{{ $errors->first('logo') }}</span>
            @if(isset($jobs->logo))
            <div class="col-sm-2"></div>
            <div class="col-sm-10 voc">
                <img src="{{ URL::to($jobs->logo) }}" class="team-image"/>
            </div>
            @endif
        </div>

        <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('body', 'Body',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::textarea('body', $jobs->body , array('class' => 'form-control ckeditor','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('body') }}</span>
            </div>
        </div>

    </div>


    <?php
    if (!empty($sharing)) {
        ?>

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Post Options</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'General',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==1)
                        {{ Form::checkbox('sharingOptions[]', $share->id, $jobs->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'Share',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==2)
                        {{ Form::checkbox('sharingOptions[]', $share->id, $jobs->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="box-body">
            <div class="pull-right">
                @if(isset( $jobs->id))
                {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                @else
                {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                @endif
            </div>
            {{ Form::close() }}
            <div class="clearfix"></div>
        </div>
    </div>

@if(count($errors) > 0)
@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{$error}}
</div>
@endforeach
@endif

@endsection

@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGJ5EazH2m_do6_Svm7ZWn2DGB3g-qLOg&sensor=false&libraries=places" type="text/javascript"></script>
<script src="{{url('/')}}/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
$(document).ready(function () {
   $('#lfm').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});

    $(".abc").on('keyup', function () {
        var key = $(this).val();
        var regex = /^\d*\.?\d+$/;
        $('.add-error').remove();
        if (!regex.test(key)) {
            $(".abc").after("<div class='add-error alert-danger'>Please enter a valid number</div>");
        }

    });

    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      neighborhood: 'long_name',
      administrative_area_level_2: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
            $('#latitude').val(latitude);
            $('#longitude').val(longitude);

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        });
    });

});
</script>
@endsection