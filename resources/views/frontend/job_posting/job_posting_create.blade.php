@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Create Job Posting')
@section('page-header')
<h1>
    Create Job posting
</h1>
@stop
@section('content')
{{ Form::open(['route'=>'frontend.jobPosting.save','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'enctype' => "multipart/form-data"]) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Create Job posting</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('job_title')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('job_title', 'Job Title',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('job_title', null, array('class' => 'form-control', 'required')) }}
                <span class="help-block">{{ $errors->first('job_title') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('job_industry')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('job_industry', 'Job Industry',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::select('job_industry', config('constant.job_industry'),null, array('class' => 'form-control', 'required')) }}
                <span class="help-block">{{ $errors->first('job_industry') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('location')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('location', 'Location',array('class'=>'control-label')) }}
            </div>

            <input type="hidden" name="street_number" class="field" id="street_number" disabled="true"></input>
            <input type="hidden" name="route" class="field" id="route" disabled="true"></input>
            <input type="hidden" name="locality" class="field" id="locality" disabled="true"></input>
            <input type="hidden" name="neighborhood" class="field" id="neighborhood" disabled="true"></input>
            <input type="hidden" name="district" class="field" id="administrative_area_level_2" disabled="true"></input>
            <input type="hidden" name="state" class="field" id="administrative_area_level_1" disabled="true"></input>
            <input type="hidden" name="country" class="field" id="country" disabled="true"></input>
            <input type="hidden" name="postal_code" class="field" id="postal_code" disabled="true"></input>

            <div class="col-sm-10">
                {{ Form::text('location', null, array('class' => 'form-control','id' => 'txtPlaces', 'required')) }}
                {{ Form::hidden('latitude', 'latitude', array('id' => 'latitude')) }}
                {{ Form::hidden('longitude', 'longitude', array('id' => 'longitude')) }}
                <span class="help-block">{{ $errors->first('location') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('pay')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('pay', 'Pay',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::number('pay', null, array('class' => 'form-control abc','placeholder'=>'Enter a integer value.', 'required','autocomplete'=>"off")) }}
                <span class="help-block">{{ $errors->first('pay') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('employment_type')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('employment_type', 'Employment Type',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::select('employment_type', config('constant.employment_type'),null, array('class' => 'form-control', 'required')) }}
                <span class="help-block">{{ $errors->first('employment_type') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('benefits_offered')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('benefits_offered', 'Benefits Offered',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::radio('benefits_offered', 1 ,true, array('required', 'id'=>'abc')) }}
                <span style="margin-right:10px;">Yes</span>
                {{ Form::radio('benefits_offered', 0 ,false,array('required', 'id'=>'xyz')) }}
                <span>No</span>
                <span class="help-block">{{ $errors->first('benefits_offered') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('logo')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('logo', 'Logo',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10 input-group" style="padding: 0 15px;">
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                    </a>
                </span>
                <input id="thumbnail" class="form-control" type="text" name="logo">
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <img id="holder" style="margin-top:15px;max-height:100px;">
            </div>
            <span class="help-block">{{ $errors->first('logo') }}</span>
        </div>

        <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('body', 'Body',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::textarea('body', null, array('class' => 'form-control ckeditor', 'required')) }}
                <span class="help-block">{{ $errors->first('body') }}</span>
            </div>
        </div>
    </div>

    <?php
    if (!empty($sharing)) {
        ?>

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Post Options</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'General',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==1)
                        {{ Form::checkbox('sharingOptions[]', $share->id, null, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'Share',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==2)
                        {{ Form::checkbox('sharingOptions[]', $share->id, null, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="box-body">
        <div class="pull-right">
            {{ Form::submit(trans('Post'), ['class' => 'btn btn-success']) }}
        </div>
        {{ Form::close() }}
        <div class="clearfix"></div>
    </div>
</div>
@endsection

@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGJ5EazH2m_do6_Svm7ZWn2DGB3g-qLOg&sensor=false&libraries=places" type="text/javascript"></script>
<script src="{{url('/')}}/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
$(document).ready(function () {
    $('#lfm').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});

    $("#pay").on('blur', function () {
        var key = $(this).val();
        var regex = /^\d*\.?\d+$/;
        $('.alert-danger').remove();
        if (!regex.test(key) && key) {
            $("#pay").after("<div class='alert alert-danger'>Please enter a valid number</div>");
        }
    });
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        neighborhood: 'long_name',
        administrative_area_level_2: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
            $('#latitude').val(latitude);
            $('#longitude').val(longitude);

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        });
    });
});
</script>

@endsection
