@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Create Update')
@section('page-header')
<h1>
    Create Update
</h1>
@stop

@section('content')

{{ Form::open(['route'=>'frontend.update.save','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'enctype' => "multipart/form-data"]) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Create Update</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('title', 'Title',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('title', null, array('class' => 'form-control','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('title') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('logo')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('logo', 'Logo',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10 input-group" style="padding: 0 15px;">
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                    </a>
                </span>
                <input id="thumbnail" class="form-control" type="text" name="logo">
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <img id="holder" style="margin-top:15px;max-height:100px;">
            </div>
            <span class="help-block">{{ $errors->first('logo') }}</span>
        </div>

        <div class="form-group <?php if ($errors->first('media')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('media', 'Media',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                <!--{{ Form::file('media', null, array('class' => 'form-control','max-length' => '100', 'required')) }}-->
                {{ Form::button('Image', array('class' => 'btn btn-primary', 'id' => 'update-media-image','style' => 'margin-right: 15px')) }}
                {{ Form::button('Video', array('class' => 'btn btn-primary', 'id' => 'update-media-video')) }}

                <div id="update-media-file-module" style="display:none;">
                    <div class="col-sm-12 input-group" style="padding: 0 15px;">
                        <span class="input-group-btn">
                            <a id="lfm2" data-input="thumbnail2" data-preview="holder2" class="btn btn-primary">
                                <i class="fa fa-picture-o"></i> Choose
                            </a>
                        </span>
                        <input id="thumbnail2" class="form-control" type="text" name="media">
                        {{ Form::hidden('media_type', config('constant.media_type.image')) }}
                    </div>
                    <div class="col-sm-12">
                        <img id="holder2" style="margin-top:15px;max-height:100px;">
                    </div>
                    <span class="help-block">{{ $errors->first('media') }}</span>
                </div>
                <div id="update-media-video-input" style="display:none;">
                    <div class="col-sm-12">
                        {{ Form::text('media', null, array('class' => 'form-control','max-length' => '100','placeholder'=>'Please insert You Tube link')) }}
                        {{ Form::hidden('media_type', config('constant.media_type.video_link')) }}
                        <span class="help-block">{{ $errors->first('media') }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('body', 'Body',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::textarea('body', null, array('class' => 'form-control ckeditor','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('body') }}</span>
            </div>
        </div>


    </div>

 <?php
    if (!empty($sharing)) {
        ?>

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Post Options</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'General',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==1)
                        {{ Form::checkbox('sharingOptions[]', $share->id, null, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'Share',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==2)
                        {{ Form::checkbox('sharingOptions[]', $share->id, null, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="box-body">
        <div class="pull-right">
            {{ Form::submit(trans('Post'), ['class' => 'btn btn-success']) }}
        </div>
        {{ Form::close() }}
        <div class="clearfix"></div>
    </div>
</div>

@endsection

@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script src="{{url('/')}}/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
$(document).ready(function () {
    $('#lfm').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});
    $('#lfm2').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});

    $('#update-media-image').on('click', function () {
        $('#update-media-image').hide();
        $('#update-media-video').hide();
        $('#update-media-video-input').remove();
        $('#update-media-file-module').show();
    });
    $('#update-media-video').on('click', function () {
        $('#update-media-image').hide();
        $('#update-media-video').hide();
        $('#update-media-file-module').remove();
        $('#update-media-video-input').show();
    });
});
</script>
@endsection
