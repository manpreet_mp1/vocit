@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Edit Update')
@section('page-header')
<h1>
    Edit Update
</h1>
@stop
@section('content')

{{ Form::open(['route'=>'frontend.update.update','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','enctype' => "multipart/form-data"]) }}
{{ Form::hidden('id', $updates->id) }}


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Update</h3>
    </div><!-- /.box-header -->


    <div class="box-body">
        <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('title', 'Title',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('title', $updates->title, array('class' => 'form-control','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('title') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('logo')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('logo', 'Logo',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10 input-group" style="padding: 0 15px;">
                @if(isset($updates->logo))
                <span class="input-group-btn">
                    <a id="lfm2" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                    </a>
                </span>
                {{ Form::text('logo', $updates->logo, array('class' => 'form-control', 'id' => "thumbnail"))}}
                @else
                {{ Form::text('logo', $updates->logo, array('class' => 'form-control', 'id' => "thumbnail", 'required'))}}
                @endif
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <img id="holder" style="margin-top:15px;max-height:100px;">
            </div>
            <span class="help-block">{{ $errors->first('logo') }}</span>
            @if(isset($updates->logo))
            <div class="col-sm-2"></div>
            <div class="col-sm-10 voc">
                <img src="{{ URL::to($updates->logo) }}" class="team-image"/>
            </div>
            @endif
        </div>

        <div class="form-group <?php if ($errors->first('media')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('media', 'Media',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::button('Image', array('class' => 'btn btn-primary', 'id' => 'update-media-image-btn', 'style'=>'display:none; margin:0 15px 15px 0;')) }}
                {{ Form::button('Video', array('class' => 'btn btn-primary', 'id' => 'update-media-video-btn', 'style'=>'display:none; margin:0 15px 15px 0;')) }}
                @if(isset($updates->media))
                <?php
                $med = 'file';
                if (strpos($updates->media, "youtube") !== false) {
                    $med = 'youtube';
                }
                ?>
                <div id = "update-media-video-input" class="form-group" style="display:<?php
                echo $med == 'youtube' ? 'block' : 'none';
                ?>;" >
                    <div class = "col-sm-12">
                        {{ Form::text('media', ($updates->media_type == config('constant.media_type.video_link')) ? $updates->media : '', array('class' => 'form-control input-video', 'max-length' => '100', 'required', 'placeholder' => 'Please insert You Tube link', 'disabled'=> $updates->media_type != config('constant.media_type.video_link'))) }}
                        {{ Form::hidden('media_type', config('constant.media_type.video_link')) }}
                        <button id="media-video-close-btn" type="button" class="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class = "help-block">{{ $errors->first('media') }}</span>
                        </div>
                </div>
                <div class="col-sm-12 input-group" id="media-input-group">
                    <div id="update-media-file-module" style="display:<?php
                    echo $med !== 'youtube' ? 'block' : 'none';
                    ?>;">
                        <div class="col-sm-12 input-group">
                            @if(isset($updates->media))
                            <span class="input-group-btn">
                                <a id="lfm" data-input="thumbnail2" data-preview="holder2" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                            {{ Form::text('media', ($updates->media_type == config('constant.media_type.image')) ? $updates->media : '', array('class' => 'form-control input-image', 'id' => "thumbnail2", 'disabled'=> $updates->media_type != config('constant.media_type.image') ))}}
                            @else
                            {{ Form::text('media', ($updates->media_type == config('constant.media_type.image')) ? $updates->media : '', array('class' => 'form-control input-image', 'id' => "thumbnail2", 'required', 'disabled'=> $updates->media_type != config('constant.media_type.image')))}}
                            @endif
                            <button class="media-img-close-btn" type="button" class="close" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ Form::hidden('media_type', config('constant.media_type.image')) }}
                        </div>
                        <div class="col-sm-12">
                            <img id="holder2" style="margin-top:15px;max-height:100px;">
                        </div>
                        <span class="help-block">{{ $errors->first('media') }}</span>
                        @if(isset($updates->media))

                        <div class="col-sm-12 voc">
                            <img src="{{ URL::to($updates->media) }}" class="team-image"/>
                        </div>
                        @endif
                    </div>
                    @endif
                </div>

            </div>
        </div>
        <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('body', 'Body',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::textarea('body', $updates->body, array('class' => 'form-control ckeditor','max-length' => '100', 'required')) }}
                <span class="help-block">{{ $errors->first('body') }}</span>
            </div>
        </div>
</div>

 <?php
    if (!empty($sharing)) {
        ?>

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Post Options</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'General',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==1)
                        {{ Form::checkbox('sharingOptions[]', $share->id, $updates->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'Share',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==2)
                        {{ Form::checkbox('sharingOptions[]', $share->id, $updates->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="box-body">
            <div class="pull-right">
                @if(isset( $updates->id))
                {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                @else
                {{ Form::submit(trans('buttons.general.crud.save'), ['class' => 'btn btn-success']) }}
                @endif
            </div>
            {{ Form::close() }}
            <div class="clearfix"></div>
        </div>
    </div>

@if(count($errors) > 0)
@foreach($errors->all() as $error)
<div class="alert alert-danger">
    {{$error}}
</div>
@endforeach
@endif

@endsection

@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script src="{{url('/')}}/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
$(document).ready(function () {
    $('#lfm').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});
    $('#lfm2').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});

    $('#media-video-close-btn').on('click', function () {
        $('#update-media-video-input').hide();
        $('#update-media-video-input .input-video').val('');
        $('#update-media-image-btn,#update-media-video-btn').show();
    });
    $('.media-img-close-btn').on('click', function () {
        $('#update-media-file-module img').hide();
        $('#media-input-group').hide();
        $('#media-input-group .input-image').val('');
        $('#update-media-image-btn, #update-media-video-btn').show();
    });
    $('#update-media-image-btn').on('click', function () {
        $('#update-media-file-module input').attr("disabled", false);
        $('#update-media-video-input input').attr("disabled", true);
        $('#update-media-image-btn,#update-media-video-btn').hide();
        $('#update-media-file-module').show();
    });
    $('#update-media-video-btn').on('click', function () {
        $('#update-media-video-input input').attr("disabled", false);
        $('#update-media-file-module input').attr("disabled", true);
        $('#update-media-image-btn,#update-media-video-btn').hide();
        $("#update-media-video-input").show();
    });
});
</script>
@endsection