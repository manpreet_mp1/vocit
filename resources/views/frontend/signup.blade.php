@extends('frontend.layouts.frontend')

@section('title')
Signup
@stop

@section('after-styles')
{{ Html::style(elixir('css/signup.css')) }}
@stop

@section('content')
<div class="main_content">
    <div class="container sign-up">
        <div class="">

            <div class="signup_page">
                <div class="parent col-container">
                    <!--User-->
                    <div class="signup_user col">  
                        <div class="innerdata"> 
                            <div class="inner_text">
                                <h2 id="voc_range">User</h2>
                                <p>Do you want to be inspired by your favorite <br>business, people and ideas? <br>
                                    Follow your favorite Voc's and receive instant <br> posts to your home page. <br></p>
                                <a href="#" class="join_user btn btn-default-black">JOIN NOW</a>

                            </div>
                            <div class="vocformdiv" style="display:none;">

                                <div class="" id="voc-create">
                                    <div class="create-voc">
                                        <!-- Create Voc content-->
                                        <div class="create-content"> 
                                            <div class="create-header">
                                                <h4 class="create-title">Create a Voc</h4>
                                            </div>
                                            <div class="create-body" id="voc-create">
                                                <div class="form-response"></div>
                                                {{ Form::open(['route'=>'frontend.storeInfo', 'id'=>'createVoc']) }}
                                                <div class="form-group">
                                                    <label> Name your VOC </label>
                                                    {{ Form::text('name', null,['placeholder' => 'Name @Voc', 'class'=>'form-input form-control','required' => 'required','id'=>'name']) }}
                                                </div>
                                                <div class="form-group">
                                                    <label>Primary region of focus</label>
                                                    {{ Form::text('zip_code', null, ['placeholder' => 'Zip Code (Optional)', 'class'=>'form-input form-control','id'=>'zip_code']) }}
                                                </div>
                                                <div class='continue-button'>
                                                    <button type="submit" class="btn btn-default-black btn-create">Continue</button>
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="" id="basic-info" style="display:none;">
                                    <div class="create-voc">
                                        <!--Create content-->
                                        <div class="create-content">
                                            <div class="create-header">
                                                <h4 class="create-title">Basic Information</h4>
                                            </div>
                                            <div class="create-body" id="storeProfile">
                                                <div class="form-response"></div>
                                                {{ Form::open(['route'=>'frontend.storeUser', 'id'=>'vocProfile']) }}
                                                {{ Form::hidden('user_role', config('constant.roles_inverse.voc')) }}
                                                <div class="form-group">
                                                    {{ Form::email('email', null, ['placeholder' => 'Professional Email', 'class'=>'form-input form-control','required' => 'required','id'=>'email']) }}
                                                </div>
                                                <!--<div class="form-group">-->
                                                {{-- Form::text('phone_number', null, ['id'=>'phone_number', 'class'=>'form-input form-control','placeholder' => 'Phone Number','required' => 'required']) --}}
                                                <!--</div>-->
                                                <div class="form-group">
                                                    {{ Form::password('password',['id'=>'password', 'class'=>'form-input form-control','placeholder' => 'Create Password','required' => 'required']) }}
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::password('password_confirmation', ['id'=>'password_confirmation', 'class'=>'form-input form-control','placeholder' => 'Confirm Password','required' => 'required']) }}
                                                </div>
                                                <div class='continue-button'>
                                                    <button type="submit" class="btn btn-default-black btn-create">Create</button>
                                                </div>
                                                {{ Form::close() }}

                                                <p class='tag-line'>Let your voice be heard.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div id="topic" style="display:none;">
                                    <?php
                                    if (isset($vocCategoriesSignUpList) && !empty($vocCategoriesSignUpList)) {
                                        $topicCount = count($vocCategoriesSignUpList);
                                        if ($topicCount <= 10) {
                                            $width = "width-min";
                                        } elseif ($topicCount >= 11 && $topicCount <= 15) {
                                            $width = "width-medium";
                                        } elseif ($topicCount >= 15 && $topicCount <= 20) {
                                            $width = "width-max";
                                        }
                                    }
                                    ?>
                                    <div class="create-voc create-width <?php
                                    if (isset($vocCategoriesSignUpList) && !empty($vocCategoriesSignUpList)) {
                                        echo "$width";
                                    }
                                    ?>">
                                        <!--Create content-->
                                        <div class="create-content">
                                            <div class="create-header">
                                                <h4 class="create-title">Choose your Voc topic </h4>
                                            </div>
                                            <div class="create-body" id="topicModal">
                                                <div class="form-response"></div>
                                                {{ Form::open(['route'=>'frontend.storeVocCategory', 'id'=>'voc-category-form']) }}
                                                <!------------------------------------------------------------------------>
                                                <div class="card collapse-icon accordion-icon-rotate">
                                                    <?php
//                                                    dd($vocCategoriesSignUpList);
                                                    if (isset($vocCategoriesSignUpList) && !empty($vocCategoriesSignUpList)) {
                                                        $count = 0;
                                                        foreach ($vocCategoriesSignUpList as $category) {
                                                            ?>
                                                            <div class="card-header">
                                                                <a role="button" class="btn-white card-title lead btn btn-red btn-width cat-subcat-selection">
                                                                    <input type="checkbox" name="voc_categories[]" value="{{$category->id}}" class="category-check">
                                                                    {{ $category->name }}
                                                                </a>
                                                            </div>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <span>No Categories found.</span>
                                                    <?php } ?>
                                                </div>
                                                <!-------------------------------------------------->
                                                <div class='continue-button'>
                                                    <button type="submit" class="btn btn-default-black btn-create">Next</button>
                                                </div>
                                                {{ Form::close() }}
                                                <p class='tag-line'> Let your Voice be heard. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="" id="template" style="display:none;">
                                    <div class="create-voc  create-width">
                                        <!--Create content-->
                                        <div class="create-content">
                                            <div class="create-header">
                                                <h4 class="create-title">Choose your Voc template </h4>
                                            </div>
                                            <div class="create-body" id="templateModal">
                                                <div class="templates">
                                                    <?php
                                                    if (isset($template) && !empty($template)) {
                                                        foreach ($template as $data) {
                                                            ?>
                                                            <div class="each-template">
                                                                <img src="{{ asset('/img/frontend/template/'.$data['cover_image'])}}" class="voc_template" data-template_id="{{$data['id']}}"/>
                                                                <span class="success-tick"><i class="fa fa-check"></i></span>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class='continue-button'>
                                                    <button type="submit" class="next-btn btn btn-default-black btn-create">Next</button>
                                                </div>
                                                <p class='tag-line'> Take control of your presence. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="" id="site-builder" style="display:none;">
                                    <div class="create-voc create-width  site-builder-format">
                                        <!--Create content-->
                                        <div class="create-content site-builder-body">
                                            <div class="create-header">
                                            </div>
                                            <div class="create-body">
                                                <div class="form-response"></div>
                                                {{ Form::open(['route' =>'frontend.storeTemplate','id'=>'siteBuilerForm', 'enctype' => "multipart/form-data"]) }}
                                                <div class="form-group">
                                                     <label for="add_logo">Voc title</label>
                                                    {{ Form::text('title', null, ['placeholder' => "Voc title", 'class'=>'form-input form-control','required'=>'required']) }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="add_logo">Add your logo</label>
                                                    {{ Form::file('logo', null, ['class' => 'form-input form-control logo', 'id'=>'add_logo', 'required'=>'required']) }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="add_cover_image">Add your cover image</label>
                                                    {{ Form::file('cover_image', null, ['class' => 'file form-input form-control','required'=>'required', 'id'=>'add_cover_image']) }}
                                                </div>
                                                <div class="form-group">
                                                     <label for="add_logo">Brief description</label>
                                                    {{ Form::textarea('information', null, ['id' => 'rich-editor', 'placeholder'=>'Brief description about your Voc']) }}
                                                </div>
                                                <div class='continue-button'>
                                                    <button type="submit" class="btn btn-default-black btn-create">Done</button>
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>       

                            </div>

                        </div>
                    </div>

                    <!--Company or Campaigner-->
                    <div class="signupcompany">
                        <div class="inner">  

                            <div class="inner_text">  
                                <h2 id="voc_range">Company or Campaigner</h2>
                                <p>Do you have a Voice that people want to<br> follow? Create your own Voc and let the world<br> hear you through you're own marketing and <br>campaigning website. <br></p>
                                <a href="#" class="join_company btn btn-default-black">JOIN NOW</a>

                            </div>
                            <div class="userform" style="display:none;">
                                <div id="homepage-create">
                                    <div class="create-voc">
                                        <!-- Create Vocdex content-->
                                        <div class="create-content">
                                            <div class="create-header">
                                                <h4 class="create-title">Create your vocdex homepage</h4>
                                            </div>
                                            <div class="create-body">
                                                <div class="form-response"></div>
                                                {{ Form::open(['id'=>'createVocDex', 'route'=>'frontend.storeUser']) }}
                                                {{ Form::hidden('user_role', config('constant.roles_inverse.voc_dex')) }}
                                                <div class="form-group">
                                                    <label for="email">Email address</label>
                                                    {{ Form::email('email', null, ['placeholder' => 'Email', 'class'=>'form-input form-control','required' => 'required','id'=>'email']) }}
                                                </div>
                                                <div class="form-group">
                                                    <label> Zipcode</label>
                                                    {{ Form::text('zip_code', null, ['placeholder' => 'Zipcode (Optional)', 'class'=>'form-input form-control','id'=>'zip_code']) }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Password</label>
                                                    {{ Form::password('password', ['placeholder' => 'Create Password', 'class'=>'form-input form-control','required' => 'required','id'=>'password']) }}
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd"> Confirm Password</label>
                                                    {{ Form::password('password_confirmation',['placeholder' => 'Confirm Password', 'class'=>'form-input form-control','required' => 'required','id'=>'confirm_password']) }}
                                                </div> 

                                                <div class='continue-button'>
                                                    <button type="submit" class="btn btn-default-black btn-vocdex">Create</button>
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="" id="preferences" style="display: none;">
                                    <div class="create-voc create-width">
                                        <!--Create Vocdex content-->
                                        <div class="create-content">
                                            <div class="create-header">
                                                <h4 class="create-title">Welcome tell us a little more</h4>
                                            </div>
                                            <div class="create-body follow">
                                                <div class="form-response"></div>
                                                {{ Form::open(['id'=>'storeVocDexSubCategory', 'route'=>'frontend.storeVocDexSubCategory']) }}
                                                <div class="form-group">
                                                    <label> Favorite Food</label>
                                                    <input name="vocdex_category[0]" class="form-input form-control" placeholder="Favorite Food" required>
                                                </div>
                                                <div class="form-group">
                                                    <label> Favorite genre of Music</label>
                                                    <input name="vocdex_category[1]" class="form-input form-control" placeholder="Favorite genre of music" required>
                                                </div>
                                                <div class="form-group">
                                                    <label> Favorite Sport</label>
                                                    <input name="vocdex_category[2]" class="form-input form-control" placeholder="Favorite sport" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Hobbies</label>
                                                    <input name="vocdex_category[3]" class="form-input form-control" placeholder="Hobbies" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Occupation</label>
                                                    <input type="text" name="vocdex_category[4]" class="form-input form-control" placeholder="Occupation" required>
                                                </div>
                                                <div class='continue-button'>
                                                    <button type="button" class="btn btn-default-black btn-skip">Skip</button>
                                                    <button type="submit" class="btn btn-default-black btn-vocdex">Done</button>
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="" id="carousel" style="display: none;"> 
                                    <div class="create-voc create-width">
                                        <!--Create Vocdex content-->
                                        <div class="create-content">
                                            <div class="create-header">
                                                <h4 class="create-title">Recommended Vocs</h4>
                                            </div>
                                            <div class="create-body">
                                                <div class="form-response"></div>
                                                {{ Form::open(['id'=>'storeRecommendedVocs-form', 'route'=>'frontend.storeRecommendedVocs']) }}

                                                <div class="carousel" id="voc-list-slider">
                                                    <div class="slides">
                                                    </div>
                                                </div>

                                                <div class='continue-button'>
                                                    <!--<button type="button" class="btn btn-default-black btn-skip">Skip</button>-->
                                                    <button type="submit" class="btn btn-default-black btn-vocdex">Done</button>
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>  
                        </div> 
                    </div>
                </div> 
            </div>
        </div>


    </div>

</div>
<div class="push"></div>
<!--voc post end-->
@endsection 
@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script>
var scroll_form_route = '{{route("frontend.scrollSearch")}}',
        storeTemplateIdUrl = "{{ route('frontend.storeVocProfileTemplateId') }}",
        searchSubCatUrl = "{{ route('frontend.searchCategory') }}";
</script>
{!! Html::script(mix('js/signup.js')) !!}   
@endsection