@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Update Promotion')
@section('page-header')
<h1>
    Update Promotion
</h1>
@stop
@section('after-styles')
@endsection
@section('content')
{{ Form::open(['route'=>'frontend.promotion.update','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
{{ Form::hidden ('id', $promotion->id) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Update Promotion</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('header')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('header', 'Header',array('class'=>'control-label','maxlength' => '255','required')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('header', $promotion->header, array('class' => 'form-control', 'maxlength' => '255', 'required')) }}
                <span class="help-block">{{ $errors->first('header') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('sub_header')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('sub_header', 'Sub Header',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('sub_header', $promotion->sub_header, array('class' => 'form-control', 'maxlength' => '255', 'required')) }}
                <span class="help-block">{{ $errors->first('sub_header') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('cover_image')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('cover_image', 'Cover Image',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10 input-group" style="padding: 0 15px;">
                @if(isset($promotion->cover_image))
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                    </a>
                </span>
                {{ Form::text('cover_image', $promotion->cover_image, array('class' => 'form-control', 'id' => "thumbnail"))}}
                @else
                {{ Form::text('cover_image', $promotion->cover_image, array('class' => 'form-control', 'id' => "thumbnail", 'required'))}}
                @endif
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <img id="holder" style="margin-top:15px;max-height:100px;">
            </div>
            <span class="help-block">{{ $errors->first('cover_image') }}</span>
            @if(isset($promotion->cover_image))
            <div class="col-sm-2"></div>
            <div class="col-sm-10 voc">
                <img src="{{ URL::to($promotion->cover_image) }}" class="team-image"/>
            </div>
            @endif
        </div>

        <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
            <div class="col-sm-2 align-right">
                {{ Form::label('body', 'body',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::textarea('body', $promotion->body, array('class' => 'form-control ckeditor', 'required')) }}
                <span class="help-block">{{ $errors->first('body') }}</span>
            </div>
        </div>
    </div>

    <?php
    if (!empty($sharing)) {
        ?>

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Post Options</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'General',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==1)
                        {{ Form::checkbox('sharingOptions[]', $share->id, $promotion->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 align-right">
                        {{ Form::label('sharingOptions[]', 'Share',array('class'=>'control-label','maxlength' => '255')) }}
                    </div>
                    <div class="col-sm-10">
                        @foreach($sharing as $share)
                        @if($share->type==2)
                        {{ Form::checkbox('sharingOptions[]', $share->id, $promotion->postSharingOption->where('sharing_option_id', $share->id)->count() > 0, ['class' => 'field']) }} <?php echo($share->name); ?><br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="box-body">
        <div class="pull-right">
            {{ Form::submit(trans('Save'), ['class' => 'btn btn-success']) }}
        </div>
        {{ Form::close() }}
        <div class="clearfix"></div>
    </div>
</div>

@endsection
@section('after-scripts')
<script src="https://cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="{{url('/')}}/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
$(document).ready(function () {
$('#lfm').filemanager('file', {allow_share_folder: false, prefix: "{{url('/').'/laravel-filemanager'}}"});

    $(function () {
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd'
        });
    });
});
</script>
@endsection 