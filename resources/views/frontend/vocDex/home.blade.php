@extends('frontend.layouts.frontend')

@section('title')
Vocdex Dashboard
@stop

@section('after-styles')
{{ Html::style(elixir('css/vocdex-home.css')) }}
@stop

@section('content')
<!--voc slider-->
@include('frontend.includes.voc_slider',array('page'=>'vocdex-home'))
<!--voc slider end-->

<div class="vocit-posts-container container">
    <h2 class="discover-next">Discover more</h2> 

    <div class="vocit-posts">
        @include('frontend.includes.voc_all_posts',array('page'=>'home','vocPosts'=>isset($vocPosts) ? $vocPosts : []))
    </div>
</div>
@endsection

@section('after-scripts')
<script>
    var scroll_form_route = '{{route("frontend.scrollSearch")}}',
            vocdex_form_route = '{{route("frontend.loadVocDex")}}';
</script>
{!! Html::script(mix('js/vocdex-home.js')) !!}
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.min.js'></script>
@endsection