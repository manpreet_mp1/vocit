@extends('frontend.layouts.frontend') 

@section('title') 
Terms Of Use
@stop 

@section('after-styles') 
{{ Html::style(elixir('css/simple-content-pages.css')) }} 
@stop 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="legal">
                <div class="section">
                    <div class="page_title"><span><i class="fa fa-balance-scale"></i></span> Vocit <span class="orange">Legal</span></div>
                    <br />
                    <br />
                    <h3>Consumer <em class="italic">Data, Analytics, Personal Identifiable Information (DAPII)</em></h3>
                    <h4 class="title bold">The definitions:</h4>
                    <p class="content">The centralized Vocit platform collects user data and turns it into valuable business analytics and then sends it back to the personal consumer's DAPII wallet. The DAPII wallet holds minimal amount of data to allow the consumer to sell their level 1 DAPII to businesses. If consumers want to download their DAPII off of the Vocit platform they can increase the consumer DAPII collected and organize DAPII that can be sold on the blockchain to businesses.</p>
                    <p class="content"><span class="bold">Data</span> &#8212; Consumer-related facts and statistics that is collected on the Vocit platform and sent back to the consumer DAPII Wallet.</p>
                    <p class="content"><span class="bold">Analytics</span> &#8212; Computational analysis of the consumers' data that is collected on the Vocit platform and sent back to the consumer DAPII Wallet.</p>
                    <p class="content"><span class="bold">Personal Identifiable Information</span> &#8212; Consumer information that the consumer would like to put into their DAPII Wallet.</p>
                </div>

                <div class="section">
                    <h4 class="title bold">What we collect and where from:</h4>
                    <p class="content"><span class="bold">Data</span> &#8212; Searches, Vocs visited, Vocs followed, followed topics followed from each Voc, posts clicked, posts saved or forwarded, events saved, activated crypto-assets from post location, computer information, time on platform, time on Vocs, categories searched, jobs applied for, email, blog, chat data, likes, purchases, crypto-asset redemptions.</p>
                    <p class="content"><span class="bold">Analytics</span> &#8212; The knowledge computed from consumer data which include interests, gender, age, tendencies, and artificial intelligence hypothesis of future interests based on similar consumer data.</p>
                    <p class="content"><span class="bold">Personal Identifiable Information</span> &#8212; Date of birth, likes, dislikes, hobbies, location, social contacts, resume.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
{!! Html::script(mix('js/frontend.js')) !!}
@endsection