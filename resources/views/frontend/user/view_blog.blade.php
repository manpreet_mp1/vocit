@extends('frontend.layouts.app')

@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<div class="row">
    <div class="col-xs-4">
        <div class="panel panel-default">
            <div class="panel-heading">Actions</div>  
            <div class="panel-body">
                <ul style="list-style-type:none">
                    <li> <a href="{{route('frontend.user.createCategory')}}">Create Category</a></li>
                    <li><a href="{{route('frontend.user.createBlog')}}">Create Blog</a></li>
                    <li> <a href="{{route('frontend.user.allBlog')}}">List Blogs</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-8">
        <div class="panel panel-default">
            <div class="panel-heading"><?php echo $data->title; ?> Blog</div>
            <div class="panel-body">
                <div class="form-group">
                    {{ Form::label('title', 'Title', ['class' => 'col-md-4 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('title', $data->title, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Title','readonly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::textarea('description', $data->description,['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Description','readonly'], ['size' => '30x5']) }}
                    </div>
                </div>
                {{ Form::open(['method' => 'POST', 'route' => ['frontend.user.addComment', $data->id],'class' => 'form-horizontal'])  }}
                <div class="form-group">
                    {{ Form::label('add-comment', 'Add comment', ['class' => 'col-md-4 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::textarea('comment',null,['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Comment'], ['size' => '30x5']) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {{ Form::submit(trans('Add comment'), ['class' => 'btn btn-primary', 'id' => 'add-comment']) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection


