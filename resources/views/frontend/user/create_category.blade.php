@extends('frontend.layouts.app')

@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<div class="row">
    <div class="col-xs-4">
        <div class="panel panel-default">
            <div class="panel-heading">Actions</div>  
            <div class="panel-body">
                <ul style="list-style-type:none">
                    <li> <a href="{{route('frontend.user.createCategory')}}">Create Category</a></li>
                    <li><a href="{{route('frontend.user.createBlog')}}">Create Blog</a></li>
                    <li> <a href="{{route('frontend.user.allBlog')}}">List Blogs</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-8">
        <div class="panel panel-default">
            <div class="panel-heading">Create Category</div>

            <div class="panel-body">
                {{ Form::open(['route' => 'frontend.user.storeCategory', 'class' => 'form-horizontal', 'method' => 'POST','role'=>'form','files'=>true]) }}

                <div class="form-group">
                    {{ Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Category Name']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {{ Form::submit(trans('Create'), ['class' => 'btn btn-primary', 'id' => 'create-blog']) }}
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
