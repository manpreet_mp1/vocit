@extends('frontend.layouts.app')

@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<div class="row">
    <div class="col-xs-4">
        <div class="panel panel-default">
            <div class="panel-heading">Actions</div>  
            <div class="panel-body">
                <ul style="list-style-type:none">
                    <li> <a href="{{route('frontend.user.createCategory')}}">Create Category</a></li>
                    <li><a href="{{route('frontend.user.createBlog')}}">Create Blog</a></li>
                    <li> <a href="{{route('frontend.user.allBlog')}}">List Blogs</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-8">
        <div class="panel panel-default">
            <div class="panel-heading">All Blogs</div>
            <div class="panel-body">
                <table class="table table-bordered" id="users-table">
                    <thead>
                        <tr>
                            <!--<th>Name</th>-->
                            <th>Title</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($all_blogs as $data) { ?>
                            <tr>
                                <!--<td> <?php echo $data->name; ?></td>--> 
                                <td> <?php echo $data->title; ?></td> 
                                <td> <?php echo $data->category; ?></td> 
                                <td><a href="{{route('frontend.user.viewBlog',$data->id)}}">View</a>&nbsp;<a href="{{route('frontend.user.editBlog',$data->id)}}">Edit</a> &nbsp;<a href="{{ url('deleteBlog/'.$data->id) }}">Delete</a></td> 
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
