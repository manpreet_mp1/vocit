@extends('frontend.layouts.app')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">Create Voc</div>

            <div class="panel-body">
                {{ Form::open(['route' => 'frontend.user.storeVoc', 'class' => 'form-horizontal', 'method' => 'POST']) }}
                <div class="form-group">
                    {{ Form::label('title', 'Voc Name', ['class' => 'col-md-4 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::text('title', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Voc Name']) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {{ Form::submit(trans('labels.general.buttons.update'), ['class' => 'btn btn-primary', 'id' => 'update-profile']) }}
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    @endsection
