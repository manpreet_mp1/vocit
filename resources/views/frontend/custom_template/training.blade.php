@extends('frontend.layouts.custom_template') 

@section('title')
{{$voc->title}}
@stop

@section('after-styles') 
{{ Html::style(elixir('css/training.css')) }}
@stop 

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="go-to-section">
                <div class="page_title"><span><i class="fa fa-leanpub"></i></span> Vocit <span class="orange">Learn</span></div>
                <ul>
                    <li class="homepage_section"><a href="https://trello-attachments.s3.amazonaws.com/5a817490a8bc7d680cc36168/5b2710bcb0c942a9ee78257c/77edcce7cdac3424ec03496a2d5002f7/vocit-training.html#homepage_training">Homepage</a></li>
                    <li class="vocdex_section"><a href="https://trello-attachments.s3.amazonaws.com/5a817490a8bc7d680cc36168/5b2710bcb0c942a9ee78257c/77edcce7cdac3424ec03496a2d5002f7/vocit-training.html#vocdex_training">Vocdex</a></li>
                    <li class="business_admin_section"><a href="https://trello-attachments.s3.amazonaws.com/5a817490a8bc7d680cc36168/5b2710bcb0c942a9ee78257c/77edcce7cdac3424ec03496a2d5002f7/vocit-training.html#business_admin_training">Business Admin</a></li>
                </ul>
            </div>
            <div id="homepage_training" class="voc_section training home-page">
                <h3>Vocit Homepage <span class="sub_title">The Platform's homepage</span></h3>
                <div class="inner">
                    <ul class="video_collection">
                        <li>
                            <div class="video">
                                <video src="Vocdex_preview.mp4" controls=""></video>
                                <p>Vocit is moving at a fast pace with popularity, to see the latest you will want to look on the homepage. Below are the four ways we show you these Vocdex's.</p>
                            </div>
                        </li>
                        <li>
                            <div class="video">
                                <video src="Vocdex_preview.mp4" controls="">

                                </video>
                                <p>Vocit is moving at a fast pace with popularity, to see the latest you will want to look on the homepage. Below are the four ways we show you these Vocdex's.</p>
                            </div>
                        </li>
                        <li>
                            <div class="video">
                                <video src="Vocdex_preview.mp4" controls=""></video>
                                <p>Vocit is moving at a fast pace with popularity, to see the latest you will want to look on the homepage. Below are the four ways we show you these Vocdex's.</p>
                            </div>
                        </li>
                    </ul>
                    <div class="most-popular-vocs">
                        <ul>
                            <li>
                                All
                                <p>All Vocdex's that are on the Vocit platform will be displayed. Clicking each one will take you to the Vocdex site.</p>
                            </li>
                            <li>
                                Trending
                                <p>The most visited Vocdex's will show up here, this can include both Nearby and Regional Voc's.</p>
                            </li>
                            <li>
                                Nearby
                                <p>Vocdex's in your area show up, can include regional and trending.</p>
                            </li>
                            <span></span>
                            <li>
                                Regional
                                <p>All regional </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="vocdex_training" class="voc_section training user-page">
                <h3>Vocdex <span class="sub_title">A consumer's homepage</span></h3>
                <div class="inner">
                    <div class="video">
                        <video src="Vocdex_preview.mp4" controls=""></video>
                        <p>Your Vocdex is the key to connecting users with your business. It will highlight all the key points about your
                            business and what you can offer. We are currently making our editor more robust with better user experience,
                            but for now, one of our Vocit Developers can quickly create a Vocdex that will capture some followers. Please
                            fill out the form below to get started.</p>
                    </div>
                </div>
            </div>
            <div id="business_admin_training" class="voc_section training business-admin">
                <h3>Voc Administration Page <span class="sub_title">The business admin page that allows a business to edit their website, create posts, and manage their business</span></h3>
                <div class="inner">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container main_content">
    <div class="row">
        <div class="col-sm-12 before-footer-divs">
            <div class="all-vocs">
                <div id="pinBoot">
                    @include('frontend.custom_template.posts.promotion')
                    @include('frontend.custom_template.posts.voice')
                    @include('frontend.custom_template.posts.jobs')
                    @include('frontend.custom_template.posts.update')
                    @include('frontend.custom_template.posts.event')
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main_content">
    <div class="all-vocs">
        <div id="pinBoot">
            @include('frontend.includes.voc_post_mini_modal')
        </div>
    </div>
</div>

@endsection