@extends('frontend.layouts.custom_template') 

@section('title')
{{$voc->title}}
@stop

@section('after-styles') 
{{ Html::style(elixir('css/voc_two_template.css')) }}
@stop 

@section('content')
<div class="container main_content">
    <div class="row">
        <div class="col-sm-12 outer-content">
            <div class="col-sm-4 description">
                {!! $voc->information !!}
            </div>
            <div class="col-sm-8 image">
                <img src="{{ url('/').config('constant.voc_image_path').$voc->image }}" class="head-image">
            </div>
        </div>
    </div>
    <div class="vocit-posts">
        @include('frontend.custom_template.posts.promotion')
        @include('frontend.custom_template.posts.voice')
        @include('frontend.custom_template.posts.jobs')
        @include('frontend.custom_template.posts.update')
        @include('frontend.custom_template.posts.event')
    </div>
</div>
@endsection