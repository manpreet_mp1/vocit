@if(isset($vocPosts) && !empty ($vocPosts))
@foreach($vocPosts as $vocPost)
@if($vocPost instanceof App\Models\Voc\VocPromotion)
<div class="voc-post col-xs-12 col-sm-4 col-md-3">
    <div class="post-back-image">
        <div class="post-image" style="background-image:url({{ URL::asset($vocPost->cover_image)}})"></div>
        <div class="post-name">Promotion</div>
    </div>
    <div class="post-desc">
        <h3 class="heading">{{ $vocPost->header }}</h3>
        <div class="subheader">{{ $vocPost->sub_header }}</div>
        <div class="desc">
            <?php
            $voc_image = URL::asset($vocPost->cover_image);
            $voc_title = $vocPost->header;
            $voc_body = html_entity_decode(strip_tags($vocPost->body));
            ?>
            {!! substr($voc_body,0,60) . "..." !!}
        </div>
        <div class="extra">
            <p class="extra-head">when</p>
            <p class="extra-desc">August 21</p>
        </div>
        <div class="options">
            <div class="share-all">
                <span class="voc-share"></span>
                @include('frontend.custom_template.posts.post_footer')
            </div>
          <?php 
            $currentRouteName = Route::currentRouteName();
            if($currentRouteName != 'frontend.getSingleVoc'){ ?>
            <a href="{{route('frontend.getSingleVoc', $vocPost->voc_id)}}">
                <span class="voc-out"></span>
            </a>
            <?php } ?>
        </div>
    </div>
</div>
@endif
@endforeach
@endif