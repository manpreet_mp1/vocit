@if(isset($vocPosts) && !empty ($vocPosts))
@foreach($vocPosts as $vocPost)
@if($vocPost instanceof App\Models\Voc\VocEvent)
<div class="voc-post job-event-post col-xs-12 col-sm-4 col-md-3">
    <div class="post-back-image">
        <img src="{{ $vocPost->logo }}" alt="" class="post-logo"/>
        <div class="post-name">Event</div>
    </div>
    <div class="post-desc">
        <h3 class="heading">{{$vocPost->name}}</h3>
        <div class="desc">
            <?php
            $voc_image = $vocPost->logo;
            $voc_title = $vocPost->name;
            $voc_body = html_entity_decode(strip_tags($vocPost->body));
            ?>
            {!! substr($voc_body,0,60) . "..." !!}
        </div>
        <div class="extra">
            <p class="extra-head">when</p>
            @php
            $commingStartDate  = $vocPost->start;
            $formatedStartDate = date("F j, Y",strtotime(str_replace('-', '/', $commingStartDate)));
            @endphp
            <p class="extra-desc">{{ $formatedStartDate }}</p>
        </div>
        <div class="options">
            <div class="share-all">
                <span class="voc-share"></span>
                @include('frontend.custom_template.posts.post_footer')
            </div>
           <?php 
            $currentRouteName = Route::currentRouteName();
            if($currentRouteName != 'frontend.getSingleVoc'){ ?>
            <a href="{{route('frontend.getSingleVoc', $vocPost->voc_id)}}">
                <span class="voc-out"></span>
            </a>
            <?php } ?>
        </div>
    </div>
</div>
@endif
@endforeach
@endif