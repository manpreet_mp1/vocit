@if(isset($vocPosts) && !empty ($vocPosts))
@foreach($vocPosts as $vocPost)
@if($vocPost instanceof App\Models\VocDashboard\Update)
<div class="voc-post col-xs-12 col-sm-4 col-md-3">
    <div class="post-back-image">
        <?php
        if ($vocPost->media_type == config('constant.media_type.image')) {
            $voc_image = $back_image = URL::asset($vocPost->media);
            ?>
            <div class="post-image" style="background-image:url({{$back_image }})"></div>  
        <?php } ?>
        <?php
        if ($vocPost->media_type == config('constant.media_type.video_link')) {
            $voc_image = $vocPost->media;
            ?>
            <iframe width="100%" height="210" frameborder="0" allowfullscreen src="{{$vocPost->media}}" class="video-iframe"></iframe>
        <?php } ?>
        <div class="post-name">Update</div>
    </div>
    <div class="post-desc">
        <h3 class="heading">{{$vocPost->title}}</h3>
        <div class="desc">
            <?php
            $voc_title = $vocPost->title;
            $voc_body = html_entity_decode(strip_tags($vocPost->body));
            ?>
            {!! substr($voc_body,0,60) . "..." !!}
        </div>
        <div class="options">
            <div class="share-all">
                <span class="voc-share"></span>
                @include('frontend.custom_template.posts.post_footer')
            </div>
            <?php 
            $currentRouteName = Route::currentRouteName();
            if($currentRouteName != 'frontend.getSingleVoc'){ ?>
            <a href="{{route('frontend.getSingleVoc', $vocPost->voc_id)}}">
                <span class="voc-out"></span>
            </a>
            <?php } ?>
        </div>
    </div>
</div>
@endif
@endforeach
@endif