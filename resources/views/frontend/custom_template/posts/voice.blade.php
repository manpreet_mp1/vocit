@if(isset($vocPosts) && !empty ($vocPosts))
@foreach($vocPosts as $vocPost)
@if($vocPost instanceof App\Models\VocDashboard\Voice)
<div class="voc-post voice-post col-xs-12 col-sm-4 col-md-3">
    <div class="post-back-image">
        <img src="{{ $vocPost->profile_image }}" alt="" class="post-logo"/>
        <div class="post-name">Voice</div>
    </div>
    <div class="post-desc">
        <h3 class="heading">{{$vocPost->name}}</h3>
        <div class="desc">
            <?php
            $voc_image = $vocPost->profile_image;
            $voc_title = $vocPost->name;
            $voc_body = html_entity_decode(strip_tags($vocPost->body));
            ?>
            {!! substr($voc_body,0,60) . "..." !!}
        </div>
        <div class="options">
            <div class="share-all">
                <span class="voc-share"></span>
                @include('frontend.custom_template.posts.post_footer')
            </div>
           <?php 
            $currentRouteName = Route::currentRouteName();
            if($currentRouteName != 'frontend.getSingleVoc'){ ?>
            <a href="{{route('frontend.getSingleVoc', $vocPost->voc_id)}}">
                <span class="voc-out"></span>
            </a>
            <?php } ?>
        </div>
    </div>
</div>
@endif
@endforeach
@endif  