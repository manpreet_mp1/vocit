    @if (Auth::user() == null)
    <div class="share-buttons">
        <a href="" class="social-share">
            {!! config('constant.sharing.Facebook') !!}
        </a>
        <a href="" class="social-share">
            {!! config('constant.sharing.Linkedin') !!}
        </a>
        <a href="" class="social-share">
            {!! config('constant.sharing.Pinterest') !!}
        </a>
        <a href="" class="social-share">
            {!! config('constant.sharing.Twitter') !!}
        </a>
        <a href="" class="social-share">
            {!! config('constant.sharing.Google+') !!}
        </a>
    </div>
    @else
    
    @if(isset($vocPost->postSharingOption) && $vocPost->postSharingOption->count() > 0)
    <div class="share-buttons">
        @foreach($vocPost->postSharingOption as $postSharingOption)

        @if(($postSharingOption->sharingOptions->name) ==  config('constant.inverse_sharing')[2])
        <a href="https://www.facebook.com/sharer.php?u={{URL::to('/')}}&app_id=179129739593908&picture={{$voc_image}}&caption={{$voc_title}}&description={{$voc_body}}" class="social-share">
            {!! config('constant.sharing')[$postSharingOption->sharingOptions->name] !!}
        </a>

        @elseif (($postSharingOption->sharingOptions->name) == config('constant.inverse_sharing')[6])
        <a href="https://plus.google.com/share?url={{URL::to('/')}}&text={{$voc_title}}&source={{$voc_image}}" class="social-share">
            {!! config('constant.sharing')[$postSharingOption->sharingOptions->name] !!}
        </a>

        @elseif (($postSharingOption->sharingOptions->name) == config('constant.inverse_sharing')[3])
        <a href="https://twitter.com/share?url={{URL::to('/')}}&text={{$voc_title}}" class="social-share">
            {!! config('constant.sharing')[$postSharingOption->sharingOptions->name] !!}
        </a>

        @elseif (($postSharingOption->sharingOptions->name) == config('constant.inverse_sharing')[4])
        <a href="https://www.linkedin.com/shareArticle?mini=true&url={{URL::to('/')}}&title={{$voc_title}}&summary={{$voc_body}}&source={{$voc_image}}" class="social-share">
            {!! config('constant.sharing')[$postSharingOption->sharingOptions->name] !!}
        </a>

        @elseif (($postSharingOption->sharingOptions->name) == config('constant.inverse_sharing')[5])
        <a href="http://pinterest.com/pin/create/button/?url={{URL::to('/')}}&media={{$voc_image}}&description={{$voc_title}}" class="social-share">
            {!! config('constant.sharing')[$postSharingOption->sharingOptions->name] !!}
        </a>
        @endif
        @endforeach
    </div>
    @endif
    @endif
