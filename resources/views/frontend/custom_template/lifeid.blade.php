@extends('frontend.layouts.custom_template') 

@section('title')
{{$voc->title}}
@stop

@section('after-styles') 
{{ Html::style(elixir('css/lifeid.css')) }}
@stop 

@section('content')
<!-- Header -->
<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-heading">Reclaim your digital identity
                <h3>with convenience, security and privacy.</h3>
            </div>
            <h4>
                lifeID is the open-source, blockchain-based platform for self-sovereign identity</h4>
            <a class="btn btn-xl js-scroll-trigger" data-scroll="" href="https://lifeid.io/whitepaper.pdf" target="_blank">View White Paper</a>
        </div>
    </div>
</header>

<section class="" id="product">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 lede-text larger-text" >
                lifeID foundation is creating the identity toolbox everyone needs to control how their data is used online, and in the physical world. Any app maker can use our open-source platform to ensure their apps provide convenience, security and privacy.
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-3 text-center">
            </div>

            <div class="col-lg-8 text-center">

                <p class="larger-text">lifeID foundation announces partnership with RChain Cooperative</p>
                <div class="video-container">

                    <iframe width="560" height="315" src="https://www.youtube.com/embed/uHEEJ9bevV0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-md-2"> </div>
            <div class="col-md-12 text-center">
                <p><a href="https://www.youtube.com/channel/UCGzdGLm38e5Tpy7U1DOvS2g"> To see more, please visit us on our youtube channel.</a>
            </div>
        </div>
</section>

<!-- Services -->
<section class="bg-light" id="features">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading text-uppercase">Self-Sovereign Identity on the lifeID Platform </h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 ">
                <h3 class="">Driven by Economic Incentive</h3>
                <p>Economic incentives,  perhaps one of the greatest human innovations, is the lynchpin of the lifeID Platform.</p>
                <p> Self-sovereign identity platforms must have built-in incentive mechanisms to ensure the ecosystem grows. Incentives also ensure ongoing SLAs to support identity ownership and management for the life of the user.</p>
            </div>
            <div class="col-md-4 ">
                <h3 class="">Open & Permissionless</h3>
                <p>An Open and permissionless platform is a definitional requirement for building a self-sovereign identity service. It’s essential that anyone, anywhere in the world can use the identity service, restriction free. The most important reason for this open access is to prevent an identity holder from ever being denied use of their digital identity for any reason. </p>
            </div>
            <div class="col-md-4 ">
                <h3 class="">Self-Governance</h3>
                <p>It’s essential that identity holders can participate in the governance of their identity infrastructure. The governance mechanisms must be both transparent and clearly communicated. Transparent governance builds a strong community by demonstrating fairness and accountability. Governance cannot truly be fair unless the mechanisms are described consistently and clearly to the community and stakeholders.
                </p>
            </div>
        </div>
</section>

<section id="as-seen-in">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading text-uppercase">As Seen In</h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6 vert-center">
                <a href="http://www.businessinsider.sg/disney-blockchain-creators-build-commercial-platform-on-dragonchain-with-ico-2017-9/" target="_blank">
                    <img class="img-fluid d-block mx-auto" src="{{ url('/').'/img/frontend/template/'.$vocTemplate->file_name }}/Businessinsider-color.png" alt="">
                </a>
            </div>
            <div class="col-md-6 col-sm-6 vert-center">
                <a href="https://www.marketwatch.com/story/lifeid-launches-company-and-blockchain-based-technology-to-transform-identity-authentication-while-eliminating-current-risks-to-privacy-and-security-2017-11-20" target="_blank">
                    <img class="img-fluid d-block mx-auto" src="{{ url('/').'/img/frontend/template/'.$vocTemplate->file_name }}/marketwatch-logo-vector-download.png" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<section class="bg-light" id="careers" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>Careers</h1>
            </div>
            <div class="col-lg-12 text-xs-center">
                <p>We're currently hiring! Please check out our <a href="https://lifeid.workable.com/" target="_blank">available opportunities.</a> </p>
            </div>
        </div>
    </div>
</section>

<!-- Contact -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Stay in touch</h2>
            </div>
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6">

                <h3 class="section-subheading " style="font-size: 18px; line-height: 26px"><span class="no-text-transform">lifeID</span> is revolutionizing the way you use your identity. 
                    Find us at <a href="https://lifeid.io/">lifeid.io</a>.</h3>

                <ul class="list-inline social-buttons">
                    <li class="list-inline-item">
                        <a href="https://twitter.com/lifeID_io" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/lifeid.io/" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.linkedin.com/company/18251138/" target="_blank">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<div class="container main_content">
    <div class="row">
        <div class="col-sm-12 before-footer-divs">
            <div class="all-vocs">
                <div id="pinBoot">
                    @include('frontend.custom_template.posts.promotion')
                    @include('frontend.custom_template.posts.voice')
                    @include('frontend.custom_template.posts.jobs')
                    @include('frontend.custom_template.posts.update')
                    @include('frontend.custom_template.posts.event')
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main_content">
    <div class="all-vocs">
        <div id="pinBoot">
            @include('frontend.includes.voc_post_mini_modal')
        </div>
    </div>
</div>
@endsection