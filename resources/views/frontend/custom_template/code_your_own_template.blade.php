@extends('frontend.layouts.code_your_own_layout') 

@section('title')
{{$voc->title}}
@stop

@section('after-styles') 
{{ Html::style(elixir('css/voc_one_template.css')) }}
{!! $vocCodeOwn->style !!}
@stop 

@section('content')
<div class="container main_content">
    <div class="row">
        <div class="voc_full_content">
            {!! $vocCodeOwn->markup !!}
        </div>
        <div class="col-sm-12 before-footer-divs">
            <div class="all-vocs">
                <div id="pinBoot">
                    @include('frontend.custom_template.posts.promotion')
                    @include('frontend.custom_template.posts.voice')
                    @include('frontend.custom_template.posts.jobs')
                    @include('frontend.custom_template.posts.update')
                    @include('frontend.custom_template.posts.event')
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main_content">
    <div class="all-vocs">
        <div id="pinBoot">
            @include('frontend.includes.voc_post_mini_modal')
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
{!! $vocCodeOwn->script !!}
@endsection