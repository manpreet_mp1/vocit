@extends('frontend.layouts.custom_template') 

@section('title')
{{$voc->title}}
@stop

@section('after-styles') 
{{ Html::style(elixir('css/voc_one_template.css')) }}
@stop 

@section('content')
<div class="container main_content">
    <div class="">
        <img src="{{ url('/').config('constant.voc_image_path').$voc->image }}" class="head-image">
        <div class="description">
            {!! $voc->information !!}
        </div>
    </div>
    <div class="vocit-posts">
        @include('frontend.custom_template.posts.promotion')
        @include('frontend.custom_template.posts.voice')
        @include('frontend.custom_template.posts.jobs')
        @include('frontend.custom_template.posts.update')
        @include('frontend.custom_template.posts.event')
    </div>
</div>
@endsection