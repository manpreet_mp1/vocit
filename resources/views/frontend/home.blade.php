@extends('frontend.layouts.frontend') 

@section('title') 
Home
@stop 

@section('after-styles') 
{{ Html::style(elixir('css/home.css')) }} 
@stop 

@section('content')
<div class="main_content">
    <div class="container">

        <div class="row">
            {{ Form::open(['route' => ['frontend.vocSearch'],'class' => 'search_form', 'role' => 'form', 'method' => 'get']) }}
            <!--left_tabs-->
            <div class="col-xs-6 col-sm-9 tab_section_outer">
                <div class="tab_section hidden-xs">
                    <h2>Most Popular <span></span></h2>
                    <ul class="left_tab list-inline">
                        <li class="popular-searches selected-option" data-range='{{ config('constant.vocs.0') }}'><a href="{{ route('frontend.vocSearch') }}" class="">{{ config('constant.vocs.0') }}</a></li>
                        <li class="popular-searches" data-range='{{ config('constant.vocs.3') }}'><a href="{{ route('frontend.vocSearch') }}" class="">{{ config('constant.vocs.3') }}</a></li>
                        <li class="popular-searches" data-range='{{ config('constant.vocs.2') }}'><a href="{{ route('frontend.vocSearch') }}" class="">{{ config('constant.vocs.2') }}</a></li>
                        <li class="popular-searches" data-range='{{ config('constant.vocs.1') }}'><a href="{{ route('frontend.vocSearch') }}" class="">{{ config('constant.vocs.1') }}</a></li>
                    </ul>
                </div>

                <div class="tab_section_mobile visible-xs-inline-block">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <h2>Most Popular <span></span></h2>
                    </a>
                    <ul class="dropdown-menu">
                        <li class=" popular-searches selected-option" data-range='{{ config('constant.vocs.0') }}'><a href="{{ route('frontend.vocSearch') }}" class="">{{ config('constant.vocs.0') }}</a></li>
                        <li class=" popular-searches" data-range='{{ config('constant.vocs.3') }}'><a href="{{ route('frontend.vocSearch') }}" class="">{{ config('constant.vocs.3') }}</a></li>
                        <li class=" popular-searches" data-range='{{ config('constant.vocs.2') }}'><a href="{{ route('frontend.vocSearch') }}" class="">{{ config('constant.vocs.2') }}</a></li>
                        <li class=" popular-searches" data-range='{{ config('constant.vocs.1') }}'><a href="{{ route('frontend.vocSearch') }}" class="">{{ config('constant.vocs.1') }}</a></li>
                    </ul>
                </div>
            </div>
            <!--Search-->
            <div class="col-xs-6 col-sm-3 search_section_outer">
                <div class="search_section">
                    <div class="input-group search_field">
                        <input type="text" class="voc_search_input form-input-line" placeholder="Search">
                        <span class="search-btn"></span>
                        <div id="search-no-data">No Results Found.</div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<!--voc slider-->
@include('frontend.includes.voc_slider',array('page'=>'home'))
<!--voc slider end-->

<div class="vocit-posts-container container">
    <h2 class="discover-next">Discover more</h2> 
    <div class="sort-by">
        <span>Sort By:</span>
        <span id="category-popup">Category</span>
    </div>
    <div id="category-outer-popup">
        <div class="overlay overlay-scale">
            <button type="button" class="overlay-close">Close</button>
            <nav>
                <div class="container">
                    <h2>Choose Categories</h2>
                          {{ Form::open(['route' => ['frontend.vocSearch'],'class' => 'search_category_form', 'role' => 'form', 'method' => 'get']) }}
                        <?php if (isset($vocCategories) && !empty($vocCategories)) { ?>
                            <ul>
                                @foreach($vocCategories as $categories)
                                <li>
                                    <a href="#" data-cat_id="{{ $categories->id }}">
                                        <input type="hidden" value=""/>
                                        {{ $categories->name }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        <?php } ?>
                        <button type="submit" class="btn btn-default-black">Choose</button>
                        {{ Form::close() }}
                </div>
            </nav>
        </div>
    </div>
    <div class="vocit-posts">
        @include('frontend.includes.voc_all_posts',array('page'=>'home','vocPosts'=>isset($vocPosts) ? $vocPosts : []))
    </div>
</div>
@endsection

@section('after-scripts')
<script>
    var scroll_form_route = '{{route("frontend.scrollSearch")}}';
</script>

{!! Html::script(mix('js/home.js')) !!}
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.min.js'></script>
@endsection