@extends('frontend.layouts.voc_dashboard')

@section ('title', 'Edit Voc')

@section('page-header')
<div class="pull-left">
    <a href="{{route("frontend.voice.template")}}" class="btn btn-danger">Change Template</a>
</div>
<div class="pull-right">
    <a href="{{route("frontend.getSingleVoc")}}" class="btn btn-black preview" target="_blank">Preview</a>
    <a href="#" class="btn btn-success" id="preview-upper-save">Save</a>
</div>
<div class="clearfix"></div>
@stop

@section('content')
<div class="box wide-box">

    <div class="box-body col-xs-8 col-sm-8" id="codeYourOwnDiv">
        <iframe src="http://dev.vocit.work/getSingleVoc" frameborder="0" width="100%" marginheight="0" marginwidth="0" scrolling="no" id="codeYourOwnIframe"></iframe>
    </div>

    <div class="box-body col-xs-4 col-sm-4 box-body-right">
        <div data-type="content-block" class="page-block-title col-xs-6 col-sm-6">
            Your Code
            <span>Styles, Scripts, Markups</span>
        </div>
        <div data-type="page-settings" class="page-block-title col-xs-6 col-sm-6 active">
            Page Settings
            <span>Title, Logo, Description</span>
        </div>
        <div class="clearfix"></div>

        <div class="page-block-content content-block" style="display:none;">
            <div class="form-response"></div>
            {{ Form::open(['route'=>'frontend.voice.codeYourOwnSave','id' => 'codeYourOwnForm', 'role' => 'form', 'method' => 'post','enctype' => "multipart/form-data"]) }}
            <div class="form-group">
                {{ Form::label('style', 'Voc Style',array('class'=>'control-label')) }}
                {{ Form::textarea('style', null , array('class' => 'form-control', 'placeholder'=>'Add your style here.', 'required'=>true)) }}
                <small>Your style must contain style tag.</small>
            </div>
            <div class="form-group">
                {{ Form::label('script', 'Voc Script',array('class'=>'control-label')) }}
                {{ Form::textarea('script', null , array('class' => 'form-control', 'placeholder'=>'Add your script here.', 'required'=>true)) }}
                <small>Your script must contain script tag.</small>
            </div>
            <div class="form-group">
                {{ Form::label('markup', 'Voc Markup',array('class'=>'control-label')) }}
                {{ Form::textarea('markup',null , array('class' => 'form-control', 'placeholder'=>'Add your html here.', 'required'=>true)) }}
            </div>

            <div class="form-submitter">
                {{ Form::submit('Save Changes', ['class' => 'btn btn-success']) }}
            </div>
            {{ Form::close() }}
            <div class="clearfix"></div>
        </div>

        <div class="page-block-content page-settings">
            <div class="form-response"></div>
            {{ Form::open(['route'=>'frontend.voice.vocUpdate','class' => 'page-settings-form', 'role' => 'form', 'method' => 'post','enctype' => "multipart/form-data"]) }}
            <div class="form-group">
                {{ Form::label('title', 'Title',array('class'=>'control-label')) }}
                {{ Form::text('title', $voc->title , array('class' => 'form-control','max-length' => '100', 'required')) }}
            </div>

            <div class="form-group">
                {{ Form::label('logo','Logo', ['class' => 'control-label']) }}
                <div class="">
                    @if(isset($voc->logo))
                    <input type="file" name="logo" class="control-label-input"/>
                    @else
                    <input type="file" name="logo" class="control-label-input" required/>
                    @endif
                </div>
                @if(isset($voc->logo))
                <div class="voc">
                    <img src="{{ URL::to('/uploads').'/'.$voc->logo }}" class="voc-image"/>
                </div>
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('information', 'Brief Description',array('class'=>'control-label')) }}
                {{ Form::textarea('information', $voc->information , array('class' => 'form-control','max-length' => '100', 'required')) }}
            </div>

            <div class="form-submitter">
                {{ Form::submit('Save Changes', ['class' => 'btn btn-success']) }}
            </div>
            {{ Form::close() }}
            <div class="clearfix"></div>
        </div>
    </div>

</div>
@endsection

@section('after-scripts')
<script>
    $(window).load(function () {
        //Hide sidebar when this page opens
        if ($(window).width() > 768) {
            $('.sidebar-toggle').click();
        }
        //END
        vocApp.resizeIframe('codeYourOwnIframe', 'codeYourOwnDiv');
    });
</script>
{{ Html::script(mix('js/voc_dashboard.js')) }}
@endsection