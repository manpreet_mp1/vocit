@extends('frontend.layouts.frontend')

@section('after-styles')
{{ Html::style(elixir('css/topic.css')) }}
@stop

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2 left-side-data">
            <ul>
                <li><a href="#"><i class="fa fa-envelope left-side-icons" aria-hidden="true"></i>Email</a></li>
                <li><a href="#"><i class="fa fa-th left-side-icons" aria-hidden="true"></i>Blogs</a></li>
                <li><a href="#"><i class="fa fa-th-large left-side-icons" aria-hidden="true"></i>Collections</a></li>
                <li><a href="#"><i class="fa fa-calendar left-side-icons" aria-hidden="true"></i>Calendar</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 all-vocs border-left">
            <h3>All VOC's</h3>

            <h3>Categories</h3>
            <!--<div class="row">-->
            <div class="col-sm-12 categories-buttons">
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">Businesses</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">Sports</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">Music</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">Food</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">Politicians</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">Favorites</a>
                </div>
            </div>
            <div class="col-sm-12 categories-buttons">
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">Employment</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">Theater</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">Hobbies</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">+</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">+</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 p-top">
                    <a href="#" class="btn btn-grey">+</a>
                </div>
            </div>
            <div class="col-sm-12 search-input-field">
                <div class="input-text">
                    <input type="text" placeholder="Search VOCS">
                    <i class="search-icon" aria-hidden="true"></i>
                </div>
            </div>
            <div class="col-sm-12 before-footer-divs">
                <div class="col-sm-6 col-md-3">
                    <h4>Current Promotions</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lacus aliquet,
                        fringilla arcu at, aliquam tortor. Nulla facilisi.<br><span>[More]</span></p>

                </div>
                <div class="col-sm-6 col-md-3">
                    <h4>Current Promotions</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lacus aliquet,
                        fringilla arcu at, aliquam tortor. Nulla facilisi.<br><span>[More]</span></p>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4>Current Promotions</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lacus aliquet,
                        fringilla arcu at, aliquam tortor. Nulla facilisi.<br><span>[More]</span></p>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4>Current Promotions</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lacus aliquet,
                        fringilla arcu at, aliquam tortor. Nulla facilisi.<br><span>[More]</span></p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection