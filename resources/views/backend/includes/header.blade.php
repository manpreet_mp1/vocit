<header class="main-header" id="voc-dashboard-header">
    <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            {{ substr(app_name(), 0, 1) }}
        </span>

        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            {{ Html::image("img/frontend/vocit_logo.png" ,'vocit_logo', ["class" => "logo"]) }}
        </span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ access()->user()->picture }}" class="user-image" alt="User Avatar"/>
                        <span class="hidden-xs">{{ access()->user()->full_name }}</span>
                        <i class="fa fa-angle-left"></i>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="user-footer">
                            <a href="{!! route('frontend.auth.logout') !!}" class="btn">
                                <i class="fa fa-sign-out"></i>
                                {{ trans('navs.general.logout') }}
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-custom-menu -->
    </nav>
</header>
