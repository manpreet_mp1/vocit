@extends ('backend.layouts.app')

@section ('title', 'Create Voc')
@section('page-header')
<h1>
    Create Voc
</h1>
@stop
@section('content')
{{ Form::open(['route'=>'admin.storeVoc','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Create Voc</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group">
            <div class="col-sm-2 align-right">
                {{ Form::label('title', 'Title',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-10">
                {{ Form::text('title', null, array('class' => 'form-control','max-length' => '100', 'required')) }}
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!--box box-success-ends-->
<!--box box-info-->
<div class="box box-info">
    <!-- /.box-body -->
    <div class="box-body">
        <div class="pull-right">
            {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success']) }}
        </div>
        {{ Form::close() }}
        <div class="clearfix"></div>
    </div>
    <!-- /.box-body-ends -->
</div>
<!--box box-info-ends-->
<!--box-->
@stop

@section('after-scripts-end')
@stop
