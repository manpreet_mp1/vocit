@extends ('backend.layouts.app')

@section ('title', 'Vocs')

@section('page-header')
<h1>
    Vocs
</h1>
@endsection
@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">All Vocs</h3>
        <div class="box-tools pull-right">
            <a href="{{ route('admin.createVoc') }}" class="btn btn-primary">Create Voc</a>
        </div>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="marketing-table" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th class='title-th'>Title</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                </tbody>
                <?php $key = 0; ?>
                @forelse($all_vocs as $voc)
                <?php $key = $key + 1; ?>
                <tr>
                    <td>{{$key}}</td>
                    <td>{{ $voc->title }}</td>
                    <td>
                        <a href="{{route('admin.editVoc',$voc->id)}}"  class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ trans('buttons.general.crud.edit') }}"></i></a>
                        <!--<a class="btn btn-xs btn-danger delete_marketing" data-marketing_title='{{ $voc->title }}' data-marketing_id="{{$voc->id}}"><i class="fa fa-trash"></i></a>-->
                    </td>
                </tr>

                @empty
                <tr>
                    <td colspan="6">No vocs present.</td>
                </tr>
                @endforelse
                <tbody>
            </table>
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->

<div class="modal fade" id="deleteMarketing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Voc delete</h4>
            </div>
            {!! Form::open(['id' => 'marketing_delete', 'method' => 'post', 'route' => 'admin.deleteVoc' ]) !!}
            <div class="modal-body">
                <p>Do you want to delete "<span id='market-name'></span>" market?</p>
                {!! Form::hidden('id','',['id'=>'hidden_marketing_id']) !!}
            </div>
            <div class="modal-footer">
                {!! Form::reset('Cancel',['class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                {!! Form::submit(trans('labels.general.yes'),['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
@section('after-scripts-end')
<script>
    $(function () {
        $('.delete_marketing').on('click', function (e) {
            var $this = $(this);
            $('#hidden_marketing_id').val($this.attr('data-marketing_id'));
            $('#market-name').text($this.attr('data-marketing_title'));
            $('#deleteMarketing').modal(show);
            e.preventDefault();
        });
    });
</script>
@stop