@extends ('backend.layouts.app')

@section ('title', 'Voc Categories')

@section('page-header')
<h1>
    Voc Categories
</h1>
@endsection

@section('content')
{{ Form::open(['route' => ['admin.category.update'], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST']) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">All Voc Categories</h3>
        <div class="pull-right">
            {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-header -->

    <div class="box-body">

        <div class="table-responsive">
            <table id="title-table" class="table table-condensed table-hover dataTable no-footer">
                <thead>
                    <tr>
                        <th class="sno">S No.</th>
                        <th class="title">Name</th>
                        <th class="action">Action</th>
                    </tr>
                </thead>
                <tbody class="count-tr">
                    <?php if ($categories->isEmpty()) { ?>
                        <tr class="sub-cat-present-remove">
                            <td colspan="3" >
                                No category added.
                            </td>
                        </tr>
                    <?php } ?>
                    @if(!empty($categories))
                    @foreach ($categories as $key => $value)
                    <tr class="multiple-tr">
                        {{ Form::hidden('categories['.$key.'][id]',  $value->id ) }}
                        {{ Form::hidden('categories['.$key.'][is_deleted]',  0, ['class'=>'is_deleted']) }}
                        <td>{{ $key + 1 }}</td> 
                        <td class="input-width">
                            {{ Form::text('categories['.$key.'][name]',  $value->name , ['class' => 'form-control dashboard-input']) }}
                        </td>
                        <td class="button-add-cat link-align-center">
                            <a href="{{ route('admin.category.delete',$value->id) }}" type="button" class="btn btn-xs btn-danger btn-danger-margin deleteProject delete-swal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>

            <div id="live-stats-head-outer"></div>
            <script id="title-template" type="text/html">
                <tr>
                    <td><%= count %></td> 
                    <td>
                        <input class="form-control" name="categories[<%= count %>][name]" type="text" required>
                    </td>
                    <td class="link-align-center">
                        <a type="button" class="btn btn-xs btn-danger btn-danger-margin deleteProject" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                </script>
                <div id="slide-2-button">
                    <button class="btn btn-xs btn-primary pull-right" id="add-more">Add more</button>
                </div>
            </div><!--table-responsive-->

        </div><!-- /.box-body -->
        <div class="box-body">
            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->


    {{ Form::close() }}
    @endsection
    @section('after-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.1/underscore-min.js"></script>	
    <script>
$('#add-more').on('click', function (e) {
    e.preventDefault();
    var countTr = $('.count-tr tr').length;
    if (countTr < 20) {
        var vocTemplate = $('#title-template').html();
        vocTemplate = _.template(vocTemplate);
        var vocCategoriesData = {
            count: parseInt($('#title-table tbody tr').length) + 1
        };
        $('#title-table tbody').append(vocTemplate(vocCategoriesData));
    } else {
        swal({
            title: "Maximum category limit reached.",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        });
    }
});
$('.deleteProject').on('click', function () {
    var parentSel = $(this).parents('tr');
//    parentSel.hide();
    parentSel.find('.is_deleted').val(1);
    $('#title-table tbody tr').each(function (i, v) {
        $(this).find('td').first().text(i + 1);
    });
});
$(".delete-swal").on("click", function (e) {
    e.preventDefault();
    var linkURL = $(this).attr("href");
    swal({
        title: "Are you sure you want to delete this category?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "{{ trans('buttons.general.crud.delete') }}",
        cancelButtonText: "{{ trans('buttons.general.cancel') }}",
        closeOnConfirm: false
    }, function (isConfirmed) {
        if (isConfirmed) {
            window.location.href = linkURL;
        }
    });
});
    </script>
    @endsection