@extends ('backend.layouts.app')

@section ('title', 'Activated Vocs')

@section('page-header')
<h1>
    Activated Vocs
</h1>
@endsection

@section('after-styles')
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">All Activated Vocs</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <table id="activatedVocsTable" class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Information</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if(!$vocs->isEmpty())
                @foreach($vocs as $data)
                <tr>
                    <td>{{$data->title}}</td>
                    <td><img src="{{  asset('uploads/'.$data->image)}}" height="80" width="275"/></td>
                    <td>{!! $data->information !!}</td>
                    <td><a href="{{ route('admin.voc.deactivate',['userid'=>$data->user_id]) }}" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deactivate"></i></a></td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="4" >
                        No active vocs.
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('after-scripts')
<script>
    $(document).ready(function () {
        $('#activatedVocsTable').DataTable({
//        responsive: true
        });
    });
</script>
@endsection
