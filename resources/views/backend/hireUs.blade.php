@extends ('backend.layouts.app')

@section ('title', 'Hire Us')

@section('page-header')
<h1>
    Hire Us
</h1>
@endsection

@section('after-styles')
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Hire Us</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <table id="activatedVocsTable" class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if(!$hireUsVocs->isEmpty())
                @foreach($hireUsVocs as $data)
                <tr>
                    <td>{{$data->user->first_name. ' '. $data->user->last_name}}</td>
                    <td>{!! $data->user->email !!}</td>
                    <td>
<!--                        <a href="{{-- route('admin.voc.deactivate',['userid'=>$data->user_id]) --}}" class="btn btn-xs btn-warning">
                            <i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deactivate"></i>
                        </a>-->
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="3">
                        No hire us till now.
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('after-scripts')
<script>
    $(document).ready(function () {
        $('#activatedVocsTable').DataTable({});
    });
</script>
@endsection
