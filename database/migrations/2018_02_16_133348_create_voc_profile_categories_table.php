<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVocProfileCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voc_profile_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('voc_id')->unsigned();
            $table->foreign('voc_id')->references('id')->on('vocs')->onDelete('cascade');
            $table->integer('voc_category_id')->unsigned();
            $table->foreign('voc_category_id')->references('id')->on('vocs_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voc_profile_categories');
    }
}
