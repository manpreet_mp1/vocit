<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostSharingOptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_sharing_options',
            function (Blueprint $table) {
            $table->increments('id');
            $table->string('postable_type', 200);
            $table->integer('postable_id');
            $table->integer('sharing_option_id')->unsigned();
            $table->foreign('sharing_option_id')->references('id')->on('sharing_options')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_sharing_options');
    }
}