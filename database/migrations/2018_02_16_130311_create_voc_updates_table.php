<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVocUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voc_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('voc_id')->unsigned();
            $table->foreign('voc_id')->references('id')->on('vocs')->onDelete('cascade');
            $table->string('title',200);
            $table->string('logo',200);
            $table->string('media',200);
            $table->tinyInteger('media_type',4);
            $table->text('body');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voc_updates');
    }
}
