<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVocVoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voc_voices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('voc_id')->unsigned();
            $table->foreign('voc_id')->references('id')->on('vocs')->onDelete('cascade');
            $table->string('name',200);
            $table->string('profile_image',200);
            $table->text('body');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voc_voices');
    }
}
