<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('voc_id')->unsigned();
            $table->foreign('voc_id')->references('id')->on('vocs')->onDelete('cascade');
            $table->string('job_title',255);
            $table->string('job_industry',100);
            $table->string('location',100);
            $table->string('latitude',100);
            $table->string('longitude',100);
            $table->text('pay');
            $table->string('employment_type',200);
            $table->string('benefits_offered',200);
            $table->string('logo',200);
            $table->text('body');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_posts');
    }
}
