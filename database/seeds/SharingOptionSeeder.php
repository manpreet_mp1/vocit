<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SharingOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sharing_options')->insert(array(
             array('name'=>'Add to Collection','type'=>'1','created_at' => Carbon::now()),
             array('name'=>'Send E-mail','type'=>'1','created_at' => Carbon::now()),
             array('name'=>'Add to Blog','type'=>'1','created_at' => Carbon::now()),
             array('name'=>'Facebook','type'=>'2','created_at' => Carbon::now()),
             array('name'=>'Twitter','type'=>'2','created_at' => Carbon::now()),
             array('name'=>'Linkedin','type'=>'2','created_at' => Carbon::now()),
             array('name'=>'Pinterest','type'=>'2','created_at' => Carbon::now()),
             array('name'=>'Instagram','type'=>'2','created_at' => Carbon::now()),
             array('name'=>'Google+','type'=>'2','created_at' => Carbon::now()),

          ));
    }
}
