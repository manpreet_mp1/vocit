<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class VocTemplatesTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('voc_templates')->insert(array(
            array('name' => 'Template 1', 'cover_image' => 'template_Image_1.png','file_name'=>'voc_one_template', 'created_at' => Carbon::now()),
            array('name' => 'Template 2', 'cover_image' => 'template_Image_2.png','file_name'=>'voc_two_template', 'created_at' => Carbon::now()),
           
        ));
    }
}