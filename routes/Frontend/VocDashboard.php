<?php

/* ------Voc Event------ */
Route::group(['prefix' => 'event', 'as' => 'event.', 'middleware' => 'auth'], function () {
    Route::get('create', 'VocEventController@create')->name('create');
    Route::post('store', 'VocEventController@store')->name('store');
    Route::get('show', 'VocEventController@show')->name('show');
    Route::get('edit/{id}', 'VocEventController@edit')->name('edit');
    Route::post('update', 'VocEventController@update')->name('update');
    Route::get('delete/{id}', 'VocEventController@delete')->name('delete');
});

/* ------Voc Promotion------ */
Route::group(['prefix' => 'promotion', 'as' => 'promotion.', 'middleware' => 'auth'], function () {
    Route::get('create', 'VocPromotionController@create')->name('create');
    Route::post('store', 'VocPromotionController@store')->name('store');
    Route::get('show', 'VocPromotionController@show')->name('show');
    Route::get('edit/{id}', 'VocPromotionController@edit')->name('edit');
    Route::post('update', 'VocPromotionController@update')->name('update');
    Route::get('delete/{id}', 'VocPromotionController@delete')->name('delete');
});

Route::group([
    'middleware' => 'access.routeNeedsRole:' . config('constant.roles.voc')], function () {

    /* ------Job Posting------ */
    Route::group(['prefix' => 'jobPosting', 'as' => 'jobPosting.', 'middleware' => 'auth'], function () {
        Route::get('/dashboard', 'JobPostingController@dashboard')->name('vocDashboard');
        Route::get('/create', 'JobPostingController@create')->name('create');
        Route::post('/save', 'JobPostingController@save')->name('save');
        Route::get('/edit/{id}', 'JobPostingController@edit')->name('edit');
        Route::post('/update', 'JobPostingController@update')->name('update');
        Route::get('/delete/{id}', 'JobPostingController@delete')->name('delete');
    });

    /* ------Voc Update------- */
    Route::group(['prefix' => 'update', 'as' => 'update.', 'middleware' => 'auth'], function () {

        Route::get('/dashboard', 'UpdateController@dashboard')->name('vocUpdateDashboard');
        Route::get('/create', 'UpdateController@create')->name('create');
        Route::post('/save', 'UpdateController@save')->name('save');
        Route::get('/edit/{id}', 'UpdateController@edit')->name('edit');
        Route::post('/update', 'UpdateController@update')->name('update');
        Route::get('/delete/{id}', 'UpdateController@delete')->name('delete');
    });

    /* --------Voc Voice------- */
    Route::group(['prefix' => 'voice', 'as' => 'voice.', 'middleware' => 'auth'], function () {
        Route::get('/dashboard', 'VoiceController@dashboard')->name('dashboard');
        Route::get('/create', 'VoiceController@create')->name('create');
        Route::post('/save', 'VoiceController@save')->name('save');
        Route::get('/edit/{id}', 'VoiceController@edit')->name('edit');
        Route::post('/update', 'VoiceController@update')->name('update');
        Route::get('/delete/{id}', 'VoiceController@delete')->name('delete');

        Route::get('/vocEdit', 'VoiceController@vocEdit')->name('vocEdit');
        Route::post('/vocUpdate', 'VoiceController@vocUpdate')->name('vocUpdate');
        
        Route::post('/codeYourOwnSave', 'VoiceController@codeYourOwnSave')->name('codeYourOwnSave');

        Route::get('/template', 'VoiceController@template')->name('template');
        Route::get('/hireUs', 'VocDexController@hireUs')->name('hireUs');
        Route::post('/templateCustomizationAdd', 'VoiceController@templateCustomizationAdd')->name('templateCustomizationAdd');
        
        Route::get('/templateStore/{templateId}', 'VoiceController@templateStore')->name('templateStore');

        Route::get('/followers', 'VoiceController@followers')->name('followers');
    });

    Route::group(['prefix' => 'vocDashboard', 'as' => 'vocDashboard.', 'middleware' => 'auth'], function () {
        Route::get('/dashboard', 'VoiceController@vocDashboard')->name('vocDashboard');
    });
});
