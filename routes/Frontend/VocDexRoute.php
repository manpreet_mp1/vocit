<?php
// VOC Signup
Route::get('signup', 'VocProfileController@index')->name('signup');
Route::post('storeInfo', 'VocProfileController@store')->name('storeInfo');
Route::get('vocSignUpProfile/{id}', 'VocProfileController@vocSignUpProfile')->name('vocSignUpProfile');
//Create Voc
Route::get('create/voc', 'VocProfileController@createVoc')->name('createVoc');
//Create VocDex
Route::get('create/vocdex', 'VocProfileController@createVocdex')->name('createVocdex');
//Store Voc and Vocdex profile
Route::post('storeUser', 'VocProfileController@storeUser')->name('storeUser');
Route::post('storeTemplate', 'VocProfileController@storeTemplate')->name('storeTemplate');
Route::post('storeVocCategory', 'VocProfileController@storeVocCategory')->name('storeVocCategory');
Route::get('getVocCategoryTopic/{id}', 'VocProfileController@getVocCategoryTopic')->name('getVocCategoryTopic');
Route::post('storeVocProfileTemplateId',
    'VocProfileController@storeVocProfileTemplateId')->name('storeVocProfileTemplateId');
Route::get('getSingleVoc/{id?}', 'VocProfileController@getSingleVoc')->name('getSingleVoc');

// VOC Dex
Route::get('VocDex', 'VocDexController@index')->name('VocDex');
Route::post('storeVocDex', 'VocDexController@store')->name('storeVocDex');
Route::post('storeVocDexSubCategory', 'VocDexController@storeVocDexSubCategory')->name('storeVocDexSubCategory');
//Route::get('getVocs', 'VocDexController@getRecommendedVocs')->name('getVocs');
Route::post('storeRecommendedVocs', 'VocDexController@storeRecommendedVocs')->name('storeRecommendedVocs');
Route::get('search', 'VocDexController@searchView')->name('search');
Route::get('searchCategory', 'VocDexController@searchCategory')->name('searchCategory');

Route::get('vocdex/confirm/{token}', 'VocDexConfirmController@confirm')->name('vocdex.confirm');
Route::get('vocprofile/confirm/{token}', 'VocProfileConfirmController@confirm')->name('vocprofile.confirm');

Route::get('getDexCategories', 'VocDexController@getDexCategories')->name('getDexCategories');

//Vocdex user routes
Route::group([
    'middleware' => 'access.routeNeedsRole:'.config('constant.roles.voc_dex'),
    ],
    function () {
    Route::group(['middleware' => 'auth'],
        function () {
        Route::get('vocdex-dashboard', 'VocDexController@vocdexHome')->name('vocdex-home');
        Route::get('vocdex-home/vocs', 'VocDexController@loadVocDex')->name('loadVocDex');
        
         Route::post('/vocFollow', 'VoiceController@vocFollow')->name('vocFollow');
    });
});
