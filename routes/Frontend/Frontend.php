<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'HomeController@home')->name('home');
Route::get('/voc/search', 'HomeController@vocSearch')->name('vocSearch');
Route::get('/voc/scroll', 'HomeController@scrollSearch')->name('scrollSearch');
Route::get('/forget_password', 'HomeController@forgetPassword')->name('forget_password');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/privacy-policy', 'HomeController@privacy')->name('privacy');
Route::get('/terms-of-use', 'HomeController@termsOfUse')->name('termsOfUse');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    //Voc and Vocdex user can open this page

    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        Route::get('createVoc', 'DashboardController@createVoc')->name('createVoc');
        Route::post('storeVoc', 'DashboardController@storeVoc')->name('storeVoc');
        /*
         * blog route
         */
        Route::get('createBlog', 'BlogController@createBlog')->name('createBlog');
        Route::post('storeBlog', 'BlogController@store')->name('storeBlog');
        Route::get('allBlog', 'BlogController@index')->name('allBlog');
        Route::get('viewBlog/{id}', 'BlogController@viewBlog')->name('viewBlog');
        Route::get('editBlog/{id}', 'BlogController@getBlog')->name('editBlog');
        Route::post('updateBlog/{id}', 'BlogController@editBlog')->name('updateBlog');
        Route::get('deleteBlog/{id}', 'BlogController@deleteBlog')->name('deleteBlog');
        Route::post('addComment/{id}', 'CommentController@store')->name('addComment');
        Route::get('createCategory', 'BlogController@createCategory')->name('createCategory');
        Route::post('storeCategory', 'BlogController@storeCategory')->name('storeCategory');
        Route::post('getCategory', 'BlogController@getCategory')->name('getCategory');
        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
    });
});
