<?php

//Deactivate and activate voc user routes
Route::group(['prefix' => 'voc', 'as' => 'voc.', 'middleware' => 'auth'], function ()
{
    Route::get('activatedVocs', 'Voc\VocController@index')->name('activatedVocs');
    Route::get('deactivatedVocs', 'Voc\VocController@getDeactivatedVocs')->name('deactivatedVocs');
    Route::get('deactivateVoc/{userid}', 'Voc\VocController@deactivateVoc')->name('deactivate');
    Route::get('activateVoc/{userid}', 'Voc\VocController@activateVoc')->name('activate');
});

