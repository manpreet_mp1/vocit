<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::get('vocHireUs', 'DashboardController@vocHireUs')->name('vocHireUs');
Route::get('mailForHireUs/{id}', 'DashboardController@mailForHireUs')->name('mailForHireUs');

//Route::get('vocs', 'DashboardController@allVocs')->name('allVocs');
//Route::get('createVoc', 'DashboardController@createVoc')->name('createVoc');
//Route::post('storeVoc', 'DashboardController@storeVoc')->name('storeVoc');
//Route::get('editVoc/{id}', 'DashboardController@editVoc')->name('editVoc');
//Route::post('updateVoc', 'DashboardController@updateVoc')->name('updateVoc');
//Route::get('deleteVoc', 'DashboardController@deleteVoc')->name('deleteVoc');
