<?php

Route::group(['prefix' => 'category', 'as' => 'category.', 'middleware' => 'auth'], function ()
{
Route::get('create', 'Voc\VocCategoryController@create');
Route::post('store', 'Voc\VocCategoryController@store')->name('store');
Route::get('show', 'Voc\VocCategoryController@show')->name('show');
Route::post('update', 'Voc\VocCategoryController@update')->name('update'); 
Route::get('delete/{id}', 'Voc\VocCategoryController@delete')->name('delete');
//Route::get('edit/{id}', 'Voc\VocCategoryController@edit')->name('edit');


//Route::get('getCategories', '\App\Http\Controllers\Frontend\TopicController@getCategories')->name('getCategories');

});