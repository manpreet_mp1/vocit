<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class RouteNeedsRole.
 */
class RouteNeedsRole {

    /**
     * @param $request
     * @param Closure $next
     * @param $role
     * @param bool $needsAll
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $needsAll = false) {
        /*
         * Roles array
         */
        if (strpos($role, ';') !== false) {
            $roles = explode(';', $role);
            $access = access()->hasRoles($roles, ($needsAll === 'true' ? true : false));
        } else {
            /**
             * Single role.
             */
            $access = access()->hasRole($role);
        }
        if (!$access) {
            if (access()->hasRole(config('constant.roles.administrator'))) {
                return redirect()
                                ->route('admin.dashboard');
            } elseif (access()->hasRole(config('constant.roles.voc'))) {
                return redirect()
                                ->route('frontend.voice.dashboard');
            } elseif (access()->hasRole(config('constant.roles.voc_dex'))) {
                return redirect()
                                ->route('frontend.vocdex-home');
            }
        }

        return $next($request);
    }

}
