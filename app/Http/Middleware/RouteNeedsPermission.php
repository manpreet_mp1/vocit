<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class RouteNeedsRole.
 */
class RouteNeedsPermission {

    /**
     * @param $request
     * @param Closure $next
     * @param $permission
     * @param bool $needsAll
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $permission, $needsAll = false) {
        /*
         * Permission array
         */
        if (strpos($permission, ';') !== false) {
            $permissions = explode(';', $permission);
            $access = access()->allowMultiple($permissions, ($needsAll === 'true' ? true : false));
        } else {
            /**
             * Single permission.
             */
            $access = access()->allow($permission);
        }

        if (!$access) {
            if (access()->hasRole(config('constant.roles.administrator'))) {
                return redirect()
                                ->route('admin.dashboard');
            } elseif (access()->hasRole(config('constant.roles.voc'))) {
                return redirect()
                                ->route('frontend.voice.dashboard');
            } elseif (access()->hasRole(config('constant.roles.voc_dex'))) {
                return redirect()
                                ->route('frontend.vocdex-home');
            }
        }

        return $next($request);
    }

}
