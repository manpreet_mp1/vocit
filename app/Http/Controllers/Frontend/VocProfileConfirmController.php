<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Exceptions\GeneralException;
use App\Models\Voc\VocProfile;

class VocProfileConfirmController extends Controller
{

    public function confirm($token)
    {
        $voc = VocProfile::where('confirmation_code', $token)->first();
        if (!is_null($voc)) {
            if ($voc->confirmed == 1) {
                return 'Your account is already activated';
            }
            if ($voc->confirmation_code == $token) {
                $voc->confirmed = 1;
                $voc->save();
                return 'Your account has been successfully activated!';
            }
        }
        throw new GeneralException(trans('exceptions.frontend.auth.confirmation.mismatch'));
    }
}