<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Request as RequestGlobal;

/**
 * Class ForgotPasswordController.
 */
class ForgotPasswordController extends Controller
{

    use SendsPasswordResetEmails;

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('frontend.auth.passwords.email');
    }

    protected function sendResetLinkResponse($response)
    {
        if (RequestGlobal::ajax()) {
            return response()->json(['success' => trans($response)], 200);
        }
        return back()->with('status', trans($response));
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        if ($request->ajax()) {
            return response()->json(['email' => trans($response)], 500);
        }
        return back()->withErrors(['email' => trans($response)]);
    }
}