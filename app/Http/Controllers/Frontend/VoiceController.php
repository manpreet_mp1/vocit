<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VocDashboard\Voice;
use App\Models\Voc\Voc;
use App\Models\Voc\VocTemplate;
use App\Models\Voc\VocProfileFollowers;
use App\Models\Voc\SharingOption;
use App\Models\Voc\PostSharingOption;
use App\Models\Voc\VocCodeOwn;
use App\Models\HireUs;
use App\Http\Requests\Frontend\VocVoice\VocVoiceRequest;
use App\Http\Requests\Frontend\VocEditRequest;
use App\Http\Requests\Frontend\Voc\VocCodeOwnRequest;
use Auth;
use Storage;

class VoiceController extends Controller {

    public function dashboard(Request $request) {
        $voc = Auth::user()->voc;
        if (empty($voc)) {
            return redirect()->back()->withFlashDanger('You are not allowed to access this page.');
        }

        $voices = Voice::where('voc_id', $voc->id)->latest()->get();
        return view('frontend.vocVoice.vocVoiceDashboard', ['vocId' => $voc->id, 'voices' => $voices]);
    }

    public function create() {
        $sharing = SharingOption::all();
        return view('frontend.vocVoice.voc_voice_create', ['sharing' => $sharing]);
    }

    public function save(VocVoiceRequest $request) {
        $voc = Voc::where('user_id', access()->id())->first();
        $voices = new Voice;
        $voices->voc_id = $voc->id;
        $voices->name = $request->name;
        $voices->profile_image = $request->profile_image;
        $voices->body = $request->body;

        if ($voices->save()) {
            foreach ($request->sharingOptions as $sharingOptionId) {

                $postSharingOptionData = new PostSharingOption();
                $postSharingOptionData->sharing_option_id = $sharingOptionId;

                $voices->postSharingOption()->save($postSharingOptionData);
            }

            return redirect()->route('frontend.voice.dashboard')->withFlashSuccess('Your voc voice saved successifully.');
        } else {

            return redirect()->back()->withFlashDanger('Unable to save voc voice. Please try again.');
        }
    }

    public function edit($id) {
        $voices = Voice::with('postSharingOption')->find($id);
        $sharing = SharingOption::all();
        if (empty($voices)) {
            return redirect()->back()->withFlashDanger('You are not allowed to access this page.');
        }
        return view('frontend.vocVoice.voc_voice_edit', ['voices' => $voices, 'sharing' => $sharing]);
    }

    public function update(VocVoiceRequest $request) {
        $data = $request->all();
        $voices = Voice::findOrFail($request->id);
        $voices->name = $data['name'];
        $voices->profile_image = $data['profile_image'];
        $voices->body = $data['body'];
        if ($voices->save()) {

            PostSharingOption::where('postable_id', $data['id'])->delete();

            foreach ($data['sharingOptions'] as $sharingOptionId) {

                $postSharingOptionData = new PostSharingOption();
                $postSharingOptionData->sharing_option_id = $sharingOptionId;
                $voices->postSharingOption()->save($postSharingOptionData);
            }

            return redirect()->route('frontend.voice.dashboard')->withFlashSuccess('Your voc voice updated successifully.');
        }
        return redirect()->back()->withFlashDanger('Unable to save voc voice.');
    }

    public function delete($id) {
        if (Voice::where('id', $id)->delete()) {

            return redirect()->route('frontend.voice.dashboard')->withFlashSuccess('Voc Voice deleted successfully.');
        }
        return redirect()->back()->withFlashDanger('Unable to delete data.');
    }

    public function vocEdit() { 
        $voc = Auth::user()->voc;
        if (empty($voc)) {
            return redirect()->route('frontend.voc.dashboard');
        }
        if ($voc->level_id == 0) {
            return view('frontend.codeBasic', ['voc' => $voc]);
        } else if ($voc->level_id == 1) {
            
        } else if ($voc->level_id == 2) {
            $vocCodeOwn = VocCodeOwn::where('voc_id', $voc->id)->first();
            return view('frontend.codeYourOwn', ['voc' => $voc, 'vocId' => $voc->id, 'vocCodeOwn' => $vocCodeOwn]);
        } else {
            return view('frontend.vocEdit', ['voc' => $voc, 'vocId' => $voc->id]);
        }
    }

    public function vocUpdate(VocEditRequest $request) {
        $voc = Auth::user()->voc;
        $voc->title = $request->input('title');
        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $logoName = image_store($logo);
            $voc->logo = $logoName;
            $voc_logo = url('/') . '/uploads/' . $voc->logo;
        } else {
            $voc_logo = url('/') . '/uploads/' . $voc->logo;
        }
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = image_store($image);
            $voc->image = $imageName;
        }

        $voc->information = $request->input('information');

        if ($voc->save()) {
            return response()->json(['message' => 'Yayy!! Page settings has been saved.', 'logo' => $voc_logo], 200);
        }
        return response()->json(null, 500);
    }

    public function codeYourOwnSave(VocCodeOwnRequest $request) {
        $voc = Auth::user()->voc;
        $vocCodeOwn = VocCodeOwn::where('voc_id', $voc->id)->first();
        if ($vocCodeOwn->count() > 0) {
            $vocCodeOwn->style = $request->input('style');
            $vocCodeOwn->script = $request->input('script');
            $vocCodeOwn->markup = $request->input('markup');
        } else {
            $vocCodeOwn = new VocCodeOwn();
            $vocCodeOwn->voc_id = $voc->id;
            $vocCodeOwn->style = $request->input('style');
            $vocCodeOwn->script = $request->input('script');
            $vocCodeOwn->markup = $request->input('markup');
        }

        if ($vocCodeOwn->save()) {
            return response()->json(['message' => 'Yayy!! Your code has been saved.'], 200);
        }
        return response()->json(null, 500);
    }

    public function template() {
        $templates = VocTemplate::where('is_active', '=', '1')->get();
        $voc = Auth::user()->voc;
        $user = Auth::user();
        if (empty($voc)) {
            return redirect()->back()->withFlashDanger('You are not allowed to access this page.');
        }
        $hireus_value = HireUs::where('user_id', $user->id)->exists();
        return view('frontend.template', ['templates' => $templates, 'currentVocTemplate' => $voc->template_id,
            'voc' => $voc, 'hireus_value' => $hireus_value]);
    }

    public function templateStore($templateId) {
        $voc = Auth::user()->voc;
        $voc->template_id = $templateId;

        if ($voc->save()) {
            return redirect()->route('frontend.voice.template')->withFlashSuccess('Your voc template has been changed successfully.');
        }
        return redirect()->back()->withFlashDanger('Unable to save voc template.');
    }

    public function vocFollow(Request $request) {
        $data = $request->all();
        $comming_subscription = $data['model'];
        $user_id = access()->id();
        if ($data['model']) {

            VocProfileFollowers::where('voc_id', $data['voc_id'])
                    ->where('user_id', $user_id)->forceDelete();

            $voc = Voc::where('id', $data['voc_id'])->first();

            foreach ($comming_subscription as $sub) {
                $follow = new VocProfileFollowers();
                $follow->voc_id = $voc->id;
                $follow->user_id = $user_id;
                $follow->subscription = $sub;
                $follow->save();
            }
            return response()->json(['success' => true], 200);
        } else {
            return response()->json(['error' => 'Select at least one post type.'], 500);
        }
    }

    public function followers() {
        $voc = Voc::where('user_id', access()->id())->first();
        if (empty($voc)) {
            return redirect()->back()->withFlashDanger('You are not allowed to access this page.');
        }
        $vocFollowers = VocProfileFollowers::where('voc_id', $voc->id)->with('user.profile')->get()->unique('user_id');
        if (empty($vocFollowers)) {
            return redirect()->back()->withFlashDanger('You are not allowed to access this page.');
        }
        return view('frontend.followers', ['vocFollowers' => $vocFollowers]);
    }

    public function vocDashboard() {
        return view('frontend.dashboard');
    }

    public function templateCustomizationAdd(Request $request) {
        $user = Auth::user();
        if (Voc::where('user_id', $user->id)->update(['level_id' => $request->input('level')])) {
            return response()->json([], 200);
        } else {
            return response()->json(['message' => 'There has been a error. Please try again.'], 500);
        }
    }

}
