<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\VocProfileRequest;
use App\Models\Voc\VocProfile;
use App\Models\Voc\Voc;
use App\Models\Voc\VocProfileFollowers;
use App\Models\Voc\VocCategory;
use App\Models\Voc\VocTemplate;
use App\Models\Voc\VocProfileCategory;
use App\Models\VocDashboard\Voice;
use App\Models\VocDashboard\Job;
use App\Models\VocDashboard\Update;
use App\Models\voc\VocEvent;
use App\Models\Voc\VocPromotion;
use App\Models\Voc\VocCodeOwn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use App\Models\Access\User\User;
use Session;
use Auth;

class VocProfileController extends Controller {

    public function index() {
        $vocCategories = VocCategory::where('parent_id', null)->with('vocs')->get();
        $categories = $vocCategories;
        $vocCategoriesSignUpList = VocCategory::where('parent_id', null)->get();

        $template = VocTemplate::where('is_active', '=', '1')->get();

        //Recommended vocs --NEED to chnage it to original
        $recommended_vocs = Voc::all();

        return view('frontend.signup', ['categories' => $categories, 'vocCategories' => $vocCategories, 'template' => $template,
            'vocCategoriesSignUpList' => $vocCategoriesSignUpList,
            'recommended_vocs' => $recommended_vocs]);
    }

    public function createVoc() {
        $vocCategories = VocCategory::where('parent_id', null)->with('vocs')->get();
        $categories = $vocCategories;
        $vocCategoriesSignUpList = VocCategory::where('parent_id', null)->get();

        $template = VocTemplate::take(2)->get();

        //Recommended vocs --NEED to chnage it to original
        $recommended_vocs = Voc::all();

        return view('frontend.includes.signup', ['categories' => $categories, 'vocCategories' => $vocCategories, 'template' => $template,
            'vocCategoriesSignUpList' => $vocCategoriesSignUpList,
            'recommended_vocs' => $recommended_vocs]);
    }

    public function createVocdex() {
        $vocCategories = VocCategory::where('parent_id', null)->with('vocs')->get();
        $categories = $vocCategories;
        $vocCategoriesSignUpList = VocCategory::where('parent_id', null)->get();

        $template = VocTemplate::take(2)->get();

        //Recommended vocs --NEED to chnage it to original
        $recommended_vocs = Voc::all();

        return view('frontend.signup', ['categories' => $categories, 'vocCategories' => $vocCategories, 'template' => $template,
            'vocCategoriesSignUpList' => $vocCategoriesSignUpList,
            'recommended_vocs' => $recommended_vocs]);
    }

    public function store(VocProfileRequest $request) {
        Session::put('voc_signup', ['name' => $request->name, 'zip_code' => $request->zip_code]);
        return response()->json(['success' => true], 200);
    }

    public function vocSignUpProfile($id) {
        return view('frontend.voc.vocSignupProfile')->with('id', $id);
    }

    //Store Voc and Vocdex profile
    public function storeUser(Request $request) {
        $vocSignup = session()->get('voc_signup');
        if (empty($vocSignup) && ($request->user_role == config('constant.roles_inverse.voc'))) {
            return response()->json(['status' => 0], 200);
        }
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email|unique:users',
                    'password' => 'required|confirmed|min:6',
                ])->validate();
        if (!empty($validator) && $validator->fails()) {
            return response()->json(['error' => $validator->errors()], 500);
        }
        $user = new User();
        $user->first_name = '';
        $user->last_name = '';
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        if ($request->user_role) {
            if ($request->user_role == config('constant.roles_inverse.voc')) {
                $vocSignupFirstFormData = Session::get('voc_signup');
                $user->first_name = $vocSignupFirstFormData['name'];
            }
        }
        $user->confirmed = 0;
        $user->confirmation_code = md5(uniqid(mt_rand(), true));
        $user->save();
        if ($request->user_role) {
            $user->attachRole($request->user_role);
        }
        $user->notify(new UserNeedsConfirmation($user->confirmation_code));
        $vocSignupFirstFormData = Session::get('voc_signup');
        $voc_profile = new VocProfile();
        $voc_profile->user_id = $user->id;
        if ($request->user_role) {
            if ($request->user_role == config('constant.roles_inverse.voc')) {
                $vocSignupFirstFormData = Session::get('voc_signup');
                $voc_profile->zip_code = $vocSignupFirstFormData['zip_code'];
            } else {
                $voc_profile->zip_code = $request->zip_code;
            }
        }
        if ($request->phone_number) {
            $voc_profile->phone_number = $request->phone_number;
        }
        $voc_profile->save();

        //Creating a voc for getting Voc_id
        if ($request->user_role == config('constant.roles_inverse.voc')) {
            $voc = new Voc();
            $voc->user_id = $user->id;
            $voc->template_id = 1;
            $voc->save();
            session()->put('voc_id', $voc->id);
        } else {
            session()->put('user_id', $user->id);
        }

        if ($request->user_role == config('constant.roles_inverse.voc')) {
            return response()->json(['success' => true], 200);
        } else {
            return response()->json(['success' => true], 200);
        }
    }

    public function storeVocCategory(Request $request) {
        $voc_id = session()->get('voc_id');
        if (!empty($voc_id)) {
            if (!empty($request->input('voc_categories'))) {
                foreach ($request->input('voc_categories') as $category) {
//                    if (isset($category['check']) && $category['check'] == 'on' && isset($category['sub_category'])) {
                    $sub_category = new VocProfileCategory();
                    $sub_category->voc_id = $voc_id;
                    $sub_category->voc_category_id = $category;
                    $sub_category->save();
//                    }
                }
                return response()->json(['success' => true, 200]);
            } else {
                return response()->json(['error' => 'Something went wrong. Please try again.'], 200);
            }
        } else {
            return response()->json(['status' => 0], 200);
        }
    }

    public function storeVocProfileTemplateId(Request $request) {
        $voc_id = session()->get('voc_id');
        if (!empty($voc_id)) {
            $voc = Voc::where('id', $voc_id)->first();
            $voc->template_id = $request->template_id;
            $voc->save();

            return response()->json(['success' => true], 200);
        } else {
            return response()->json(['status' => 0], 200);
        }
    }

    public function storeTemplate(Request $request) {
        $voc_id = session()->get('voc_id');
        if (!empty($voc_id)) {
            $template = Voc::findOrFail($voc_id);
            $template->title = $request->title;

            if ($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $logoName = image_store($logo);
                $template->logo = $logoName;
            }
            if ($request->hasFile('cover_image')) {
                $image = $request->file('cover_image');
                $imageName = image_store($image);
                $template->image = $imageName;
            }

            $template->information = $request->information;
            if ($template->update()) {
                //Login user
                $voc = Voc::where('id', $voc_id)->first();
                $user = User::find($voc->user_id);
                Auth::login($user);

                session()->forget('voc_id');
                return response(['success' => true, 'message' => 'Yayy!! Your Voc profile has been created successfully.',
                    'route' => route('frontend.vocDashboard.vocDashboard')], 200);
            }
        } else {
            return response()->json(['status' => 0], 200);
        }
    }

    public function getSingleVoc(Request $request, $id = null) {
        if ($id) {
            $voc = Voc::where('id', $id)->first();
        } else {
            if (access()->hasRole(config('constant.roles.voc'))) {
                $voc = Auth::user()->voc;
            } else {
                return redirect()->route('frontend.home');
            }
        }
        if ($voc == null) {
            return redirect()->route('frontend.home');
        }

        $followersData = VocProfileFollowers::select('id', 'subscription')->where('user_id', access()->id())
                ->where('voc_id', $voc->id)
                ->pluck('subscription');
        $voc_template = VocTemplate::where('id', $voc->template_id)->first();

        $vocPosts = collect();
        $vocPosts = $vocPosts->merge(VocEvent::where('voc_id', $voc->id)->get());
        $vocPosts = $vocPosts->merge(VocPromotion::where('voc_id', $voc->id)->get());
        $vocPosts = $vocPosts->merge(Voice::where('voc_id', $voc->id)->get());
        $vocPosts = $vocPosts->merge(Job::where('voc_id', $voc->id)->get());
        $vocPosts = $vocPosts->merge(Update::where('voc_id', $voc->id)->get());

        if ($voc->level_id === NULL || $voc->level_id == 3) {
            //just for now
            return view('frontend.custom_template.' . $voc_template->file_name)->with([
                        'voc' => $voc,
                        'vocPosts' => $vocPosts,
                        'vocTemplate' => $voc_template,
                        'followersData' => $followersData]);
        } else if ($voc->level_id == 0) {
           return view('frontend.custom_template.' . $voc_template->file_name)->with([
                        'voc' => $voc,
                        'vocPosts' => $vocPosts,
                        'vocTemplate' => $voc_template,
                        'followersData' => $followersData]);
        } else if ($voc->level_id == 1) {
            
        } else if ($voc->level_id == 2) {
            $vocCodeOwn = VocCodeOwn::where('voc_id', $voc->id)->first();
            return view('frontend.custom_template.code_your_own_template', ['voc' => $voc, 'vocCodeOwn' => $vocCodeOwn]);
        }
    }

    public function getVocCategoryTopic($id) {
//        if ($id) {
//            $voc          = Voc::where('id', $id)->first();
////            dd(VocTemplate::where('id', $voc->template_id)->first());
//            $voc_template = VocTemplate::where('id', $voc->template_id)->first();
//            return view('frontend.custom_template.'.$voc_template->file_name)->with(['voc' => $voc]);
//        } else {
//            return redirect()->route('frontend.home')->withFlashError('Oops!! Invalid Voc.');
//        }
    }

}
