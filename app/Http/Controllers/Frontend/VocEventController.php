<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\VocEventRequest;
use App\Models\Voc\VocEvent;
use App\Models\Voc\Voc;
use App\Models\Voc\SharingOption;
use App\Models\Voc\PostSharingOption;
use Carbon\Carbon;
use Auth;

class VocEventController extends Controller
{

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function create()
    {
        $sharing = SharingOption::all();
        return view('frontend.voc_event.createEvent', ['sharing' => $sharing]);
    }

    public function store(VocEventRequest $request)
    {
        $voc             = Voc::where('user_id', access()->id())->first();
        $data            = $request->all();
        $comingStartDate = $data['start'];
        $comingEndDate   = $data['end'];
        $newStartDate    = date("Y-m-d h:i:s", strtotime($comingStartDate));
        $newEndDate      = date("Y-m-d h:i:s", strtotime($comingEndDate));

        $event                = new VocEvent;
        $event->voc_id        = $voc->id;
        $event->name          = $data['name'];
        $event->body          = $data['body'];
        $event->start         = $newStartDate;
        $event->end           = $newEndDate;
        $event->logo          = $data['logo'];
        $event->street_number = $data['street_number'];
        $event->route         = $data['route'];
        $event->locality      = $data['locality'];
        $event->neighborhood  = $data['neighborhood'];
        $event->district      = $data['district'];
        $event->state         = $data['state'];
        $event->country       = $data['country'];
        $event->postal_code   = $data['postal_code'];
        $event->location      = $data['location'];
        $event->latitude      = $data['latitude'];
        $event->longitude     = $data['longitude'];
        $event->created_at    = Carbon::now();


        if ($event->save()) {
            if (!empty($data['sharingOptions'])) {
                foreach ($data['sharingOptions'] as $sharingOptionId) {
                    $postSharingOptionData                    = new PostSharingOption();
                    $postSharingOptionData->sharing_option_id = $sharingOptionId;

                    $event->postSharingOption()->save($postSharingOptionData);
                }
            }

            return redirect()->route('frontend.event.show')->withFlashSuccess('Voc Event saved successfully.');
        }

        return redirect()->back()->withFlashDanger('Unable to save data.');
    }

    public function show()
    {
        $voc = Auth::user()->voc;
        if (!empty($voc)) {
            $event = VocEvent::where('voc_id', $voc->id)->latest()->get();
            return view('frontend.voc_event.showEvents', ['event' => $event]);
        }
        return view('frontend.voc_event.showEvents', ['event' => new VocEvent]);
    }

    public function edit($id)
    {
        $event   = VocEvent::with('postSharingOption')->find($id);
        $sharing = SharingOption::all();
        return view('frontend.voc_event.updateEvent',
            compact('event', 'sharing'));
    }

    public function update(VocEventRequest $request)
    {
        $data                 = $request->all();
        $event                = VocEvent::findOrFail($request->id);
        $comingStartDate      = $data['start'];
        $comingEndDate        = $data['end'];
        $newStartDate         = date("Y-m-d h:i:s", strtotime($comingStartDate));
        $newEndDate           = date("Y-m-d h:i:s", strtotime($comingEndDate));
        $event->name          = $data['name'];
        $event->body          = $data['body'];
        $event->start         = $newStartDate;
        $event->end           = $newEndDate;
        $event->logo          = $data['logo'];
        $event->street_number = $data['street_number'];
        $event->route         = $data['route'];
        $event->locality      = $data['locality'];
        $event->neighborhood  = $data['neighborhood'];
        $event->district      = $data['district'];
        $event->state         = $data['state'];
        $event->country       = $data['country'];
        $event->postal_code   = $data['postal_code'];
        $event->location      = $data['location'];
        $event->latitude      = $data['latitude'];
        $event->longitude     = $data['longitude'];
        $event->created_at    = Carbon::now();


        if ($event->save()) {

            PostSharingOption::where('postable_id', $data['id'])->delete();
            if (!empty($data['sharingOptions'])) {
                foreach ($data['sharingOptions'] as $sharingOptionId) {

                    $postSharingOptionData                    = new PostSharingOption();
                    $postSharingOptionData->sharing_option_id = $sharingOptionId;
                    $event->postSharingOption()->save($postSharingOptionData);
                }
            }

            return redirect()->route('frontend.event.show')->withFlashSuccess('Voc Event updated successfully.');
        }

        return redirect()->back()->withFlashDanger('Unable to save data.');
    }

    public function delete($id)
    {
        VocEvent::where('id', $id)->delete();

        return redirect()->route('frontend.event.show')
                ->withFlashSuccess('Voc Event deleted successfully.');
    }
}