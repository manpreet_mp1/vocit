<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\VocDashboard\Update;
use App\Models\Voc\Voc;
use App\Models\Voc\SharingOption;
use App\Models\Voc\PostSharingOption;
use App\Http\Requests\Frontend\VocUpdate\VocUpdateRequest;
use Auth;

class UpdateController extends Controller {

    public function dashboard() {
        $voc = Auth::user()->voc;
        if (!empty($voc)) {
        $updates = Update::where('voc_id', $voc->id)->latest()->get();
        return view('frontend.vocUpdate.vocUpdateDashboard', ['updates' => $updates]);
        }
        return view('frontend.vocUpdate.vocUpdateDashboard', ['updates' => new Update]);
    }

    public function create() {
        $sharing = SharingOption::all();
        return view('frontend.vocUpdate.voc_update_create', ['sharing' => $sharing]);
    }

    public function save(VocUpdateRequest $request) {
        $voc = Voc::where('user_id', access()->id())->first();
        if ($voc == null){
            return redirect()->back()->withFlashDanger('voc do not exist');
        }
        $data = $request->all();
        $updates = new Update;
        $updates->voc_id = $voc->id;
        $updates->title = $data['title'];
        $updates->logo = $data['logo'];
        $updates->media = $data['media'];
        $updates->body = $data['body'];
        $updates->media_type = $data['media_type'];

        if ($data['media_type'] == '2') {
            $url = $updates->media;
            parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
            if (!empty($my_array_of_vars)) {
                $key = $my_array_of_vars['v'];
                $youtube_url = 'https://www.youtube.com/embed/'. $key;
                $updates->media = $youtube_url;
            }
        }

        if ($updates->save()) {

            foreach ($data['sharingOptions'] as $sharingOptionId) {

                $postSharingOptionData = new PostSharingOption();
                $postSharingOptionData->sharing_option_id = $sharingOptionId;

                $updates->postSharingOption()->save($postSharingOptionData);
            }

            return redirect()->route('frontend.update.vocUpdateDashboard')->withFlashSuccess('Voc Update saved successfully');
        } 
            return redirect()->back()->withFlashDanger('Unable to save data.');
    }

    public function edit($id) {
        $updates = Update::with('postSharingOption')->find($id);
        $sharing = SharingOption::all();
        return view('frontend.vocUpdate.voc_update_edit', ['updates' => $updates, 'sharing' => $sharing]);
    }

    public function update(VocUpdateRequest $request) {
        $data = $request->all();
        
        $updates = Update::findOrFail($request->id);
        $updates->title = $data['title'];
        $updates->logo = $data['logo'];
        $updates->media = $data['media'];
        $updates->body = $data['body'];
        $updates->media_type = $data['media_type'];
        if ($data['media_type'] == '2') {
            $url = $updates->media;
            parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
            if (!empty($my_array_of_vars)) {
                $key = $my_array_of_vars['v'];
                $youtube_url = 'https://www.youtube.com/embed/'. $key;
                $updates->media = $youtube_url;
            }
        }

        if ($updates->save()) {

            PostSharingOption::where('postable_id', $data['id'])->delete();

            foreach ($data['sharingOptions'] as $sharingOptionId) {

                $postSharingOptionData = new PostSharingOption();
                $postSharingOptionData->sharing_option_id = $sharingOptionId;
                $updates->postSharingOption()->save($postSharingOptionData);
            }

            return redirect()->route('frontend.update.vocUpdateDashboard')->withFlashSuccess('Voc Update updated successfully');
        } else {

            return redirect()->back()->withFlashDanger('Unable to update data.');
        }
    }

    public function delete($id) {
        Update::where('id', $id)
                ->delete();
        return redirect()->route('frontend.update.vocUpdateDashboard')
                        ->withFlashSuccess('Voc Update deleted successfully.');
    }

}
