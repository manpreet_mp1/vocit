<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Blog\Blog;
use App\Models\Blog\Comment;
use App\Models\Blog\Category;
use App\Http\Requests\Frontend\BlogRequest;
use App\Http\Requests\Frontend\CategoryRequest;

class BlogController extends Controller
{

    public function index()
    {
        $all_blogs = Blog::where('user_id', access()->id())->orderBy('id',
                'DESC')->get();
        return view('frontend.user.all_blogs', compact('all_blogs'));
    }

    public function store(BlogRequest $request)
    {
        if ($request->hasFile('image')) {
            $destinationPath = public_path().'/img/';
            $file            = $request->file('image');
            $filename        = $file->getClientOriginalName();
            $request->file('image')->move($destinationPath, $filename);
        }
        $data              = new Blog();
        $data->user_id     = access()->id();
        $data->title       = $request->title;
        $data->category_id = $request->category;
        $data->description = $request->description;
        if (!empty($filename)) {
            $data->image = $filename;
        }
        if ($data->save()) {
            $comment          = new Comment();
            $comment->blog_id = $data->id;
            $comment->comment = $request->comment;
            $comment->save();
        }
        return redirect()->route('frontend.user.createBlog')->with('message',
                'Blog created successfully please list blogs to see the blog');
    }

    public function createBlog()
    {
        $category = Category::where('user_id', access()->id())->get();
        $temp;
        foreach ($category as $cat) {
            $temp[$cat['id']] = $cat['name'];
        }
        return view('frontend.user.create_blog', compact('temp'));
    }

    public function viewBlog($id)
    {
        $data = Blog::find($id);
        return view('frontend.user.view_blog', compact('data'));
    }

    public function getBlog($id)
    {
        $data     = Blog::find($id);
        $category = Category::where('user_id', access()->id())->get();
        $temp;
        foreach ($category as $cat) {
            $temp[$cat['id']] = $cat['name'];
        }
        return view('frontend.user.edit_blog', compact('data', 'temp'));
    }

    public function editBlog(BlogRequest $request, $id)
    {
        if ($request->hasFile('image')) {
            $destinationPath = public_path().'/img/';
            $file            = $request->file('image');
            $filename        = $file->getClientOriginalName();
            $request->file('image')->move($destinationPath, $filename);
        }
        $data              = Blog::find($id);
        $data->user_id     = access()->id();
        $data->title       = $request->title;
        $data->category_id = $request->category;
        $data->description = $request->description;
        if (!empty($filename)) {
            $data->image = $filename;
        }
        $data->save();
        return redirect()->route('frontend.user.allBlog')->with('message',
                'Blog edited successfully');
    }

    public function deleteBlog($id)
    {

        $comment = Comment::where('blog_id', $id)->get();
        foreach ($comment as $c) {
            $comment_id = $c['id'];
        }
        if (!empty($comment_id)) {

            $comment_data = Comment::find($comment_id);
            $comment_data->delete();
            $data         = Blog::find($id);
            $data->delete();
        } else {
            $data = Blog::find($id);
            $data->delete();
        }
        return redirect()->route('frontend.user.allBlog')->with('message',
                'Blog deleted successfully');
    }

    public function createCategory()
    {
        return view('frontend.user.create_category');
    }

    public function storeCategory(CategoryRequest $request)
    {
        $category          = new Category();
        $category->user_id = access()->id();
        $category->name    = $request->name;
        $category->save();
        return redirect()->route('frontend.user.createCategory')->with('message',
                'Category created successfully');
    }
}