<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Voc\Voc;
use App\Http\Requests\Frontend\VocRequest;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $all_vocs = Voc::all();
        return view('frontend.user.dashboard', compact('all_vocs'));
    }

   
    public function createVoc() {
        return view('frontend.user.create_voc');
    }
    public function storeVoc(VocRequest $request) {
        Voc::create($request->all());
        return redirect()->route('frontend.user.dashboard');
    }

}
