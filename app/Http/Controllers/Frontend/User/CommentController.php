<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Blog\Comment;
use App\Models\Blog\Blog;
use App\Http\Requests\Frontend\CommentRequest;

class CommentController extends Controller
{

    public function store(CommentRequest $request, $id)
    {

        $data          = new Comment();
        $data->blog_id = $id;
        $data->comment = $request->comment;
        if ($data->save()) {
            $all_blogs = Blog::where('user_id', access()->id())->orderBy('id',
                    'DESC')->get();
            return view('frontend.user.all_blogs', compact('all_blogs'));
        }
    }
}