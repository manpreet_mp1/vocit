<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\VocPromotionRequest;
use App\Models\Voc\VocPromotion;
use App\Models\Voc\Voc;
use App\Models\Voc\SharingOption;
use App\Models\Voc\PostSharingOption;
use Auth;

class VocPromotionController extends Controller
{

    public function create()
    {
        $sharing = SharingOption::all();
        return view('frontend.voc_promotion.createPromotion',
            ['sharing' => $sharing]);
    }

    public function store(VocPromotionRequest $request)
    {
        $voc                    = Voc::where('user_id', access()->id())->first();
        $data                   = $request->all();
        $promotion              = new VocPromotion;
        $promotion->voc_id      = $voc->id;
        $promotion->header      = $data['header'];
        $promotion->sub_header  = $data['sub_header'];
        $promotion->body        = $data['body'];
        $promotion->cover_image = $data['cover_image'];

        if ($promotion->save()) {

            foreach ($data['sharingOptions'] as $sharingOptionId) {

                $postSharingOptionData                    = new PostSharingOption();
                $postSharingOptionData->sharing_option_id = $sharingOptionId;

                $promotion->postSharingOption()->save($postSharingOptionData);
            }

            return redirect()->route('frontend.promotion.show')->withFlashSuccess('Voc Promotion saved successfully.');
        }

        return redirect()->back()->withFlashDanger('Unable to save Voc Promotion.');
    }

    public function show()
    {
        $voc = Auth::user()->voc;
        if (!empty($voc)) {
            $promotion = VocPromotion::where('voc_id', $voc->id)->latest()->get();
            return view('frontend.voc_promotion.showPromotions',
                compact('promotion'));
        }
        return view('frontend.voc_promotion.showPromotions', ['promotion' => new VocPromotion]);
    }

    public function edit($id)
    {
        $promotion = VocPromotion::with('postSharingOption')->find($id);
        $sharing   = SharingOption::all();
        return view('frontend.voc_promotion.updatePromotion',
            compact('promotion', 'sharing'));
    }

    public function update(VocPromotionRequest $request)
    {
        $promotion              = VocPromotion::findOrFail($request->id);
        $data                   = $request->all();
        $promotion->header      = $data['header'];
        $promotion->sub_header  = $data['sub_header'];
        $promotion->body        = $data['body'];
        $promotion->cover_image = $data['cover_image'];

        if ($promotion->save()) {

            PostSharingOption::where('postable_id', $data['id'])->delete();

            foreach ($data['sharingOptions'] as $sharingOptionId) {

                $postSharingOptionData                    = new PostSharingOption();
                $postSharingOptionData->sharing_option_id = $sharingOptionId;
                $promotion->postSharingOption()->save($postSharingOptionData);
            }

            return redirect()->route('frontend.promotion.show')->withFlashSuccess('Voc Promotion updated successfully.');
        }

        return redirect()->back()->withFlashDanger('Unable to save Voc Promotion.');
    }

    public function delete($id)
    {
        VocPromotion::where('id', $id)->delete();

        return redirect()->route('frontend.promotion.show')
                ->withFlashSuccess('Voc Promotion deleted successfully.');
    }
}