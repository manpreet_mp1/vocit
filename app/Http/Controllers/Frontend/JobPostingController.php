<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\VocDashboard\Job;
use App\Models\Voc\Voc;
use App\Models\Voc\SharingOption;
use App\Models\Voc\PostSharingOption;
use App\Http\Requests\Frontend\Voc\JobPosting;
use Auth;

class JobPostingController extends Controller
{

    public function dashboard()
    {
        $voc = Auth::user()->voc;
        if (!empty($voc)) {
            $jobs = Job::where('voc_id', $voc->id)->latest()->get();
            return view('frontend.job_posting.vocPostingDashboard',
                ['jobs' => $jobs]);
        }
        return view('frontend.job_posting.vocPostingDashboard',
            ['jobs' => new Job]);
    }

    public function create()
    {
        $sharing = SharingOption::all();
        return view('frontend.job_posting.job_posting_create',
            ['sharing' => $sharing]);
    }

    public function save(JobPosting $request)
    {
        $voc                   = Voc::where('user_id', access()->id())->first();
        $data                  = $request->all();
        $jobs                  = new Job;
        $jobs->voc_id          = $voc->id;
        $jobs->job_title       = $data['job_title'];
        $jobs->job_industry    = $data['job_industry'];
        $jobs->street_number   = $data['street_number'];
        $jobs->route           = $data['route'];
        $jobs->locality        = $data['locality'];
        $jobs->neighborhood    = $data['neighborhood'];
        $jobs->district        = $data['district'];
        $jobs->state           = $data['state'];
        $jobs->country         = $data['country'];
        $jobs->postal_code     = $data['postal_code'];
        $jobs->location        = $data['location'];
        $jobs->latitude        = $data['latitude'];
        $jobs->longitude       = $data['longitude'];
        $jobs->pay             = $data['pay'];
        $jobs->employment_type = $data['employment_type'];

        if ($data['benefits_offered'] == config('constant.benefits_offered.true')) {
            $jobs['benefits_offered'] = config('constant.benefits_offered.true');
        } else {
            $jobs['benefits_offered'] = config('constant.benefits_offered.false');
        }

        $jobs->logo = $data['logo'];
        $jobs->body = $data['body'];
        if ($jobs->save()) {
            if (!empty($data['sharingOptions'])) {
                foreach ($data['sharingOptions'] as $sharingOptionId) {
                    $postSharingOptionData                    = new PostSharingOption();
                    $postSharingOptionData->sharing_option_id = $sharingOptionId;

                    $jobs->postSharingOption()->save($postSharingOptionData);
                }
            }

            return redirect()->route('frontend.jobPosting.vocDashboard')->withFlashSuccess('Job Post saved successfully.');
        } else {

            return redirect()->back()->withFlashDanger('Unable to save job post.');
        }
    }

    public function edit(int $id)
    {
        $jobs    = Job::with('postSharingOption')->find($id);
        $sharing = SharingOption::all();
        return view('frontend.job_posting.job_posting_edit',
            ['jobs' => $jobs, 'sharing' => $sharing]);
    }

    public function update(JobPosting $request)
    {
        $data                  = $request->all();
        $jobs                  = Job::findOrFail($request->id);
        $jobs->job_title       = $data['job_title'];
        $jobs->job_industry    = $data['job_industry'];
        $jobs->street_number   = $data['street_number'];
        $jobs->route           = $data['route'];
        $jobs->locality        = $data['locality'];
        $jobs->neighborhood    = $data['neighborhood'];
        $jobs->district        = $data['district'];
        $jobs->state           = $data['state'];
        $jobs->country         = $data['country'];
        $jobs->postal_code     = $data['postal_code'];
        $jobs->location        = $data['location'];
        $jobs->latitude        = $data['latitude'];
        $jobs->longitude       = $data['longitude'];
        $jobs->pay             = $data['pay'];
        $jobs->employment_type = $data['employment_type'];

        if ($data['benefits_offered'] == config('constant.benefits_offered.true')) {
            $jobs->benefits_offered = config('constant.benefits_offered.true');
        } else {
            $jobs->benefits_offered = config('constant.benefits_offered.false');
        }
        $jobs->logo = $data['logo'];
        $jobs->body = $data['body'];
        if ($jobs->save()) {

            PostSharingOption::where('postable_id', $data['id'])->delete();

            if (!empty($data['sharingOptions'])) {
                foreach ($data['sharingOptions'] as $sharingOptionId) {
                    $postSharingOptionData                    = new PostSharingOption();
                    $postSharingOptionData->sharing_option_id = $sharingOptionId;
                    $jobs->postSharingOption()->save($postSharingOptionData);
                }
            }

            return redirect()->route('frontend.jobPosting.vocDashboard')->withFlashSuccess('Job Post updated successfully.');
        } else {

            return redirect()->back()->withFlashDanger('Unable to save job post.');
        }
    }

    public function delete(int $id)
    {
        Job::where('id', $id)
            ->delete();
        return redirect()->route('frontend.jobPosting.vocDashboard')
                ->withFlashSuccess('Job Post deleted successfully.');
    }
}