<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class NewController extends Controller {

    public function tem1() {
        return view('frontend.custom_template.voc_one_template');
    }

    public function tem2() {
        return view('frontend.custom_template.voc_two_template');
    }

}
