<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Voc\VocCategory;
use App\Http\Controllers\Controller;
use App\Models\Voc\VocTemplate;
use App\Models\Voc\Voc;
use Auth;
use Illuminate\Http\Request;
use App\Models\voc\VocEvent;
use App\Models\Voc\VocPromotion;
use App\Models\VocDashboard\Update;
use App\Models\VocDashboard\Job;
use App\Models\VocDashboard\Voice;
use App\Models\Voc\SharingOption;
use App\Models\Voc\VocProfileFollowers;

class HomeController extends Controller {

    public function home(Request $request) {
        if (Auth::check()) {
            if (access()->hasRole(config('constant.roles.administrator'))) {
                return redirect()
                                ->route('admin.dashboard');
            }
        }

        $page = 1;
        $limit = 5;
        $voc_post_limit = 30;
        $skip = ($page * $limit) - $limit;

        $vocCategories = VocCategory::where('parent_id', null)->with('vocs')->get();

        $originalVocs = Voc::with(['events' => (function ($query) {
                        $query->with(['postSharingOption' =>
                            function($query1) {
                                $query1->whereHas('sharingOptions')->with('sharingOptions');
                            }]);
                    }), 'jobs' => (function ($query) {
                        $query->with(['postSharingOption' =>
                            function($query1) {
                                $query1->whereHas('sharingOptions')->with('sharingOptions');
                            }]);
                    }), 'updates' => (function ($query) {
                        $query->with(['postSharingOption' =>
                            function($query1) {
                                $query1->whereHas('sharingOptions')->with('sharingOptions');
                            }]);
                    }), 'promotions' => (function ($query) {
                        $query->with(['postSharingOption' =>
                            function($query1) {
                                $query1->whereHas('sharingOptions')->with('sharingOptions');
                            }]);
                    }), 'voices' => (function ($query) {
                        $query->with(['postSharingOption' =>
                            function($query1) {
                                $query1->whereHas('sharingOptions')->with('sharingOptions');
                            }]);
                    })])->get();
                $vocs = $originalVocs->slice($skip, $limit);
                $vocPosts = collect();
                $postSharingOptions = collect();
                $sharingOptions = collect();

                foreach ($originalVocs as $item) {
                    $vocPosts = $vocPosts->merge($item->events);
                    $vocPosts = $vocPosts->merge($item->jobs);
                    $vocPosts = $vocPosts->merge($item->updates);
                    $vocPosts = $vocPosts->merge($item->promotions);
                    $vocPosts = $vocPosts->merge($item->voices);
                }

                $allVocsCount = $vocs->count();
                $vocPosts = $vocPosts->slice($skip, $voc_post_limit);

                return view('frontend.home', ['vocCategories' => $vocCategories, 'allVocsCount' => $allVocsCount,
                    'vocPosts' => $vocPosts, 'vocs' => $vocs, 'postSharingOptions' => $postSharingOptions]);
            }

            public function forgetPassword() {
                return view('frontend.includes.forget_password');
            }

//  Search Voc by Regional, All, Nearby and Voc Categories
            public function vocSearch(Request $request) {
                $page = 1;
                if ($request->next_page) {
                    $page = $request->next_page;
                }
                $limit = per_page_search_limit();
                $skip = ($page * $limit) - $limit;

                //get current user ip adress
                $ip = \Request::ip();
                $data = \Location::get($ip);
                $state = $data->cityName;

                $category_ids = $request->categories;
                $vocs = new Voc();

                if (count($category_ids) > 0 && !empty($category_ids) && $category_ids[0] != NULL) {
                    $vocs = $vocs->whereHas('vocCategories', function ($query) use($category_ids) {
                        $query->whereIn('vocs_categories.id', $category_ids);
                    });
                    if (!empty($request->string)) {
                        $vocs = $vocs->where('title', 'LIKE', '%' . $request->string . '%');
                    }
                    $vocs = $vocs->with('events.postSharingOption', 'jobs.postSharingOption', 'updates.postSharingOption', 'promotions.postSharingOption', 'voices.postSharingOption')
                                    ->with('vocFollowers')->get();

                    $vocPosts = collect();
                    $vocs = $vocs->slice($skip, $limit);
                    $postSharingOptions = collect();

                    foreach ($vocs as $item) {
                        $vocPosts = $vocPosts->merge($item->events);
                        $vocPosts = $vocPosts->merge($item->jobs);
                        $vocPosts = $vocPosts->merge($item->updates);
                        $vocPosts = $vocPosts->merge($item->promotions);
                        $vocPosts = $vocPosts->merge($item->voices);
                    }
                    $vocs->getIterator()->uasort(function ($a, $b) {
                        return (($a->vocFollowers->count() + $a->events->count() + $a->jobs->count() + $a->updates->count() + $a->promotions->count() + $a->voices->count()) / 2 > ($b->vocFollowers->count() + $b->events->count() + $b->jobs->count() + $b->updates->count() + $b->promotions->count() + $b->voices->count()) / 2) ? -1 : 1;
                    });

                    $vocPosts = $vocPosts->slice(0, per_page_search_limit());
                    foreach ($vocPosts as $item) {
                        $postSharingOptions = $postSharingOptions->merge($item->postSharingOption);
                    }

                    if (count($vocs) > 0) {
                        $vocs = view('frontend.includes.voc_slider', compact('vocs'))->render();
                        if (count($vocPosts) > 0) {
                            $vocPosts = view('frontend.includes.voc_all_posts', compact(['vocPosts', 'postSharingOptions']))->render();
                        } else {
                            $vocPosts = "";
                        }
                        return response()->json([
                                    'all' => TRUE,
                                    'success' => TRUE,
                                    'vocs' => $vocs,
                                    'vocPosts' => $vocPosts], 200);
                    }

                    $vocs = "";
                    $vocPosts = "";
                    return response()->json([
                                'success' => false,
                                'vocs' => $vocs,
                                'vocPosts' => $vocPosts], 500);
                }
//Search All Vocs
                if ($request->range == config('constant.vocs.0') && !empty($request->next_page)) {
                    if (!empty($request->string)) {
                        $vocs = $vocs->where(function($query) use($request) {
                            $query->whereHas('events', function($query) use($request) {
                                        $query->where('name', 'LIKE', '%' . $request->string . '%')
                                        ->orWhere('body', 'LIKE', '%' . $request->string . '%');
                                    })->orWhereHas('jobs', function($query) use($request) {
                                        $query->where('job_title', 'LIKE', '%' . $request->string . '%')
                                        ->orWhere('body', 'LIKE', '%' . $request->string . '%');
                                    })->orWhereHas('updates', function($query) use($request) {
                                        $query->where('title', 'LIKE', '%' . $request->string . '%')
                                        ->orWhere('body', 'LIKE', '%' . $request->string . '%');
                                    })->orWhereHas('promotions', function($query) use($request) {
                                        $query->where('header', 'LIKE', '%' . $request->string . '%')
                                        ->where('sub_header', 'LIKE', '%' . $request->string . '%')
                                        ->orWhere('body', 'LIKE', '%' . $request->string . '%');
                                    })
                                    ->orWhereHas('voices', function($query) use($request) {
                                        $query->where('name', 'LIKE', '%' . $request->string . '%')
                                        ->orWhere('information', 'LIKE', '%' . $request->string . '%');
                                    });
                        });
                    }
                    $vocs = $vocs->with('events.postSharingOption', 'jobs.postSharingOption', 'updates.postSharingOption', 'promotions.postSharingOption', 'voices.postSharingOption')
                                    ->with('vocFollowers')->get();

                    $allVocsCount = $vocs->count();
                    $vocs = $vocs->slice($skip, $limit);
                    $vocPosts = collect();
                    $postSharingOptions = collect();

                    foreach ($vocs as $item) {
                        $vocPosts = $vocPosts->merge($item->events);
                        $vocPosts = $vocPosts->merge($item->jobs);
                        $vocPosts = $vocPosts->merge($item->updates);
                        $vocPosts = $vocPosts->merge($item->promotions);
                        $vocPosts = $vocPosts->merge($item->voices);
                    }
                    $vocs->getIterator()->uasort(function ($a, $b) {
                        return (($a->vocFollowers->count() + $a->events->count() + $a->jobs->count() + $a->updates->count() + $a->promotions->count() + $a->voices->count()) / 2 > ($b->vocFollowers->count() + $b->events->count() + $b->jobs->count() + $b->updates->count() + $b->promotions->count() + $b->voices->count()) / 2) ? -1 : 1;
                    });

                    $vocPosts = $vocPosts->slice(0, per_page_search_limit());
                    foreach ($vocPosts as $item) {
                        $postSharingOptions = $postSharingOptions->merge($item->postSharingOption);
                    }
                    if (count($vocs) > 0) {
                        $vocs = view('frontend.includes.voc_slider', compact('vocs'))->render();
                        $vocPosts = view('frontend.includes.voc_all_posts', compact(['vocPosts', 'postSharingOptions']))->render();

                        return response()->json(['allVocsCount' => $allVocsCount, 'page' => (int) $request->next_page,
                                    'all' => TRUE,
                                    'success' => TRUE, 'vocs' => $vocs,
                                    'vocPosts' => $vocPosts], 200);
                    }

                    return response()->json(['empty' => ''], 200);
                }

//Search regional
                elseif ($request->range == config('constant.vocs.1') && !empty($request->next_page)) {
                    $page = 1;
                    if ($request->next_page) {
                        $page = $request->next_page;
                    }
                    $limit = per_page_search_limit();
                    $skip = ($page * $limit) - $limit;
//            if (!empty($request->string)) {
                    $vocs = $vocs->where(function($query) use($state, $request) {
                        $query->whereHas('events', function($query) use($state, $request) {
                            $query->where('state', $state)
                                    ->where(function ($query) use($request) {
                                        $query->where('name', 'LIKE', '%' . $request->string . '%')
                                        ->orWhere('body', 'LIKE', '%' . $request->string . '%');
                                    });
                        })->orWhereHas('jobs', function($query) use($state, $request) {
                            $query->where('state', $state)
                                    ->where(function ($query) use($request) {
                                        $query->where('job_title', 'LIKE', '%' . $request->string . '%')
                                        ->orWhere('body', 'LIKE', '%' . $request->string . '%');
                                    });
                        });
                    });
//            }
                    $vocs = $vocs->with('events.postSharingOption', 'jobs.postSharingOption')->get();
                    $vocPosts = collect();
                    $allVocsCount = $vocs->count();
                    $vocs = $vocs->slice($skip, $limit);

                    foreach ($vocs as $item) {
                        $vocPosts = $vocPosts->merge($item->events);
                        $vocPosts = $vocPosts->merge($item->jobs);
                    }

                    $vocs->getIterator()->uasort(function ($a, $b) {
                        return (($a->vocFollowers->count() + $a->events->count() + $a->jobs->count()) / 2 < ($b->vocFollowers->count() + $b->events->count() + $b->jobs->count()) / 2) ? -1 : 1;
                    });
                    $vocPosts = $vocPosts->slice(0, per_page_search_limit());

                    if (count($vocs) > 0) {
                        $vocs = view('frontend.includes.voc_slider', compact('vocs'))->render();
                        $vocPosts = view('frontend.includes.voc_all_posts', compact('vocPosts'))->render();

                        return response()->json(['allVocsCount' => $allVocsCount, 'page' => (int) $request->next_page,
                                    'regional' => TRUE, 'success' => TRUE, 'vocs' => $vocs,
                                    'vocPosts' => $vocPosts], 200);
                    }
                    return response()->json(['empty' => ''], 200);
                }

//Search Nearby Vocs
                elseif ($request->range == config('constant.vocs.2') && !empty($request->next_page)) {
                    $page = 0;
                    if ($request->next_page) {
                        $page = $request->next_page;
                    }
                    $limit = per_page_search_limit();
                    $skip = ($page * $limit) - $limit;
                    $loggedInLatitude = $data->latitude;
                    $loggedInLongitude = $data->longitude;
                    $radius = 7;

                    $haversine = "( 6371 * acos(cos(radians($loggedInLatitude)) * cos(radians(latitude))
                * cos(radians(longitude) - radians($loggedInLongitude)) + sin(radians($loggedInLatitude))
                * sin(radians(latitude))))";
//            if (!empty($request->string)) {
                    $vocs = $vocs->where(function($query) use($radius, $haversine, $request) {
                        $query->WhereHas('events', function($query) use($radius, $haversine, $request) {
                            $query->selectRaw("{$haversine} AS distance")
                                    ->whereRaw("{$haversine} < ?", [$radius])
                                    ->where(function ($query) use($request) {
                                        $query->where('name', 'LIKE', '%' . $request->string . '%')
                                        ->orWhere('body', 'LIKE', '%' . $request->string . '%');
                                    });
                        })->orWhereHas('jobs', function($query) use($radius, $haversine, $request) {
                            $query->selectRaw("{$haversine} AS distance")
                                    ->whereRaw("{$haversine} < ?", [$radius])
                                    ->where(function ($query) use($request) {
                                        $query->where('job_title', 'LIKE', '%' . $request->string . '%')
                                        ->orWhere('body', 'LIKE', '%' . $request->string . '%');
                                    });
                        });
                    });
//            }
                    $vocs = $vocs->with('events.postSharingOption', 'jobs.postSharingOption')->get();
                    $allVocsCount = $vocs->count();
                    $vocs = $vocs->slice($skip, $limit);
                    $vocPosts = collect();

                    foreach ($vocs as $item) {
                        $vocPosts = $vocPosts->merge($item->events);
                        $vocPosts = $vocPosts->merge($item->jobs);
                    }

                    $vocs->getIterator()->uasort(function ($a, $b) {
                        return (($a->vocFollowers->count() + $a->events->count() + $a->jobs->count()) / 2 < ($b->vocFollowers->count() + $b->events->count() + $b->jobs->count()) / 2) ? -1 : 1;
                    });
                    $vocPosts = $vocPosts->slice(0, per_page_search_limit());

                    if (count($vocs) > 0) {
                        $vocs = view('frontend.includes.voc_slider', compact('vocs'))->render();
                        $vocPosts = view('frontend.includes.voc_all_posts', compact('vocPosts'))->render();

                        return response()->json(['allVocsCount' => $allVocsCount, 'page' => (int) $request->next_page,
                                    'nearby' => TRUE, 'success' => TRUE, 'vocs' => $vocs,
                                    'vocPosts' => $vocPosts]);
                    }
                    return response()->json(['empty' => ''], 200);
                }
            }

            public function scrollSearch(Request $request) {
                $limit = per_page_search_limit();
                $skip = ($request->next_page * $limit) - $limit;
                $voc_ids = $request->arr;

                if ($voc_ids) {
                    $vocs = Voc::whereIn('id', $voc_ids);
                    $vocs = $vocs->with('events', 'jobs', 'updates', 'promotions', 'voices', 'vocFollowers')->get();
                    $vocPosts = collect();
                    foreach ($vocs as $item) {
                        $vocPosts = $vocPosts->merge($item->events);
                        $vocPosts = $vocPosts->merge($item->jobs);
                        $vocPosts = $vocPosts->merge($item->updates);
                        $vocPosts = $vocPosts->merge($item->promotions);
                        $vocPosts = $vocPosts->merge($item->voices);
                    }
                    $vocPosts = $vocPosts->slice($skip, $limit);

                    if (count($vocPosts) > 0) {
                        $vocPosts = view('frontend.includes.voc_all_posts', compact('vocPosts'))->render();

                        return response()->json(['scroll' => TRUE, 'success' => TRUE, 'vocPosts' => $vocPosts]);
                    }
                }
                return response()->json(['empty' => ''], 200);
            }

            public function about() {

                $followersData = VocProfileFollowers::select('id', 'subscription')->where('user_id', access()->id())
                        ->where('voc_id', 1)
                        ->pluck('subscription');

                $vocPosts = collect();
                $vocPosts = $vocPosts->merge(VocEvent::where('voc_id', 1)->get());
                $vocPosts = $vocPosts->merge(VocPromotion::where('voc_id', 1)->get());
                $vocPosts = $vocPosts->merge(Voice::where('voc_id', 1)->get());
                $vocPosts = $vocPosts->merge(Job::where('voc_id', 1)->get());
                $vocPosts = $vocPosts->merge(Update::where('voc_id', 1)->get());


                return view('frontend.about')->with([
                            'vocPosts' => $vocPosts,
                            'followersData' => $followersData]);
            }

            public function privacy() {
                return view('frontend.privacy');
            }

            public function termsOfUse() {
                return view('frontend.terms');
            }

        }
        