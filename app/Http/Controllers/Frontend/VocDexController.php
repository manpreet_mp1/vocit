<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\Models\Voc\VocProfile;
use App\Models\Voc\VocProfileFollowers;
use App\Models\Voc\VocDexProfileCategory;
use App\Models\Voc\VocProfileCategory;
use App\Models\Voc\VocCategory;
use App\Models\Access\User\User;
use App\Models\Voc\Voc;
use Illuminate\Support\Facades\Mail;
use App\Models\HireUs;
use DB;

class VocDexController extends Controller {

    public function vocdexHome(Request $request) {
        $page = 1;
        $limit = per_page_search_limit();
        $skip = ($page * $limit) - $limit;

        $categories = VocCategory::where('parent_id', null)->get();

        $followersData = VocProfileFollowers::where('user_id', access()->id());
        if ($request->has('category_id')) {
            $followersData = $followersData->whereHas('voc', function($query) use($request) {
                        $query->whereHas('vocCategories', function($query) use($request) {
                            $query->where('vocs_categories.id', $request->category_id);
                        });
                    })->with(['voc' => function($query) use($request) {
                    $query->whereHas('vocCategories', function($query) use($request) {
                                $query->where('vocs_categories.id', $request->category_id);
                            });
                }]);
        } else {
            $followersData = $followersData->with('voc');
        }

        $followersData = $followersData->get();

        $vocs = $followersData->unique('voc_id')->map(function ($item, $key) {
            return $item->voc;
        });

        $posts = $followersData->groupBy('subscription');

        if (!$posts->isEmpty()) {
            $posts->transform(function ($post, $model) {
                return $model::whereIn('voc_id', $post->pluck('voc_id'))->get();
            });

            $eventposts = isset($posts['App\Models\Voc\VocEvent']) ? $posts['App\Models\Voc\VocEvent'] : null;
            $promotionposts = isset($posts['App\Models\Voc\VocPromotion']) ? $posts['App\Models\Voc\VocPromotion'] : null;
            $voiceposts = isset($posts['App\Models\VocDashboard\Voice']) ? $posts['App\Models\VocDashboard\Voice'] : null;
            $jobposts = isset($posts['App\Models\VocDashboard\Job']) ? $posts['App\Models\VocDashboard\Job'] : null;
            $updateposts = isset($posts['App\Models\VocDashboard\Update']) ? $posts['App\Models\VocDashboard\Update'] : null;

            $vocPosts = collect();
            $vocPosts = $vocPosts->merge($eventposts);
            $vocPosts = $vocPosts->merge($promotionposts);
            $vocPosts = $vocPosts->merge($voiceposts);
            $vocPosts = $vocPosts->merge($jobposts);
            $vocPosts = $vocPosts->merge($updateposts);
        }
        if (!$posts->isEmpty()) {
            $vocPosts = $vocPosts->slice($skip, $limit);
        }
        $vocs = $vocs->slice($skip, $limit);

        return view('frontend.vocDex.home', compact(['categories', 'vocs', 'vocPosts']));
    }

    public function vocdeximgae() {
        $categories = VocCategory::where('parent_id', null)->limit(10)->get();
        return view('frontend.vocDex.home', compact('categories'));
    }

    public function searchCategory(Request $request) {
        $category_id = trim($request->category_id);

        if ($category_id != 0) {
            $data = VocCategory::where('parent_id', $category_id)->get();
        } else {
            $data = VocCategory::where('parent_id', '<>', null)->get();
        }
        $formatted_list = [];

        foreach ($data as $key) {
            $formatted_list[] = ['id' => $key->id, 'text' => $key->name];
        }
        return response()->json($formatted_list, 200);
    }

    public function storeVocDexSubCategory(Request $request) {
        $searchedCategories = collect();
        if (count($request->vocdex_category) > 0) {
            foreach ($request->vocdex_category as $singleCategory) {
                $searchedCategories = $searchedCategories->merge(explode(",", $singleCategory));
            }

            foreach ($searchedCategories as $searchedCategory) {
                $searched[] = trim($searchedCategory);
            }
            $recommended_vocs = collect();

            $recommended_vocs = $recommended_vocs->merge(Voc::where(function ($query) use ($searched) {
                        foreach ($searched as $single) {
                            $query->orWhere('information', '%' . $single . '%')
                                    ->orWhere('title', 'LIKE', '%' . $single . '%');
                        }
                    })->take(2)->get());

            if (count($recommended_vocs) > 0) {
                $recommended_vocs = view('frontend.includes.vocdex_carousel', compact('recommended_vocs'))->render();
                return response()->json(['success' => true, 'recommended_vocs' => $recommended_vocs], 200);
            } else {
                $recommended_vocs = $recommended_vocs->merge(Voc::latest()->take(2)->get());
                $recommended_vocs = view('frontend.includes.vocdex_carousel', compact('recommended_vocs'))->render();
                return response()->json(['success' => true, 'recommended_vocs' => $recommended_vocs], 200);
            }
        }
        return response()->json(['error' => 'Please enter categories.'], 500);
//        $userId = session()->get('user_id');
//
//        if (!empty($userId)) {
//            if (empty($request->input('vocdex_category')) && empty($request->input('occupation'))) {
//                return response()->json(['error' => 'Enter your info below or skip.'],
//                        500);
//            }
//
//            if (!empty($request->input('vocdex_category'))) {
//                foreach ($request->input('vocdex_category') as $category) {
////        dd($category);
////                    foreach ($category as $sub_category) {
//                    $vocdex_profile_category                  = new VocDexProfileCategory();
//                    $vocdex_profile_category->user_id         = $userId;
//                    $vocdex_profile_category->voc_category_id = $category;
//                    $vocdex_profile_category->save();
////                    }
//                }
//            }
//            if (!empty($request->input('occupation'))) {
//                $vocProfile             = VocProfile::where('user_id', $userId)->first();
//                $vocProfile->occupation = $request->input('occupation');
//                $vocProfile->save();
//            }
//            return response()->json(['success' => true], 200);
//            //Status zero means somebody has open second modal of signup process and trying to fill info without the first modal.
//        }
//        return response()->json(['status' => 0], 200);
    }

//    public function getRecommendedVocs(Request $request)
//    {
//        $id            = $request->id;
//        $category_data = VocDexCategory::where('voc_dex_id', 9)->get()->toArray();
//        $category      = [];
//        $sub_category  = [];
//        if (!empty($category_data)) {
//
//            foreach ($category_data as $data) {
//                array_push($category, $data['category_id']);
//                array_push($sub_category, $data['subcategory_id']);
//            }
//            $voc_ids  = VocProfileCategory::whereIn('voc_category_id', $category)->whereIn('voc_subcategory_id',
//                    $sub_category)->pluck('voc_profile_id')->toArray();
//            $voc_data = VocProfile::whereIn('id', $voc_ids)->take(15)->get()->toArray();
//            return response($voc, 200);
//        }
//        return response()->json(['status' => FALSE], 500);
//    }

    public function storeRecommendedVocs(Request $request) {
        $userId = session()->get('user_id');
        if (!empty($userId)) {
            //Login user
            $user = User::find($userId);
            Auth::login($user);

            session()->forget('user_id');

            //Making current user follow about voc
            $follow = new VocProfileFollowers();
            $follow->voc_id = 1;
            $follow->user_id = $userId;
            $follow->subscription = 'App\Models\VocDashboard\Job';
            $follow->save();

            return response()->json(['success' => true, 'message' => 'Yayy!! Your Vocdex profile has been created successfully.',
                        'route' => route('frontend.vocdex-home')], 200);
        }
        //Status zero means somebody has open second modal of signup process and trying to fill info without the first modal.
        return response()->json(['status' => 0], 200);
    }

    public function getDexCategories() {
        $categories = VocCategory::paginate(10)->where('parent_id', null)->toArray();
        if (!empty($categories)) {
            return view('frontend.vocDex.home', compact('categories'));
        }
        return redirect()->back()->withFlashDanger('Unable to load vocdex categories.');
    }

    public function loadVocDex(Request $request) {
        $page = 0;
        if ($request->next_page) {
            $page = $request->next_page;
        }
        $limit = per_page_search_limit();
        $skip = ($page * $limit) - $limit;

        $followersData = VocProfileFollowers::where('user_id', access()->id());
        if ($request->has('category_id')) {
            $followersData = $followersData->whereHas('voc', function($query) use($request) {
                        $query->whereHas('vocCategories', function($query) use($request) {
                            $query->where('vocs_categories.id', $request->category_id);
                        });
                    })->with(['voc' => function($query) use($request) {
                    $query->whereHas('vocCategories', function($query) use($request) {
                                $query->where('vocs_categories.id', $request->category_id);
                            });
                }]);
        } else {
            $followersData = $followersData->with('voc');
        }

        $followersData = $followersData->get();
        $vocs = $followersData->unique('voc_id')->map(function ($item, $key) {
            return $item->voc;
        });

        $posts = $followersData->groupBy('subscription');

        if (!$posts->isEmpty()) {
            $posts->transform(function ($post, $model) {
                return $model::whereIn('voc_id', $post->pluck('voc_id'))->get();
            });

            $eventposts = isset($posts['App\Models\Voc\VocEvent']) ? $posts['App\Models\Voc\VocEvent'] : null;
            $promotionposts = isset($posts['App\Models\Voc\VocPromotion']) ? $posts['App\Models\Voc\VocPromotion'] : null;
            $voiceposts = isset($posts['App\Models\VocDashboard\Voice']) ? $posts['App\Models\VocDashboard\Voice'] : null;
            $jobposts = isset($posts['App\Models\VocDashboard\Job']) ? $posts['App\Models\VocDashboard\Job'] : null;
            $updateposts = isset($posts['App\Models\VocDashboard\Update']) ? $posts['App\Models\VocDashboard\Update'] : null;

            $vocPosts = collect();
            $vocPosts = $vocPosts->merge($eventposts);
            $vocPosts = $vocPosts->merge($promotionposts);
            $vocPosts = $vocPosts->merge($voiceposts);
            $vocPosts = $vocPosts->merge($jobposts);
            $vocPosts = $vocPosts->merge($updateposts);
        }
        $allVocsCount = $vocs->count();
        $vocs = $vocs->slice($skip, $limit);
        $vocPosts = $vocPosts->slice($skip, $limit);

        if (count($vocs) > 0) {
            $vocs = view('frontend.includes.voc_slider', compact('vocs'))->render();
            $vocPosts = view('frontend.includes.voc_all_posts', compact('vocPosts'))->render();

            return response()->json(['allVocsCount' => $allVocsCount, 'page' => (int) $request->next_page,
                        'success' => TRUE, 'vocs' => $vocs,
                        'vocPosts' => $vocPosts], 200);
        }
        return response()->json(['empty' => ''], 200);
    }
    
    public function hireUs() {
        $user = Auth::user();
        if (!HireUs::where('user_id', $user->id)->exists()) {
            DB::beginTransaction();
            if ((HireUs::create(['user_id' => $user->id]) && (Voc::where('user_id', $user->id)->update(['level_id'=> config('constant.services.hire_us')])))) {
                DB::commit();
                return response(['status' => true, 'message'=>'Thanks for hiring us. We will contact you shortly.'], 200);
            }
            DB::rollback();
        }
        //means user has already asked for hire us option
         return response(['status' => false, 'message'=>'You have already send us request for hire.'], 500);
    }

}
