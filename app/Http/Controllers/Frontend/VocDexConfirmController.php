<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Exceptions\GeneralException;
use App\Models\Voc\VocDex;

class VocDexConfirmController extends Controller {

    public function confirm($token) {
        $voc = VocDex::where('confirmation_code', $token)->first();
        if (!is_null($voc)) {

            if ($voc->confirmed == 1) {
                //need to return to the login page 
                return 'Your account is already activated';
            }
            if ($voc->confirmation_code == $token) {
                $voc->confirmed = 1;
                $voc->save();
                return 'Your account has been successfully activated!';
            }
        }
        throw new GeneralException(trans('exceptions.frontend.auth.confirmation.mismatch'));
    }

}
