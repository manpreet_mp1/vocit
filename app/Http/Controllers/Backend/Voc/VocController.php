<?php

namespace App\Http\Controllers\Backend\Voc;

use App\Http\Controllers\Controller;
use App\Models\Voc\Voc;

class VocController extends Controller
{

    public function index()
    {
        $vocs = Voc::get();
        return view('backend.vocs.activatedVocs', compact('vocs'));
    }

    public function getDeactivatedVocs()
    {
        $vocs = Voc::onlyTrashed()->get();
        return view('backend.vocs.deactivatedVocs', compact('vocs'));
    }

    public function deactivateVoc($userid = null)
    {
        if ($userid != null) {
            $voc = Voc::where('user_id', $userid);
            if ($voc->delete()) {
                return redirect()->back()->withFlashSuccess('Voc Deactivated Successfully.');
            }
        }
        return redirect()->back()->withFlashDanger('Unable to deactivate the voc.');
    }

    public function activateVoc($userid = null)
    {
        if ($userid != null) {
            $voc = Voc::withTrashed()->where('user_id', $userid);
            if ($voc->restore()) {
                return redirect()->route('admin.voc.activatedVocs')->withFlashSuccess('Voc activated Successfully.');
            }
        }
        return redirect()->back()->withFlashDanger('Unable to activate the voc.');
    }
}