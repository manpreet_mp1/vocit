<?php

namespace App\Http\Controllers\Backend\Voc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\VocCategoryRequest;
use App\Http\Requests\Backend\UpdateVocCategoryRequest;
use App\Models\Voc\VocCategory;
use Illuminate\Support\Collection;

class VocCategoryController extends Controller
{

    public function create()
    {
        if (access()->hasRole(config('constant.roles.Administrator')) || access()->hasRole(config('constant.roles.Voc'))) {
            return view('backend.vocs.createCategory');
        } else {
            return redirect()->back()->withFlashDanger("You have no rights to access this.");
        }
    }

//    public function edit($id)
//    {
//    }

    public function store(VocCategoryRequest $request)
    {
        $data             = $request->all();
        $categories       = new VocCategory;
        $categories->name = $data['name'];
        if ($categories->save()) {
            return redirect()->route('admin.category.show');
        }

        return redirect()->back()->withFlashDanger('Unable to save data.');
    }

    public function show()
    {
        $categories = VocCategory::where('parent_id', null)->get();
        return view('backend.vocs.allCategories', compact('categories'));
    }

    public function update(UpdateVocCategoryRequest $request)
    {
        if (!empty($request->categories)) {
            $requestData = $request->categories;

            $collection = collect($requestData);

            $collectedData = $collection->count();
            if ($collectedData < 20 && !empty($requestData)) {
//            $requestData = $request->categories;

                foreach ($requestData as $data) {

                    if (isset($data['id'])) {
                        $projectData = VocCategory::findOrFail($data['id']);
                    } else {
                        $projectData = new VocCategory();
                    }

                    if (isset($data['id']) && $data['is_deleted']) {
                        $projectData->delete();
                    } else {
                        $projectData->name = $data['name'];

                        if (!$projectData->save()) {
                            return redirect()->back()->withFlashDanger('Unable to save data.');
                        }
                    }
                }

                return redirect()->back()->withFlashSuccess('Voc category updated successfully.');
            } else {

                return redirect()->back()->withFlashDanger('Maximum category limit 20 reached.');
            }
        } else {

            return redirect()->back()->withFlashDanger('Voc category updated successfully.');
        }
    }

    public function delete($id)
    {
        VocCategory::where('id', $id)
            ->delete();
        return redirect()->back()
                ->withFlashSuccess('Category deleted successfully.');
    }
}