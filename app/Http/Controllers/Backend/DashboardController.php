<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Voc\Voc;
use App\Http\Requests\Frontend\VocRequest;
use App\Models\HireUs;
use App\Models\Access\User\User;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('backend.dashboard');
    }

    public function allVocs() {
        $all_vocs = Voc::all();
        return view('backend.vocs.index', compact('all_vocs'));
    }

    public function createVoc() {
        return view('backend.vocs.create');
    }

    public function storeVoc(VocRequest $request) {
        Voc::create($request->all());
        return redirect()->route('admin.allVocs');
    }

    public function editVoc($id = null) {
        $voc = Voc::find($id);
        return view('backend.vocs.edit', compact('voc'));
    }

    public function updateVoc(VocRequest $request) {
        $voc = Voc::findOrFail($request->id);
        $voc->update($request->all());
        return redirect()->route('admin.allVocs');
    }
    
    public function deleteVoc(VocRequest $request) {
        $delete = Voc::findOrFail($request->id)->delete();
        if ($delete) {
            return redirect()->route('admin.allVocs')
                            ->withFlashSuccess('Voc deleted successfully.');
        }
    }
    
    public function vocHireUs() {
        $hireUsVocs = HireUs::with('user.voc')->get();
//        dd($hireUsVocs->toArray());
         return view('backend.hireUs', compact('hireUsVocs'));
    }
    
    public function mailForHireUs($id) {
        $user = User::where('id', $id)->first();
        DB::beginTransaction();
        if (HireUs::where('user_id', $id)->update(['status' => config('constant.hire_us_status.contacted')])) {

            Mail::raw('Hi ' . $user->name . ', This mail is regarding the hire us process which you initiated', function ($message) {
                $message->to($user->email)
                        ->from('hireus@vocit.io')
                        ->subject('Hire Us');
            });
            DB::commit();
        }
        DB::rollback();
    }

}
