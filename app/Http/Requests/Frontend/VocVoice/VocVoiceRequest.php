<?php

namespace App\Http\Requests\Frontend\VocVoice;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class MarketingRequest
 */
class VocVoiceRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'name' => 'required|min:3|max:255',
            'body' => 'required',
        ];
        if (!isset($this->id) && empty($this->id)) {
             $rules += ['profile_image' => 'required'];
        }
        return $rules;
    }

    public function messages() {
        return [
            'name' => 'Please enter a title',
            'body' => 'Please enter description',
            'profile_image' => 'Please upload the video below 10Mb',
        ];
    }

}
