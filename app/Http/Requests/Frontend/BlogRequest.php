<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;
/**
 * Description of BlogRequest
 *
 * @author pc8
 */
class BlogRequest extends Request {
    //put your code here
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
        'title' => 'required',
        'category' => 'required',
        ];
    }
}
