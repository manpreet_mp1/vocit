<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class VocEditRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'title' => 'required',
            'information' => 'required',
        ];
        $voc = Auth::user()->voc;
        if (isset($voc->id) && !empty($voc->id)) {
            $rules += ['image' => 'mimes:jpeg,bmp,png|max:8000'];
        } else {
            $rules += ['image' => 'required|mimes:jpeg,bmp,png|max:8000'];
        }
        return $rules;
    }

    public function messages() {
        return [
            'title.required' => 'Please enter a title.',
            'image.required' => 'Please upload the image.',
            'information.required' => 'Please enter description.',
        ];
    }

}
