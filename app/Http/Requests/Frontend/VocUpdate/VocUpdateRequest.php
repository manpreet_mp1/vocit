<?php

namespace App\Http\Requests\Frontend\VocUpdate;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class MarketingRequest
 */
class VocUpdateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'title' => 'required',
            'body' => 'required',
        ];
        if (!isset($this->id) && empty($this->id)) {
             $rules += ['logo' => 'required'];
        }
        if($this->media_type == 2) {
            $rules += ['media' => 'required'];
        }
        if (!isset($this->id) && empty($this->id)) {
           if($this->media_type == 2){
               $rules += ['media' => 'required|url'];
           } else{
             $rules += ['media' => 'required'];
           }
        }
        return $rules;
    }

    public function messages() {
        return [
            'title' => 'Please enter a title',
            'logo' => 'Please upload the logo',
            'media' => 'Please upload the media',
            'body' => 'Please enter description',
        ];
    }

}
