<?php

namespace App\Http\Requests\Frontend\Voc;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class MarketingRequest
 */
class VocCodeOwnRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'style' => 'required',
            'script' => 'required',
            'markup' => 'required',
        ];
        return $rules;
    }

    public function messages() {
        return [
            'style.required' => 'Enter valid style.',
            'script.required' => 'Enter valid script.',
            'markup.required' => 'Enter valid markup.',
        ];
    }

}
