<?php

namespace App\Http\Requests\Frontend\Voc;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class MarketingRequest
 */
class JobPosting extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
       $rules = [
            'job_title' => 'required|min:3|max:255',
            'job_industry' => 'required',
            'location' => 'required',
            'pay' => 'required',
            'employment_type' => 'required',
            'benefits_offered' => 'required',
            'body' => 'required',
        ];

        if (isset($this->id) && !empty($this->id)) {
            $rules += ['logo' => ''];
        } else {
            $rules += ['logo' => 'required'];
        }
        return $rules;
    }

    public function messages() {
        return [
            'job_title' => 'Please enter a job title',
            'job_industry' => 'Please enter a job industry',
            'location' => 'Please enter a job location',
            'pay' => 'Please enter a pay',
            'employment_type' => 'Please enter a Employment',
            'benefits_offered' => 'Please enter a benefits offered',
            'logo' => 'Please upload images below 10 MB.',
        ];
    }

}
