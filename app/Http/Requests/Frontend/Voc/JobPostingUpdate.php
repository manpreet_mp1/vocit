<?php

namespace App\Http\Requests\Frontend\Voc;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class MarketingRequest
 */
class JobPostingUpdate extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'job_title' => 'required|min:3|max:255',
            'job_industry' => 'required',
            'location' => 'required',
            'pay' => 'required',
            'employment_type' => 'required',
            'benefits_offered' => 'required',
            'body' => 'required',
        ];


        if (isset($this->id) && !empty($this->id)) {
            $rules += ['picture' => 'mimes:jpeg,bmp,png|max:10000'];
        } else {
            $rules += ['picture' => 'required|mimes:jpeg,bmp,png|max:10000'];
        }
        return $rules;
    }

    public function messages() {
        return [
            'job_title' => 'Please enter a job title',
            'job_industry' => 'Please enter a job industry',
            'job_location' => 'Please enter a job location',
            'pay' => 'Please enter a pay',
            'employment' => 'Please enter a Employment',
            'benefits' => 'Please enter a benefits offered',
            'picture' => 'Please upload images below 10 MB.',
        ];
    }

}
