<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class VocEventRequest extends FormRequest {

    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolx
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'name' => 'required|min:3|max:255',
            'body' => 'required',
            'start' => 'required',
            'end' => 'required',
            'location' => 'required',
        ];
        if (!isset($this->id) && empty($this->id)) {
             $rules += ['logo' => 'required'];
        }
        return $rules;
    }

    public function messages() {
        return [
            'name' => 'Please enter event name.',
            'body' => 'Please enter body.',
            'start' => 'Please enter start time',
            'end' => 'Please enter end time',
            'logo' => 'Please enter cover image below 10Mb',
            'location' => 'Please enter location',
        ];
    }

}
