<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;

class VocProfileRequest extends Request
{

    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email',
            'name' => 'required',
        ];
    }
}