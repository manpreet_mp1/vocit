<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class VocPromotionRequest extends FormRequest {

    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolx
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
         $rules = [
            'header' => 'required|min:3|max:255',
            'sub_header' => 'required|min:3|max:255',
            'body' => 'required',
        ];
        if (!isset($this->id) && empty($this->id)) {
             $rules += ['cover_image' => 'required'];
        }
        return $rules;
    }
    public function messages() {
        return [
            'header' => 'Please enter header.',
            'sub_header' => 'Please enter sub_header.',
            'body' => 'Please enter body.',
            'cover_image' => 'Please enter cover image below 10Mb.',
        ];
    }

}
