<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VocCategory extends Model
{
    public $timestamps  = true;
    protected $table    = 'vocs_categories';
    protected $fillable = [];

    use SoftDeletes;
    protected $tables = [
        'deleted_at',
    ];

    public function parent()
    {
        return $this->belongsTo('App\Models\Voc\VocCategory', 'parent_id');
    }

    public function vocs()
    {
        return $this->belongsToMany('App\Models\Voc\Voc', 'voc_profile_categories',
                'voc_category_id', 'voc_id');
    }
}