<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;

class VocDexProfileCategory extends Model {

    protected $table = 'vocdex_profile_categories';
    protected $fillable = [];
    public $timestamps = false;

}
