<?php

namespace App\Models\voc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VocCodeOwn extends Model
{
    public $timestamps  = true;
    protected $table    = 'voc_code_owns';
    protected $fillable = [
        'style',
        'script',
        'markup',
    ];

    use SoftDeletes;
    protected $tables = [
        'deleted_at',
    ];

    public function voc()
    {
        return $this->belongsTo('App\Models\Voc\Voc')  ;
    }
}