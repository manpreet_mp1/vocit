<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;
use Voc;

class VocTemplate extends Model
{
    protected $table    = 'voc_templates';
    protected $fillable = [];

    public function voc()
    {

        return $this->belongsTo('App\Models\Voc\Voc', 'template_id');
    }
}