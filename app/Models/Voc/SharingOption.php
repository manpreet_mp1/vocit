<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;

class SharingOption extends Model
{
    public $timestamps  = true;
    protected $fillable = [
    ];


    public function postSharingOption()
    {
        return $this->hasMany('App\Models\Voc\PostSharingOption', 'sharing_option_id');
    }
}