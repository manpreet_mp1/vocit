<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Illuminate\Notifications\Notifiable;

class VocProfile extends Model
{

    use Notifiable;
    protected $searchableColumns = ['name'];
    protected $table             = 'profiles';
    protected $fillable          = [];

    public function follower()
    {

        return $this->belongsTo('App\Models\Voc\VocProfileFollowers', 'user_id');
    }
}