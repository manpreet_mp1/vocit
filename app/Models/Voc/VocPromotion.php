<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VocPromotion extends Model
{
    public $timestamps  = true;
    protected $table    = 'voc_promotions';
    protected $fillable = [
        'header',
        'sub_header',
        'cover_image',
        'body',
    ];

    use SoftDeletes;
    protected $tables = [
        'deleted_at',
    ];

    public function followers()
    {
        return $this->morphMany('App\Models\Voc\VocProfileFollowers',
                'subscription');
    }

    public function postSharingOption()
    {
        return $this->morphMany('App\Models\Voc\PostSharingOption', 'postable');
    }

    public function voc()
    {
        return $this->belongsTo('App\Models\Voc\Voc')  ;
    }
}