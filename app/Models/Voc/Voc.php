<?php
namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voc extends Model
{
    use SoftDeletes;
    protected $table = 'vocs';
    protected $fillable = [];
    protected $dates = ['deleted_at'];

    public function templates() {

        return $this->hasOne('App\Models\Voc\VocTemplate','template_id');
    }

    public function vocCategories()
    {
        return $this->belongsToMany('App\Models\Voc\VocCategory', 'voc_profile_categories');
    }

    public function vocFollowers()
    {
        return $this->hasMany('App\Models\Voc\VocProfileFollowers', 'voc_id');
    }

    public function events()
    {
        return $this->hasMany('App\Models\Voc\VocEvent');
    }
    
    public function codeOwns()
    {
        return $this->hasOne('App\Models\Voc\VocCodeOwn');
    }

    public function promotions()
    {
        return $this->hasMany('App\Models\Voc\VocPromotion');
    }
    public function voices()
    {
        return $this->hasMany('App\Models\VocDashboard\Voice');
    }
    public function jobs()
    {
        return $this->hasMany('App\Models\VocDashboard\Job');
    }
    public function updates()
    {
        return $this->hasMany('App\Models\VocDashboard\Update');
    }

}