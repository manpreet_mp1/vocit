<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;

class PostSharingOption extends Model
{
    public $timestamps  = true;
    protected $fillable = [
    ];

    public function postable()
    {
        return $this->morphTo();
    }

    public function sharingOptions()
    {
        return $this->belongsTo('App\Models\Voc\SharingOption', 'sharing_option_id');
    }
}