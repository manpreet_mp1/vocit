<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class VocDex extends Model
{
    use Notifiable;
    protected $table    = 'voc_dex';
    protected $fillable = [];

}