<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Voc\Voc;

class VocProfileFollowers extends Model
{
    protected $table    = 'voc_profile_followers';
    protected $fillable = [];
    use SoftDeletes;

    public function VocDex()
    {
        return $this->hasMany('App\Models\Voc\VocDex');
    }

    public function subscription()
    {
        return $this->morphTo();
    }

    public function voc()
    {
        return $this->belongsTo(Voc::class);
    }

    public function user()
    {

        return $this->belongsTo('App\Models\Access\User\User');
    }
}