<?php

namespace App\Models\Voc;

use Illuminate\Database\Eloquent\Model;

class VocProfileCategory extends Model
{
    protected $table    = 'voc_profile_categories';
    protected $fillable = [];
    public $timestamps  = false;

}