<?php

namespace App\Models\VocDashboard;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Update extends Model
{
    public $timestamps  = true;
    protected $table    = 'voc_updates';
    protected $fillable = [
    ];

    use SoftDeletes;
    protected $tables = ['deleted_at'];

    public function followers()
    {
        return $this->morphMany('App\Models\Voc\VocProfileFollowers',
                'subscription');
    }

    public function postSharingOption()
    {
        return $this->morphMany('App\Models\Voc\PostSharingOption', 'postable');
    }

    public function voc()
    {
        return $this->belongsTo('App\Models\Voc\Voc')  ;
    }
}