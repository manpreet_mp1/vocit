<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps    = true;
    protected $primaryKey = 'id';
    protected $fillable   = [
        'name',
    ];

}