<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public $timestamps    = true;
    protected $primaryKey = 'id';
    protected $fillable   = [
        'title',
        'category_id',
        'description',
    ];

}