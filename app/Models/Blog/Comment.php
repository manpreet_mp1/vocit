<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $timestamps    = true;
    protected $primaryKey = 'id';
    protected $fillable   = [
        'comment',
    ];

}