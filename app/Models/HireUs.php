<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HireUs extends Model
{
    protected $table = 'hire_us';
    use SoftDeletes;
    protected $fillable = ['user_id'];
    
    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User','user_id');
    }
}
